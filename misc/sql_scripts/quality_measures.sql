
DROP TABLE IF EXISTS SMAC_QualityMeasures;
DROP TABLE IF EXISTS SMAC_QualityFunction;

-- Table to store the computed quality measures
CREATE TABLE SMAC_QualityMeasures (
    SMACExperimentID              INT UNSIGNED NOT NULL,
    ExperimentID                  INT UNSIGNED NOT NULL,
    TotalSolved                   INT(11) NOT NULL,
    TotalTime                     INT(11) NOT NULL,
    WeightedSolved                INT(11) NOT NULL,
    PRIMARY KEY (SMACExperimentID, ExperimentID),
    FOREIGN KEY (SMACExperimentID) REFERENCES SMACExperiment(SMACExperimentID)
);

-- Table to represent the chosen quality function for the experiment

CREATE TABLE SMAC_QualityFunction (
    SMACExperimentID            INT UNSIGNED NOT NULL,
    Measure                     ENUM('avg_rating', 'solved_time', 'total_solved', 'unique_solved_all', 
                                     'unique_solved_exp', 'unique_solved_best_heuristic', 'weighted_solved') NOT NULL,
    Incumbent                   INT UNSIGNED NOT NULL,
    -- TimeOut                     Int(11) NOT NULL,
    PRIMARY KEY (SMACExperimentID),
    FOREIGN KEY (SMACExperimentID) REFERENCES SMACExperiment(SMACExperimentID)
    -- FOREIGN KEY (Incumbent) REFERENCES Incumbent(IncumbentID)
);


