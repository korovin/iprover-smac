"""
Module for running prover and experiment processes.
"""
from typing import Optional, Tuple
import os
import subprocess
import time
import signal
import logging

from hos import utils
from hos.experiment_config import get_prover_exec

log = logging.getLogger()


class Process:
    def __init__(self):
        self.proc: Optional[subprocess.Popen] = None  # type:  ignore
        self.cmd: Optional[str] = None
        self.start_time: Optional[float] = None

        self.out_file: str = utils.get_tmp_out_file()
        self.out_file_errs = self.out_file + "_error"
        self.out_err: Optional[str] = None
        self.returncode: Optional[int] = None
        self.errors: Optional[bool] = None

    def _get_cmd(self) -> str:
        raise NotImplementedError()

    def start(self):
        self.cmd = self._get_cmd()
        log.debug("Running: " + self.cmd)
        proc = subprocess.Popen(self.cmd, shell=True, preexec_fn=os.setsid)
        self.proc = proc
        self.start_time = time.time()

    def wait(self, interval: float):
        # TODO implement some sort of max time?
        while self.is_running():
            time.sleep(interval)

    def is_running(self) -> bool:
        assert self.proc is not None
        return self.proc.poll() is None

    def is_finished(self):
        return not self.is_running()

    def kill(self) -> None:
        # If a process is still active
        if self.is_running():
            # Get process group id and kill the group
            assert self.proc is not None

            try:
                pgrp = os.getpgid(self.proc.pid)
                os.killpg(pgrp, signal.SIGKILL)
                log.debug(f"Killed process: {pgrp}")
            except ProcessLookupError:
                # In case there is a race condition on the termination, in which case
                # the process has terminated as it doesn't exist any more.
                log.debug("Process already killed, skipping ...")

            _ = self.proc.communicate()  # Empty buffer

    def _empty_buffers(self):
        assert self.proc is not None
        # The buffers sometimes hang - need to be shut down
        # The clausifier is usually the root of this problem.
        try:
            # Attempt to empty
            _ = self.proc.communicate(timeout=3)
        except subprocess.TimeoutExpired:
            self.kill()  # Something is wrong (e.g. hanging) kill and go again.
            _ = self.proc.communicate()

    def _check_out_errs(self):
        # Empty buffers so file is read correctly
        self._empty_buffers()

        with open(self.out_file_errs, "r") as f:
            out_err = f.read().strip().rstrip()

        if out_err != "":
            self.out_err = out_err.strip("\n")

    def check_errors(self) -> bool:
        """Returns True if there are errors."""
        assert self.proc is not None
        self._check_out_errs()
        self.returncode = self.proc.returncode

        # If out error is not empty or non-valid exit code is given.
        # Processes can be killed with SIGKILL so -9 is also considered valid.
        if self.out_err is not None or self.returncode not in [0, -9]:
            log.error(
                f'"{self}" ran with exit code {self.returncode} and error: {self.out_err}'
            )
            log.error(f"cmd was: {self.cmd}")
            self.errors = True
            return True  # There are errors!

        self.errors = False
        return False

    def remove_out_files(self) -> None:
        utils.remove_file(self.out_file)
        utils.remove_file(self.out_file_errs)


class ExperimentProcess(Process):
    def __init__(
        self,
        exp_config_file: str,
        problem_set: str,
        machines: str,
        mock_exp: bool = False,
    ):
        super().__init__()
        self.exp_config_file = exp_config_file
        self.problem_set = problem_set
        self.machines = machines
        self.mock_exp = mock_exp  # Does not run the experiment - just sets it up

    def _get_cmd(self) -> str:
        base_cmd = [
            "setup-experiment",
            "-f",
            self.exp_config_file,
            "-pc",
            self.problem_set,
            "-s",
            self.machines,
            "-q",
        ]
        if not self.mock_exp:
            base_cmd += ["-i"]  # Add the flag for running the experiment
        return " ".join(base_cmd) + f" 1>> {self.out_file} 2>> {self.out_file_errs}"

    def get_experiment_id(self) -> int:
        exp_id = utils.extract_experiment_id_from_config_out(self.out_file)
        if exp_id is None:
            log.warning(
                "Something went wrong while running the experiment. Could not retrieve experiment ID"
            )
            with open(self.out_file, "r") as f:
                outs = f.read()
            log.info(f"Output of outfile: START\n{outs}\nEND")
            return -1
        return exp_id

    def clean(self) -> None:
        self.remove_out_files()
        utils.remove_file(self.exp_config_file)


class ProverProcess(Process):
    def __init__(
        self, heuristic: str, problem_path: str, prover_path: Optional[str] = None
    ):
        super().__init__()

        self.heuristic = heuristic
        self.problem_path = problem_path
        if prover_path is None:
            self.prover_path = get_prover_exec()
        else:
            self.prover_path = prover_path

        self.szs_status: Optional[str] = None

    def _get_cmd(self) -> str:
        return (
            f"{self.prover_path} {self.heuristic} "
            f"{self.problem_path} 1>> {self.out_file} 2>> {self.out_file_errs}"
        )

    """
    def success(self) -> bool: TODO remove
        self.szs_status = utils.get_prover_output_status(self.out_file)
        return check_prover_success(self.szs_status, ltb_mode=self.problem.is_ltb)
    """

    def clean(self):
        self.remove_out_files()

    def get_output(self) -> str:
        with open(self.out_file, "r") as f:
            r = f.read()
        return r

    def __str__(self):
        return "ProverProcess"


def run_prover_process(
    heuristic: str, problem: str, interval: float, prover_path: Optional[str] = None
) -> ProverProcess:
    proc = ProverProcess(heuristic, problem, prover_path)
    proc.start()
    proc.wait(interval=interval)
    proc.check_errors()
    # proc.remove_out_files() TODO UNCOMMENT
    return proc


def run_solver_stats_extraction(
    heuristic: str, problem: str, prover_path: str, wait_time: float
) -> Tuple[str, bool]:
    proc = ProverProcess(heuristic, problem, prover_path)
    proc.start()
    time.sleep(wait_time)
    proc.wait(interval=0.2)
    proc.check_errors()
    assert proc.errors is not None

    # Get output and clean up
    out = proc.get_output()
    proc.remove_out_files()
    return out, proc.errors
