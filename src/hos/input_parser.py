"""
Module containing the input argument parser for the optimisation loop.
"""
import argparse
import logging
from typing import List

from hos.admissible import ClusterQualityMetric
from hos.cluster import ClusterMethod
from hos.admissible import UnsolvedMapping
from hos.embedding import TargetType
from hos.target.eval_cost import QualityMetric


def get_optimisation_input_args(args: List[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "problem_collection",
        type=str,
        help="The problem collection to optimise over (name specified in the database).",
    )
    parser.add_argument(
        "parameter_space",
        type=str,
        help="Path to the file describing the parameter space.",
    )

    parser.add_argument(
        "--initial_heuristics",
        default=None,
        nargs="*",
        type=int,
        help="List of initial heuristics to consider as incumbents. Computing default if None.",
    )

    parser.add_argument(
        "--wc_local",
        default=10,
        type=int,
        help="Local wallclock time limit used during local optimisation.",
    )

    parser.add_argument(
        "--wc_global",
        default=20,
        type=int,
        help="Global wallclock time limit used during global evaluation.",
    )

    parser.add_argument(
        "--quality_metric",
        type=QualityMetric,
        choices=list(QualityMetric),
        default=QualityMetric.WEIGHTED_SOLVED,
        help="Metric used to score target evaluation runs.",
    )

    parser.add_argument(
        "--smac_run_count",
        default=20,
        type=int,
        help="Number of configurations for SMAC to evaluate on each problem cluster",
    )

    parser.add_argument(
        "--smac_run_count_increment",
        default=2,
        type=int,
        help="Evaluation increment for each HLP loop.",
    )

    parser.add_argument(
        "--max_hlp_iterations",
        default=7,
        type=int,
        help="Termination condition - number of hlp iterations.",
    )
    parser.add_argument(
        "--max_runtime",
        default=8,
        type=int,
        help="Termination condition - maximum time before last iteration (in hours).",
    )

    parser.add_argument(
        "--max_no_invalid_heuristics",
        default=1000,
        type=int,
        help="The maximum number of invalid heuristics allowed for the target function.",
    )

    parser.add_argument(
        "--cluster_method",
        type=ClusterMethod,
        choices=list(ClusterMethod),
        default=ClusterMethod.ADMISSIBLE,
        help="Methodology used to cluster the problems.",
    )
    parser.add_argument(
        "--no_cluster_samples",
        type=int,
        default=4,
        help="Maximum number of problem clusters to optimise over for each hlp loop.",
    )
    parser.add_argument(
        "--max_cluster_population",
        type=int,
        default=108,
        help="Maximum sampled population in each problem cluster.",
    )

    parser.add_argument(
        "--admissible_constant",
        type=float,
        default=2.0,
        help="Relative constant for computing admissibility.",
    )
    parser.add_argument(
        "--admissible_factor",
        type=float,
        default=1.15,
        help="Relative factor for computing admissibility.",
    )
    parser.add_argument(
        "--admissible_filter_ratio",
        type=float,
        default=0.95,
        help="Filter problems with ratio of admissible heuristics to mitigate density grouping drift.",
    )
    parser.add_argument(
        "--admissible_quality_metric",
        type=ClusterQualityMetric,
        choices=list(ClusterQualityMetric),
        default=ClusterQualityMetric.DI,
        help="Quality measure used to compute the optimal number of clusters.",
    )

    parser.add_argument(
        "--admissible_unsolved_mapping",
        type=UnsolvedMapping,
        choices=list(UnsolvedMapping),
        default=UnsolvedMapping.DISTANCE,
        help="Mapping method for non-clustered unsolved isntances to the admissible clusters",
    )
    parser.add_argument(
        "--admissible_unsolved_target",
        type=TargetType,
        choices=list(TargetType),
        default=TargetType.ADMISSIBLE,
        help="Target embedding type for learnign the embedding function",
    )
    parser.add_argument(
        "--admissible_unsolved_sampling_ratio",
        type=float,
        default=0.4,
        help="Ratio of solved-unsolved problems when including unsolved into the clusters",
    )
    parser.add_argument(
        "--admissible_unsolved_n_neighbours",
        type=int,
        default=3,
        help="Number of nearest neighbours when mapping via knn",
    )

    # Set loglevel
    parser.add_argument(
        "-d",
        "--debug",
        help="Print debug logs",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        # default=logging.WARNING,
    )
    """
    parser.add_argument(
        "-v",
        "--verbose",
        help="Print info logs",
        action="store_const",
        dest="loglevel",
        const=logging.INFO,
    )
    """
    parser.add_argument(
        "-q",
        "--quiet",
        help="Be quiet",
        action="store_const",
        dest="loglevel",
        const=logging.WARNING,
    )

    return parser.parse_args(args=args)
