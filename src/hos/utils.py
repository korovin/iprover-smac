"""
Module containing base utility functions
"""
import json
from pathlib import Path
import os
import tempfile
import re
from typing import Optional, Dict

from hos.logger import get_current_logdir

TMP_DIR = "hos_ml"


def get_tmp_out_file(prefix: str = "") -> str:
    # Make the dir if not exists
    Path(f"/tmp/{TMP_DIR}").mkdir(parents=True, exist_ok=True)

    # Create the tmp file in the current tmp directory and return the file name
    fd, filepath = tempfile.mkstemp(prefix=f"{TMP_DIR}" + prefix)
    os.close(fd)  # Close the open file descriptor
    return filepath


def remove_file(file):
    try:
        os.remove(file)
    except FileNotFoundError:
        pass


def extract_experiment_id_from_config_out(out_file_path: str) -> int | None:
    with open(out_file_path, "r") as f:
        outs = f.read()

    # Get the experiment id from the output
    exp_match = re.search(r"^Experiment ID: (\d+)\s*$", outs, flags=re.MULTILINE)
    if exp_match is None:
        return None
    exp_id = exp_match.group(1)
    return int(exp_id)


def check_prover_success(szs_status: Optional[str], ltb_mode: bool) -> bool:
    # Function for returning whether a terminated prover run was successfull or not
    if szs_status is None:
        return False

    if ltb_mode:
        # LTB mode check (uses axiom selector)
        if re.match("(Theorem|Unsatisfiable)", szs_status):
            return True
        else:
            return False
    # Standard zsz ontology success check
    else:
        if re.match(
            "(Theorem|Unsatisfiable|Satisfiable|CounterSatisfiable)", szs_status
        ):
            return True
        else:
            return False


def get_szs_status_from_output_file(out_file) -> Optional[str]:
    with open(out_file, "r") as f:
        outs = f.read()
    return get_szs_status_from_output(outs)


def get_szs_status_from_output(outs) -> Optional[str]:
    szs_status = re.search("% SZS status.*", outs)
    if szs_status is None:
        return None

    # Extract the status
    return szs_status.group(0).split()[3]


def read_experiment_parameters() -> Dict[str, str | int]:
    logdir = get_current_logdir()
    with open(logdir / Path("experiment_params.json"), "r") as f:
        params = json.load(f)

    return params  # type: ignore


def store_experiment_parameters(
    problem_collection: str, wc_global: int, wc_local: int, loglevel: int
) -> None:
    """
    Store the key parameters in a file for the target function to read.
    """
    params = {
        "problem_collection": problem_collection,
        "wc_local": wc_local,
        "wc_global": wc_global,
        "loglevel": loglevel,
    }
    logdir = get_current_logdir()
    with open(logdir / Path("experiment_params.json"), "w") as f:
        json.dump(params, f)
