"""
Module containing common and general db queries.
"""

import os
from typing import List, Dict

from hos.database.base import query_select


def get_prover_executable_from_id(prover_id) -> str:
    query = f"""
             SELECT ProverExecutable
             FROM Prover
             WHERE ProverID={prover_id}
             ;"""

    res = query_select(query)
    return str(res[0][0])


def get_problem_relative_path(prob_version_id: int) -> str:
    query = f"""
               SELECT LD.Path AS Path, PV.Filename AS Filename
               FROM (ProblemVersion PV
                    INNER JOIN Problem P ON  PV.Problem = P.ProblemID)
                    INNER JOIN  LibraryDomain LD ON P.LibraryDomain =  LD.LibraryDomainID
               WHERE PV.ProblemVersionID={prob_version_id}
             ;"""
    res = query_select(query)[0]
    return str(os.path.join(res[0], res[1]))


def get_problem_ids_in_collection(problem_collection: str) -> List[int]:
    query = f"""
                 SELECT Problem
                 FROM CollectionProblems
                 WHERE Collection = (SELECT CollectionID
                                     FROM Collection
                                     WHERE CollectionName = "{problem_collection}")
                ORDER BY Problem
            ;"""
    res = query_select(query)
    ids = [int(r[0]) for r in res]
    return ids


def get_problem_filenames_in_collection(
    collection: str, library_name: str, library_version: str
) -> List[str]:
    '''
    query = f"""
            SELECT DISTINCT Filename
            FROM ProblemVersion
            WHERE Problem IN
            (
                SELECT CollectionProblemID
                FROM CollectionProblems
                WHERE Collection IN
                (
                    SELECT CollectionID
                    FROM Collection
                    WHERE CollectionName='{collection}'
                )
            )
            ; """
    '''
    query = f"""
            SELECT PV.Filename
            FROM ((((Library L INNER JOIN LibraryVersion LV ON L.LibraryID = LV.Library)
                    INNER JOIN ProblemVersion PV ON LV.LibraryVersionID = PV.Version)
                    INNER JOIN Problem P ON PV.Problem = P.ProblemID)
                    INNER JOIN LibraryDomain LD ON P.LibraryDomain = LD.LibraryDomainID)
                    INNER JOIN SZSStatus S ON PV.Status = S.SZSStatusID
                WHERE L.LibraryName = '{library_name}'
                AND LV.Version = '{library_version}'
                AND EXISTS
                    (
                    SELECT *
                    FROM CollectionProblems CP
                    INNER JOIN Collection C ON CP.Collection = C.CollectionID
                    WHERE PV.ProblemVersionID = CP.Problem
                    AND C.CollectionName IN ('{collection}')
                    )
                ;
            """
    res = query_select(query)

    filenames = [r[0] for r in res]
    return filenames


def get_filename_problem_id_map(
    problem_names: List[str], collection_name: str, reverse: bool = False
) -> Dict[str, int] | Dict[int, str]:
    # TODO might rename to something more intuitive
    # Quote each name and convert to list
    filenames = " ,".join(["'" + str(p) + "'" for p in problem_names])

    # Get the quality measure from this experiment
    query = f"""
                 SELECT Filename, ProblemVersionID
                 FROM ProblemVersion
                 WHERE Filename IN ({filenames})
                 AND ProblemVersionID
                 IN (
                       SELECT Problem
                       FROM CollectionProblems
                       WHERE Collection = (SELECT CollectionID FROM Collection WHERE CollectionName='{collection_name}')
                    )
                 ORDER BY Filename ASC
                 ;"""

    res = query_select(query)

    # Extract as dict
    if reverse:
        id_map = {int(p): f for f, p in res}
    else:
        id_map = {f: int(p) for f, p in res}
    return id_map


def get_filename_from_problem_ids_map(
    problem_ids: List[str], collection_name: str, reverse: bool = False
) -> Dict[str, int] | Dict[int, str]:
    # Get the quality measure from this experiment
    query = f"""
                 SELECT Filename, ProblemVersionID
                 FROM ProblemVersion
                 WHERE ProblemVersionID
                 IN (
                       SELECT Problem
                       FROM CollectionProblems
                       WHERE Collection = (SELECT CollectionID FROM Collection WHERE CollectionName='{collection_name}')
                    )
                 ORDER BY Filename ASC
                 ;"""

    res = query_select(query)

    # Extract as dict
    if reverse:
        id_map = {int(p): f for f, p in res}
    else:
        id_map = {f: int(p) for f, p in res}

    return id_map
