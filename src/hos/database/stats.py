"""
Module containing common queries related computing key statistics.
"""
from typing import Optional, List, Dict

from config import PROBLEM_COLLECTION_IS_LTB, INCLUDE_INCORRECT
from hos.database.base import query_select


def _base_query_solved_problemrun(
    select: str, exp_id: int, upper_time_bound: Optional[float]
):
    sql_query = f"""
        {select}
        FROM ProblemRun PR
        """

    # Join to get status information
    sql_query += """
        JOIN ProblemVersion PV ON PV.ProblemVersionID = PR.Problem
        JOIN SZSStatus SL ON PV.Status = SL.SZSStatusID
        JOIN SZSStatus SR ON PR.Status = SR.SZSStatusID
        """

    sql_query += f"""
            WHERE PR.Experiment={exp_id}
            """

    # Apply upper time bound if set
    if upper_time_bound is not None:
        sql_query += f"""
                AND PR.Runtime <= {upper_time_bound}
                """

    if PROBLEM_COLLECTION_IS_LTB:
        sql_query += """
             AND (PR.SZS_Status = 'Theorem'
             OR PR.SZS_Status = 'Unsatisfiable')
                     """
    else:
        sql_query += """
             AND SR.Solved = 1
                     """

    # Remove the incorrects by only including the correct
    if not INCLUDE_INCORRECT:
        sql_query += """
            AND ((SL.Unsat AND SR.Sat)
                OR (SL.Sat AND SR.Unsat)) = 0"""

    sql_query += ";"

    # Execute
    res = query_select(sql_query)
    return res


def query_no_solved_problems_in_exp(
    exp_id: int, upper_time_bound: Optional[int] = None
) -> int:
    # Get the number of solved problems
    select = "SELECT IFNULL(COUNT(*), 0)"
    res = _base_query_solved_problemrun(select, exp_id, upper_time_bound)
    return int(res[0][0])


def query_tot_time_in_exp(exp_id: int) -> int:
    # Get the cpu time of the experiment
    query = f"""
                 SELECT IFNULL(SUM(PR.Runtime), 0)
                 FROM ProblemRun PR
                 WHERE PR.Experiment={exp_id}
             ;"""

    res = query_select(query)
    return int(res[0][0])


def query_id_solved_problems_in_exp(exp_id: int, upper_time_bound=None) -> List[str]:
    # get the number of solved problems
    select = "SELECT PR.Problem"
    res = _base_query_solved_problemrun(select, exp_id, upper_time_bound)
    # Make it into a list
    res_list = [str(r[0]) for r in res]
    return res_list


def query_id_runtime_solved_problems_in_exp(
    exp_id: int, upper_time_bound: Optional[float] = None
) -> Dict[int, float]:
    # get the number of solved problems
    select = "SELECT PR.Problem, PR.Runtime"
    res = _base_query_solved_problemrun(select, exp_id, upper_time_bound)

    # Make into a dictionary
    res_dict = {int(r[0]): float(r[1]) for r in res}
    return res_dict


def query_heuristic_evaluation(exp_id: int) -> List[float | None]:
    # Start building a case query of select, by first setting
    # the errors and timeout to zero
    sql_query = """
        SELECT
        (
            CASE
            WHEN (SR.Solved=0) THEN NULL
        """

    # Set the incorrect problems to NULL
    if not INCLUDE_INCORRECT:
        sql_query += """
            WHEN ((SL.Unsat and SR.Sat) OR (SL.Sat and SR.Unsat)) THEN NULL
            """

    # Separate cases for LTB/Non LTB
    if PROBLEM_COLLECTION_IS_LTB:
        # Only include unsat problems and set all to NULL
        sql_query += """
           WHEN (PR.SZS_Status = 'Theorem' OR PR.SZS_Status = 'Unsatisfiable') THEN ROUND(PR.Runtime, 2)
           ELSE NULL
                     """
    else:
        # Non-ltb, we include all statuses
        sql_query += """
            ELSE ROUND(PR.Runtime, 2)
                     """
    # Case statement needs to have a variable otherwise library throws a unicode error
    sql_query += """
         END
         ) as res
                 """

    # Get the result from the problem run
    sql_query += """
        FROM ProblemRun PR
                 """

    # Get Status information
    sql_query += """
        join ProblemVersion PV ON PV.ProblemVersionID = PR.Problem
        join SZSStatus SL ON PV.Status = SL.SZSStatusID
        join SZSStatus SR ON PR.Status = SR.SZSStatusID
                """

    # Get experiment and order
    sql_query += """
        WHERE PR.Experiment={0}
        ORDER BY PR.Problem
                """.format(
        exp_id
    )
    sql_query += ";"

    res = query_select(sql_query)
    res = [r[0] for r in res]
    return res  # type: ignore


def _base_query_solved_problemrun_filter_collection(
    select: str, exp_id: int, collection: str, upper_time_bound: float
):
    sql_query = f"""
        {select}
        FROM ProblemRun PR
            INNER JOIN CollectionProblems CP
            ON CP.Problem=PR.Problem
        """

    # Get Status information
    sql_query += """
        JOIN ProblemVersion PV ON PV.ProblemVersionID = PR.Problem
        JOIN SZSStatus SL ON PV.Status = SL.SZSStatusID
        JOIN SZSStatus SR ON PR.Status = SR.SZSStatusID
        """

    # Filter on expeirment and collection
    sql_query += f" WHERE Experiment={exp_id} "
    sql_query += f" AND CP.Collection = (SELECT CollectionID FROM Collection WHERE CollectionName = '{collection}') "

    # Set upper time bound
    if upper_time_bound is not None:
        sql_query += f" AND PR.Runtime <= {upper_time_bound} "

    if PROBLEM_COLLECTION_IS_LTB:
        sql_query += """
             AND (PR.SZS_Status = 'Theorem'
             OR PR.SZS_Status = 'Unsatisfiable')
                     """
    else:
        sql_query += """
            AND SR.Solved=1
                     """

    # Remove the incorrect by only including the correct
    if not INCLUDE_INCORRECT:
        sql_query += """
            AND ((SL.Unsat AND SR.Sat)
                OR (SL.Sat AND SR.Unsat)) = 0"""

    # Execute
    sql_query += ";"
    res = query_select(sql_query)[0]
    return res


def query_heuristic_summary_solved_in_collection(
    exp_id: int, collection: str, upper_time_bound: float
):
    select_query = f"""SELECT IFNULL(PR.Experiment, {exp_id}),
                    Count(*),
                    IFNULL(SUM(PR.RunTime), 0)
                     """
    return _base_query_solved_problemrun_filter_collection(
        select_query, exp_id, collection, upper_time_bound
    )


def query_no_solved_problems_in_collection_in_exp(
    exp_id: int, collection: str, upper_time_bound: float
) -> int:
    # Get the number of solved problems
    select = "SELECT IFNULL(COUNT(*), 0)"
    res = _base_query_solved_problemrun_filter_collection(
        select, exp_id, collection, upper_time_bound
    )
    return int(res[0][0])
