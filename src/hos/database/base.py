"""
Module for representing an abstract layer between base operations and the db.
"""
import sys
from typing import List
import logging

import mysql.connector as db

log = logging.getLogger()


class DBError(Exception):
    pass


def _get_db_connection():
    # Connect to the database
    try:
        import db_cred
    except ImportError as err:
        print(err)
        print("Need to add database credentials dictionary in a `db_cred.py` file")
        sys.exit(1)

    # Get database connection details
    db_connection_details = db_cred.db_connection_details
    conn = db.connect(**db_connection_details)
    curs = conn.cursor()
    return conn, curs


def query_select(query: str):
    # Get the problem names of a heuristic
    conn = curs = None
    try:
        conn, curs = _get_db_connection()
        curs.execute(query)
        return curs.fetchall()

    except Exception as err:
        log.error(err)
        log.error(query)
        raise DBError(err)
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def query_commit(query: str) -> None:
    conn = curs = None
    try:
        conn, curs = _get_db_connection()
        curs.execute(query)
        conn.commit()

    except Exception as err:
        # print(err, query)
        # print(traceback.format_exc()) # FIXME
        log.error(err)
        log.error(query)
        print(query)
        raise DBError(err)
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def queries_update_autocommit(queries: List[str]):
    # Get the problem names of a heuristic
    conn = curs = None
    try:
        conn, curs = _get_db_connection()
        conn.autocommit = True  # Turn on autocommit

        for query in queries:
            curs.execute(query)

        return curs.fetchall()

    except Exception as err:
        log.error(err)
        log.error(query)
        print(query)
        raise DBError(err)
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()
