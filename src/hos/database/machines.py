"""
Module containing queries related to organising machines.
"""

from hos.database.base import query_commit, queries_update_autocommit


def release_set_of_machines(machines) -> None:
    # Function for releasing a set of machines after finishing the experiment
    query = f"""
                 UPDATE SMAC_ParallelExperimentMachines
                 SET Status="available"
                 WHERE Machines="{machines}"
                 AND PSMACExperimentID = (SELECT MAX(PSMACExperimentID)
                                                 FROM SMAC_ParallelExperiment)
                ;"""
    query_commit(query)


def lock_available_machine_set() -> str:
    """Requests a machine for an iProver experiment."""

    # TODO why this machine value?
    queries = [
        """
                 SET @node := "vip-hlp-evaluate"
               ;""",
        """
                 UPDATE SMAC_ParallelExperimentMachines SET Status = 'busy', Machines = (SELECT @node := Machines)
                 WHERE PSMACExperimentID = (SELECT MAX(PSMACExperimentID) FROM SMAC_ParallelExperiment)
                 AND Status = "Available"
                 ORDER BY Machines
                 LIMIT 1
               ;""",
        """
                 SELECT @node
               ;""",
    ]

    res = queries_update_autocommit(queries)
    return str(res[0][0])
