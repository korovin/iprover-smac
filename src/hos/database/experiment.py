"""
Module containing common queries related to Experiments.
"""
import time
from typing import List, Optional, Tuple

import config
from hos.database.base import query_select, query_commit


def get_problem_collection() -> str:
    """Get the problem collection of the current SMAC experiment."""
    query = """
             SELECT ProblemCollection
             FROM   SMAC_ProblemCollection
             WHERE  SMACExperimentID=(SELECT MAX(SMACExperimentID)
                                      FROM SMAC_ProblemCollection)
             ;"""
    res = query_select(query)[0][0]
    return str(res)


def remove_problems_from_queue(exp_id: int):
    """Remove problems from the experiment queue."""
    query = f"""
             DELETE FROM ExperimentProblem
             WHERE Experiment={exp_id}
             ;"""

    query_commit(query)


def _get_any_experiment_machine(exp_id: int) -> str:
    """Get the name of any machine in the given experiment."""

    query = f"""
            SELECT Machine
            FROM ExperimentMachine
            WHERE Experiment={exp_id} LIMIT 1
            ;"""
    res = query_select(query)
    return str(res[0][0])


def _insert_experiment_result(
    exp_id: int, machine_id: str, szs_str: str, szs_num: int, error_msg: str
) -> None:
    query = f"""
     INSERT INTO ProblemRun (Problem, Experiment, Runtime, Machine, SZS_Status, Status, StatusOutput)
     SELECT Problem, {exp_id}, 0.0, {machine_id}, "{szs_str}", {szs_num}, "{error_msg}"
     FROM ExperimentProblem
     WHERE Experiment={exp_id}
     ;"""
    query_commit(query)


def store_error_results(exp_id: int) -> None:
    """Insert errors in the DB for all the problems in the experiment."""
    machine_id = _get_any_experiment_machine(exp_id)
    _insert_experiment_result(exp_id, machine_id, "Error", 39, "Illegal Heuristic")


def store_unknown_results(exp_id: int) -> None:
    """Insert non-viable in the DB for all the problems in the experiment."""
    machine_id = _get_any_experiment_machine(exp_id)
    _insert_experiment_result(
        exp_id, machine_id, "Inappropriate", 52, "Non-viable heuristic"
    )


def get_experiment_library_path(exp_id: int) -> str:
    query = f"""
             SELECT LibraryPath
             FROM Experiment
             WHERE ExperimentID={exp_id}
            ;"""
    res = query_select(query)
    return str(res[0][0])


def create_problem_collection(
    problem_ids: List[int], base_problem_collection: str, max_collection_size: int
) -> str:
    # Init collection
    collection_name = time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime())
    query_create_collection = f"""
                 INSERT INTO Collection (CollectionName, CollectionDescription)
                 VALUES ("{collection_name}", "smac_hlp")
                 ;"""
    query_commit(query_create_collection)

    # Get the ID of the newly inserted problem collection
    query_collection_id = """
                 SELECT MAX(CollectionID)
                 FROM Collection
                 ;"""
    collection_id = query_select(query_collection_id)[0][0]

    # Make a new problem collection from on the base collection and cluster problem ids
    query_insert_collection_problems = f"""
                INSERT INTO CollectionProblems (Collection, Problem)
                SELECT {collection_id}, Problem
                FROM CollectionProblems
                WHERE Collection = (SELECT CollectionID
                                    FROM Collection
                                    WHERE CollectionName="{base_problem_collection}" LIMIT 1)
                AND Problem in ({", ".join([str(n) for n in problem_ids])})
                ORDER BY RAND() LIMIT {max_collection_size}
                ;"""
    query_commit(query_insert_collection_problems)

    return collection_name


def insert_end_time_hlp():
    query = """
            UPDATE HLP_Experiment
            SET HLP_Experiment.EndTime = (SELECT NOW())
            ORDER BY ExperimentID
            DESC LIMIT 1
            ;"""
    query_commit(query)


def delete_problem_collection_subsets():
    # Delete all the problem references
    query_delete_collection_problems = """
                 DELETE FROM CollectionProblems
                 WHERE Collection IN
                       (
                          SELECT CollectionID
                          FROM Collection
                          WHERE CollectionDescription = 'smac_hlp'
                          GROUP BY CollectionID)
                 ;"""
    query_commit(query_delete_collection_problems)

    # Delete the collection reference
    query_delete_collections = """
                 DELETE FROM Collection
                 WHERE CollectionDescription = 'smac_hlp'
                 ;"""
    query_commit(query_delete_collections)


def store_heuristic_run_data(
    best_heuristics: List[int],
    new_heuristics: Optional[List[int]],
    global_heuristics: List[int],
) -> None:
    if new_heuristics is not None and len(new_heuristics) == 0:
        new_heuristics = None

    def to_str(heuristics) -> str | None:
        if heuristics is None:
            return ""
        return ", ".join(str(h) for h in heuristics)

    query = f"""
                 INSERT INTO HLP_Run
                 (ExperimentID, BestHeuristics, NewHeuristics, GlobalHeuristics, Time)
                 VALUES ((SELECT MAX(ExperimentID) FROM HLP_Experiment),
                        "{to_str(best_heuristics)}",
                        "{to_str(new_heuristics)}",
                        "{to_str(global_heuristics)}",
                        NOW())
             ;"""
    query_commit(query)


# Get optimal options from incumbent or experiment
def get_experiment_options(experiment_id: int) -> List[Tuple[str, int | float | str]]:
    """
    Get the parameter values of a heuristics from the db.
    Queries each value column of the given experiment.
    """

    query_int_params = f"""
                 SELECT ParameterName, ParameterValueInt
                 FROM ExperimentProverParameters
                 WHERE ParameterValueInt IS NOT NULL
                 AND Experiment="{experiment_id}";
                 """
    value_int = query_select(query_int_params)

    query_bool_params = f"""
                 SELECT ParameterName, ParameterValueBool
                 FROM ExperimentProverParameters
                 WHERE ParameterValueBool IS NOT NULL
                 AND Experiment="{experiment_id}";
                 """
    value_bool = query_select(query_bool_params)
    # Convert boolean '0','1' to 'false','true' / This could have been done in the db query TODO
    for n in range(0, len(value_bool)):
        if value_bool[n][1]:
            value_bool[n] = (value_bool[n][0], "true")
        else:
            value_bool[n] = (value_bool[n][0], "false")

    query_string_params = f"""
                 SELECT ParameterName, ParameterValueString
                 FROM ExperimentProverParameters
                 WHERE ParameterValueString IS NOT NULL
                 AND Experiment="{experiment_id}";
                 """
    value_string = query_select(query_string_params)

    # ParameterValueFloat
    query_float_params = f"""

                 SELECT ParameterName, ParameterValueFloat
                 FROM ExperimentProverParameters
                 WHERE ParameterValueFloat IS NOT NULL
                 AND Experiment="{experiment_id}";
                 """
    value_float = query_select(query_float_params)

    # Combine all options into a list of tuples
    return value_int + value_bool + value_string + value_float  # type: ignore


def report_experiment_evaluation_id(experiment_id: int) -> None:
    """
    Create a link between which evaluation experiments that have been constructed in the experiment.
    """

    query = f"""
                 INSERT INTO HLP_EvaluationRun (HLP_ExperimentID, iProverExperimentID)
                 VALUES ((SELECT MAX(ExperimentID) FROM HLP_Experiment), "{experiment_id}")
                 ;"""
    query_commit(query)


def query_experiment_unsound_results(exp_id) -> List[Tuple[str]]:
    query = f"""
                SELECT ProblemName
                FROM ExperimentScoreboard
                WHERE Experiment={exp_id}
                AND Incorrect=1
            """
    # Supress error on sat problems
    if config.PROBLEM_COLLECTION_IS_LTB:
        query += """
                         AND SolvedKnownUnsat = 1
                         """
    query += ";"
    res = query_select(query)
    return res  # type: ignore


def get_number_of_problems_in_experiment(exp_id: int) -> int:
    query = f"""
                 SELECT COUNT(*)
                 FROM ExperimentRun
                 WHERE Experiment={exp_id}
                 ;"""
    res = query_select(query)
    return int(res[0][0])


def get_error_strings_in_experiment(exp_id: int) -> List[Tuple[str, str, str]]:
    query = f"""
                 SELECT P.ProblemName AS ProblemName, PR.Runtime AS Runtime, PR.StatusOutput AS Output
                 FROM (((Problem P INNER JOIN ProblemVersion PV ON P.ProblemID = PV.Problem)
                        INNER JOIN ProblemRun PR ON PV.ProblemVersionID = PR.Problem)
                       LEFT OUTER JOIN SZSStatus S ON PR.Status = S.SZSStatusID)
                      INNER JOIN Machine M ON PR.Machine = M.MachineID
                 WHERE PR.Experiment = {exp_id}
                 AND (S.Error = TRUE OR PR.Status IS NULL)
                 ORDER BY P.ProblemName
                 ;"""
    res = query_select(query)
    return res  # type: ignore


def get_smac_last_experiment_heuristics(
    exclude_heuristics: Optional[List[int]] = None,
) -> List[int]:
    if exclude_heuristics is not None:
        hfilter = ", ".join(map(str, exclude_heuristics))
    else:
        hfilter = ""

    # Get all the candidate heuristics, given the current experiment and the
    sql_query = f"""
                 SELECT WrapperRun.Experiment
                 FROM WrapperRun
                 INNER JOIN SMAC_ParamString
                 ON SMAC_ParamString.iProverExperimentID = WrapperRun.Experiment
                 WHERE WrapperRun.SMACExperiment = (SELECT MAX(SMACExperimentID)
                                                    FROM SMACExperiment)
                 AND SMAC_ParamString.ParamString NOT IN
                 (
                     SELECT ParamString
                     FROM SMAC_ParamString
                     WHERE iProverExperimentID IN ({hfilter})
                 )
                ;"""

    res = query_select(sql_query)
    heur = sorted(int(r[0]) for r in res)

    # Return each heuristic
    return heur
