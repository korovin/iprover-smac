"""
Module for representing SMAC Scenarios used to create the base context for optimisation.
"""
from smac import Scenario
from ConfigSpace import ConfigurationSpace


def get_scenario(
    config_space: ConfigurationSpace,
    n_smac_trials: int,
    n_parallel_workers: int,
    use_default_config: bool,
) -> Scenario:
    scenario = Scenario(
        config_space,
        deterministic=True,
        objectives="cost",
        n_trials=n_smac_trials,
        n_workers=n_parallel_workers,
        use_default_config=use_default_config,
    )

    return scenario
