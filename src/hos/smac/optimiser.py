"""
Module containing the key functionality for tying together the SMAC functionality and running instances of SMAC.
"""
import warnings
from typing import List, Tuple

from ConfigSpace import Configuration
from smac import AlgorithmConfigurationFacade as ACFacade
import logging

import config
from hos import logger
import hos.stats as stats
import hos.database as db
import hos.experiment_config as exp_conf
from hos.experiment import (
    check_iprover_experiment_errors,
    check_iprover_experiment_incorrect_result,
)
from hos.smac.parameter_space import get_smac_configuration_space
from hos.smac.scenario import get_scenario
from hos.stats import get_experiment_admissible_set_cover
from hos.target.eval_cost import QualityMetric

log = logging.getLogger()

N_WORKERS: int = 1


def run_smac(
    pcs_path: str,
    incumbent: int,
    n_smac_trials: int,
    n_parallel_workers: int,
    parameter_freeze,
    target_function=config.TARGET_FUNCTION,
    use_default_config: bool = False,
) -> Configuration | List[Configuration]:
    config_space = get_smac_configuration_space(pcs_path, incumbent, parameter_freeze)
    scenario = get_scenario(
        config_space, n_smac_trials, n_parallel_workers, use_default_config
    )
    with warnings.catch_warnings():
        smac_logger = logger.get_smac_logger()
        warnings.simplefilter("ignore")
        smac = ACFacade(
            scenario=scenario,
            target_function=target_function,
            overwrite=True,
            # logging_level=smac_logger.level,
            logging_level=False,  # This is set to false so the following logger (via ctx) can log properly
        )
        # Mute the root logger as SMAC keeps writing to both logs for some reason...
        # Probably due to the __name__ way of obtaining loggers.
        log.debug("Temporary muting the main/root logger")
        root_level = log.level
        log.setLevel(logging.ERROR)
        # Redirect the SMAC logs to a separate file
        with logger.LoggingContext(smac_logger):
            new_incumbent = smac.optimize()
        log.setLevel(root_level)

    if check_if_crashed_runs(smac):
        raise ValueError("Target function CRASHED during the smac run.")
    return new_incumbent


def check_if_crashed_runs(smac) -> bool:
    # Check if any of the target runs crashed.
    # This is a hack and may change at any time..
    if max(smac.runhistory._cost_per_config.values()) == float("inf"):
        log.error("At least one of the target runs CRASHED")
        return True

    return False


def get_local_incumbent(
    problem_collection: str, heuristics: List[int]
) -> Tuple[int, float]:
    # Get the time-out for computing
    timeout = exp_conf.get_local_timeout()

    # Compute the local optimal heuristic for the sample on the local timeout
    assert len(heuristics) > 0
    incumbent, incumbent_score = stats.compute_top_heuristic_for_collection(
        problem_collection, heuristics, timeout
    )
    # If no incumbent is found, use the initial heuristic
    if incumbent is None:
        log.warning(
            "No incumbent found amongst the heuristics, selecting the first heuristic in the provided list"
        )
        incumbent = heuristics[0]
        incumbent_score = db.stats.query_no_solved_problems_in_collection_in_exp(
            incumbent, problem_collection, timeout
        )

    return incumbent, incumbent_score


def pre_smac_experiment_db(
    pcs_path: str,
    problem_collection: str,
    initial_incumbent: int,
    quality_metric: QualityMetric,
) -> int:
    # Scenario path and smac version no longer relevant
    smac_exp_id = db.smac.initialise_smac_experiment_db("", "", pcs_path)
    db.smac.report_quality_function(smac_exp_id, quality_metric, initial_incumbent)
    db.smac.report_problem_collection(smac_exp_id, problem_collection)

    return smac_exp_id


def post_smac_experiment_db(smac_exp_id: int) -> None:
    db.smac.report_smac_experiment_end_time(smac_exp_id)


def discover_local_heuristics(
    pcs_path: str,
    problem_collection: str,
    global_heuristics: List[int],
    n_smac_trials: int,
    quality_metric: QualityMetric,
    admissible_constant: float,
    admissible_factor: float,
) -> List[int]:
    # FIXME - if adding freezing - need to add the loop back in here

    # Get the best local heuristic
    incumbent, incumbent_score = get_local_incumbent(
        problem_collection, global_heuristics
    )
    log.debug(
        f'For collection "{problem_collection}" retrieved incumbent {incumbent} with score {incumbent_score}.'
    )

    # TODO need to set up machines as well??
    smac_exp_id = pre_smac_experiment_db(
        pcs_path, problem_collection, incumbent, quality_metric
    )
    log.debug(f"Running SMAC experiment: {smac_exp_id}")
    run_smac(
        pcs_path, incumbent, n_smac_trials, N_WORKERS, None
    )  # FIXME set freezing to none for now
    post_smac_experiment_db(smac_exp_id)

    # Get heuristic results but remove previously evaluated heuristics
    new_evaluations = db.experiment.get_smac_last_experiment_heuristics(
        global_heuristics
    )
    # Check and report if any errors occurred while running the evaluation
    check_iprover_experiment_errors(new_evaluations)
    # check for soundness errors in the prover
    check_iprover_experiment_incorrect_result(new_evaluations)

    # Extract the best local heuristics from the evaluation
    local_heuristics = get_experiment_admissible_set_cover(
        new_evaluations, problem_collection, admissible_constant, admissible_factor
    )
    return local_heuristics
