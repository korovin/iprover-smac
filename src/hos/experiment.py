"""
Module containing the key abstractions for running and managing ATP experiments on the cluster.
"""
from typing import List
import logging

from hos import experiment_config as exp_conf
from hos.process import ExperimentProcess
from hos.smac.parameter_space import get_smac_configuration_space
from hos.target.heuristic import get_heuristic_from_db
import hos.database as db
from hos.target.translate_to_iprover import translate_argument_dict_to_heuristic

log = logging.getLogger()

MUTE_ERRORS_WITH_MESSAGE = [
    "Illegal Heuristic",
    "Exited with status 2. Fatal error: exception Signals.Time_out_real Fatal error: exception Signals.Time_out_real\n",
    "Exited with status 2. Fatal error: exception Lib.Termination_Signal Fatal error: exception "
    "Lib.Termination_Signal\n",
    "Exited with status 2.  ------ Preprocessing...Fatal error: exception Lib.Termination_Signal\n",
    "Exited with status 2.  ------ Preprocessing... sf_s Fatal error: exception Lib.Termination_Signal\n",
    'Exited with status 2. Fatal error: exception Failure("proof_search_loop: empty component list") Fatal error: '
    'exception Failure("proof_search_loop: empty component list")\n',
    "Killed with signal -7.",
]


def run_cluster_experiment(
    exp_config_file: str, problem_set: str, machines: str, interval: float = 2
) -> int:
    ep = ExperimentProcess(exp_config_file, problem_set, machines, mock_exp=False)
    ep.start()  # Only db and file creation
    ep.wait(interval=interval)
    exp_id = ep.get_experiment_id()
    errors = ep.check_errors()
    # log.warning("Re-insert clean")
    ep.clean()

    if errors:
        raise Exception("Prover ERROR")
    return exp_id


def evaluate_heuristics_on_problem_collection(
    heuristics: List[int],
    problem_collection: str,
    machines: str,
    insert_smac_param_string: bool = True,
) -> List[int]:
    # Lists the new globally evaluated heuristics
    global_ids: List[int] = []

    for local_id in heuristics:
        log.info(f"Evaluating on: {local_id}")

        # Get the heuristic from the experiment id
        heuristic = get_heuristic_from_db(local_id)

        # Create the experiment config file
        timeout = exp_conf.get_global_timeout()
        exp_config = exp_conf.get_global_experiment_config(
            heuristic.get_prover_options(timeout)
        ).to_file()

        # Run the experiment on the global problem set
        global_id = run_cluster_experiment(exp_config, problem_collection, machines)

        # Report to logger
        log.info(f"Returned: {global_id}")

        # Get the iProver experiment ID
        global_ids.append(global_id)

        # Report to db
        db.experiment.report_experiment_evaluation_id(global_id)

        if insert_smac_param_string:
            # Insert previous SMAC HLP string to allow warmstarting from a global heuristic
            db.smac.insert_smac_param_string_evaluation(global_id, local_id)

    # Check and report if any errors occurred while running the evaluation
    check_iprover_experiment_errors(global_ids)

    # check for soundness errors in the prover
    check_iprover_experiment_incorrect_result(global_ids)

    return global_ids


def check_iprover_experiment_incorrect_result(exp_ids: List[int]) -> bool:
    """
    Check for unsoundness in the prover by SZS status
    """

    unsound = False
    for exp in exp_ids:
        res = db.experiment.query_experiment_unsound_results(exp)

        # We had some incorrect results, report
        if len(res) > 0:
            unsound = True
            for error in res:
                print(f"Experiment: {exp} INCORRECT Problem: {error[0]}")

    return unsound


def check_iprover_experiment_problems_ran(exp_ids: List[int]) -> bool:
    """
    Checks whether any experiment ran without any problems. Highly suspicious if so.
    """

    error = False
    for exp in exp_ids:
        res = db.experiment.get_number_of_problems_in_experiment(exp)
        if res == 0:
            error = True
            print(f"Exp: {exp}: No problems ran")

    return error


def check_iprover_experiment_error_messages(exp_ids: List[int]) -> None:
    # The muted error messages
    for exp in exp_ids:
        # Get error messages for the experiment
        res_status = db.experiment.get_error_strings_in_experiment(exp)

        if len(res_status) == 0:
            return

        # Check if we have error messages
        if len(res_status) > 0:
            for prob, time, msg in res_status:
                if (
                    msg not in MUTE_ERRORS_WITH_MESSAGE
                    and "Killed with signal -10." not in msg
                ):
                    print(f"Exp: {exp}  Problem: {prob}  Runtime: {time} msg: {msg}")


def check_iprover_experiment_errors(exp_ids: List[int]) -> None:
    check_iprover_experiment_problems_ran(exp_ids)
    check_iprover_experiment_error_messages(exp_ids)


def evaluate_default_heuristic_configuration(
    problem_collection: str, pcs_file: str, machines: str
) -> int:
    # Load the default configuration
    default_config = get_smac_configuration_space(
        pcs_file, incumbent=None, parameter_freeze=None
    ).get_default_configuration()

    # Convert to a heuristic - ensure all values are strings
    inp_params = {k: str(v) for k, v in dict(default_config).items()}
    heuristic = translate_argument_dict_to_heuristic(inp_params)

    # Create the experiment config file
    timeout = exp_conf.get_global_timeout()
    exp_config = exp_conf.get_global_experiment_config(
        heuristic.get_prover_options(timeout)
    ).to_file()

    # Run the experiment
    exp_id = run_cluster_experiment(exp_config, problem_collection, machines)

    # Check for errors
    # FIXME should be grouped into a single function? - calling this repeatedly
    check_iprover_experiment_errors([exp_id])
    check_iprover_experiment_incorrect_result([exp_id])

    return exp_id
