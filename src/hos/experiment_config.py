"""
Module and class for handling the slightly annoying experiment_config files
used for running online experiments on the clusters.
"""

from pathlib import Path
from datetime import datetime
from typing import Dict, Optional

from hos.utils import get_tmp_out_file, read_experiment_parameters
import hos.database as db

# Run environment variable for holding the wall clocks and respective grace
RUN_ENV: Optional[Dict[str, Dict[str, int]]] = None

# TODO should move this out src
# Experiment Environment
# Maybe move to some json as well? Or yaml or toml??
EXP_ENV: Dict[str, int | str] = {
    # "prover": 5,
    # "prover": 10,
    # "prover": 937,
    "prover": 938,
    "memory_limit": 10000,  # 10GB
    "stack_size": 200,  # 200MB
    "environment": "TPTP=/shareddata/TPTP-v8.1.2/",
    "library_path": "/shareddata/TPTP-v8.1.2/Problems",
    "library": "TPTP",
    "library_version": "v8.1.2",
    "problem_forms": '"FOF", "CNF"',
    # "problem_forms": "",
}


class ExperimentConfig:
    def __init__(
        self,
        prover: int,
        timeout_wallclock: int,
        memory_limit: int,
        stack_size: int,
        environment: str,
        library_path: str,
        library: str,
        library_version: str,
        problem_forms: str,
        wallclock_grace: int,
        prover_options: str,
    ):
        self.prover = prover
        self.timeout_wallclock = timeout_wallclock
        self.memory_limit = memory_limit
        self.stack_size = stack_size
        self.environment = environment
        self.library_path = library_path
        self.library = library
        self.library_version = library_version
        self.problem_forms = problem_forms
        self.wallclock_grace = wallclock_grace
        self.prover_options = prover_options

    def _to_str(self) -> str:
        # TODO should this be __str__?
        # TODO should this be changed to __str__?
        """Return the object as a str in the format used by the cluster-scripts"""
        exp_str = (
            f"# {datetime.now().strftime('%m/%d/%Y, %H:%M:%S')}\n"
            f"timeout_wallclock={self.timeout_wallclock + self.wallclock_grace}\n"
            f"memory_limit={self.memory_limit}\n"
            f"stack_size={self.stack_size}\n\n"
            f'environment="{self.environment}"\n\n'
            f'library_path="{self.library_path}"\n'
            f'library="{self.library}"\n'
            f'library_version="{self.library_version}"\n'
            f"problem_forms={self.problem_forms}\n\n"
            f"prover={self.prover}\n"
            f"prover_options={self.prover_options}\n"
        )
        return exp_str

    def to_file(self) -> str:
        tmp_file = get_tmp_out_file()
        with open(tmp_file, "w") as fp:
            fp.write(self._to_str())

        return tmp_file

    def get_timeout(self) -> float:
        return self.timeout_wallclock


def set_run_env() -> None:
    param_dict = read_experiment_parameters()
    global RUN_ENV
    RUN_ENV = {
        "local": {
            "timeout_wallclock": int(param_dict["wc_local"]),
            "wallclock_grace": 2,
        },
        "global": {
            "timeout_wallclock": int(param_dict["wc_global"]),
            "wallclock_grace": 5,
        },
    }


def _get_env(mode):
    if RUN_ENV is None:
        set_run_env()
        assert RUN_ENV

    return {**RUN_ENV[mode], **EXP_ENV}


def get_global_experiment_config(prover_options: str) -> ExperimentConfig:
    return ExperimentConfig(prover_options=prover_options, **_get_env("global"))


def get_local_experiment_config(prover_options: str) -> ExperimentConfig:
    return ExperimentConfig(prover_options=prover_options, **_get_env("local"))


def get_local_timeout() -> int:
    return int(_get_env("local")["timeout_wallclock"])


def get_global_timeout() -> int:
    return int(_get_env("global")["timeout_wallclock"])


def get_prover_id() -> int:
    return int(EXP_ENV["prover"])


def get_prover_exec() -> str:
    """Return the prover executable path for the current experiment."""
    # Get the prover id
    prover_id = get_prover_id()

    try:
        # Query db for the path to the executable
        prover_exec = db.general.get_prover_executable_from_id(prover_id)
    except Exception:
        # log.warning("Attempting to use ~/iprover/iproveropt instead of experiment prover")
        prover_exec = str(Path.home() / "iprover" / "iproveropt")

    return str(prover_exec)
