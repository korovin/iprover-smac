"""
Module for assessing the viability of a heuristic - whether it can solve the easy problems in a cluster.
"""
import os
import logging
from typing import List, Optional

from hos.process import ProverProcess
from hos.target.experiment import run_mock_experiment, ResultStatus
from hos.target.heuristic import Heuristic
import hos.database as db
from hos.utils import check_prover_success, get_szs_status_from_output_file

log = logging.getLogger()

RUNTIME_EASY_PROBLEMS = 0.8
NUMBER_OF_EASY_PROBLEMS = 4
MAX_RUNTIME_VIABLE = 1


def check_heuristic_viable_on_problem(
    heuristic: Heuristic, problem_path: str, prover_path: Optional[str] = None
) -> bool:
    # Create cmd string
    pp = ProverProcess(
        heuristic.get_heuristic_cmd(RUNTIME_EASY_PROBLEMS),
        problem_path,
        prover_path=prover_path,
    )

    pp.start()
    pp.wait(interval=0.2)
    if pp.check_errors():
        return False

    # Get the SZS status
    szs_status = get_szs_status_from_output_file(pp.out_file)
    pp.clean()

    if szs_status is None:
        log.debug("Could not find szs status in output")
        return False

    # Get whether the problem was solved - any solution is success
    return check_prover_success(szs_status, ltb_mode=False)


def get_easy_problems(incumbent: int) -> List[str]:
    # Get problem ids that are solved under the given time bound
    problems = list(
        db.stats.query_id_runtime_solved_problems_in_exp(
            incumbent, RUNTIME_EASY_PROBLEMS
        ).keys()
    )
    problems = problems[:NUMBER_OF_EASY_PROBLEMS]  # Truncate

    # Get the library path
    library_path = db.experiment.get_experiment_library_path(incumbent)

    # Get the path of all problems
    paths: List[str] = []
    for i, prob in enumerate(problems):
        paths.append(
            str(os.path.join(library_path, db.general.get_problem_relative_path(prob)))
        )

    return paths


def check_viability_on_problems(
    heuristic: Heuristic, problems: List[str], prover_path: Optional[str] = None
) -> bool:
    # Test each problem in the set
    for prob in problems:
        solved = check_heuristic_viable_on_problem(heuristic, prob, prover_path)
        # Test failed for one of the problems - return false
        if not solved:
            log.info("Viability test FAILED")
            return False

    log.info("Viability test PASSED")
    return True


def check_heuristic_viability(
    heuristic: Heuristic, prover_path: Optional[str] = None
) -> bool:
    # Get incumbent
    incumbent = db.smac.get_current_smac_experiment_incumbent()
    # Get the problems based on the incumbent
    problems = get_easy_problems(incumbent)

    # If there are not enough easy problems - the test is invalid so the heuristic is viable
    if len(problems) < NUMBER_OF_EASY_PROBLEMS:
        log.warning("Not enough easy problems found. Skipping viability test")
        return True

    # Run the viability test over each problem
    viable = check_viability_on_problems(heuristic, problems, prover_path)
    return viable


def handle_nonviable_heuristic(exp_config_file, problem_set, machines) -> int:
    exp_id = run_mock_experiment(
        exp_config_file, problem_set, machines, ResultStatus.UNKNOWN
    )

    return exp_id
