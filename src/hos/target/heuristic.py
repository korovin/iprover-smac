"""
Class to represent a heuristic for use in an online experiment.
"""
import re
from typing import Union, Optional, Dict

import hos.database as db


class Heuristic:
    def __init__(self):
        self.options = {}

    def add_option(self, option, value):
        if option in self.options:
            raise ValueError(
                f"Tried adding option {option}  which exists in the heuristic."
            )
        self.options[option] = value

    def get_no_options(self) -> int:
        return len(self.options)

    def get_prover_options(
        self, timeout: float, extra_options: Optional[Dict[str, str]] = None
    ) -> str:
        """Return heuristic in the experiment config format."""
        self._set_timeout(timeout)

        if extra_options is None:
            extra_options = dict()

        options = ", ".join(
            f'"--{opt} {val}"' for opt, val in {**self.options, **extra_options}.items()
        )
        return options

    def get_heuristic_cmd(
        self, timeout: float, extra_options: Optional[Dict[str, str]] = None
    ) -> str:
        """Return heuristic for use as a cmd for use in a shell process."""
        self._set_timeout(timeout)

        if extra_options is None:
            extra_options = dict()
        # Return the heuristic as for use as a cmd line argument
        return " ".join(
            f"--{opt} '{val}'" for opt, val in {**self.options, **extra_options}.items()
        )

    def get_option_value(self, op) -> Union[str, float, None]:
        # Return None if not in the dict
        return self.options.get(op, None)

    def any_solver_is_selected(self) -> bool:
        """Check if any of the solvers (calculi) are selected, required for iProver."""
        return not (
            self.get_option_value("instantiation_flag") == "false"
            and self.get_option_value("resolution_flag") == "false"
            and self.get_option_value("superposition_flag") == "false"
        )

    def _set_timeout(self, timeout: float) -> None:
        """Sets the timeout for the current options."""

        self.options["time_out_real"] = timeout

        # Update clausifier options
        claus_opt = "clausifier_options"
        if claus_opt in self.options:
            value = re.sub(
                "-t [0-9]+([,.][0-9]+)?",
                f"-t {timeout:.2f}",
                self.options[claus_opt],
            )
            self.options[claus_opt] = value


def get_heuristic_from_db(exp_id: int) -> Heuristic:
    op = db.experiment.get_experiment_options(exp_id)
    op = [(param[2:], val) for param, val in op]  # Remove double dash

    # Build the heuristic from the options - assume everything is the same
    heur = Heuristic()
    # Add all the values
    for param, val in op:
        # Add the value
        heur.add_option(param, val)  # Remove double dash
    return heur
