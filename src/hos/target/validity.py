"""
Module for evaluating the validity a heuristic, i.e. whether it can be parsed by the ATP.
"""
from dataclasses import dataclass
from typing import Optional
import logging
from pathlib import Path

from hos import logger
from hos.process import run_prover_process
from hos.target.heuristic import Heuristic
import config
from hos.target.experiment import run_mock_experiment, ResultStatus

log = logging.getLogger()


@dataclass
class HeuristicValidityCheck:
    """Class for storing results from validity test."""

    is_valid: bool
    msg: Optional[str]
    returncode: int
    cmd: Optional[str] = None


def create_counter_file():
    # Ensure path exists
    logdir = logger.get_current_logdir()
    # Path(config.invalid_heuristic_counter).parent.mkdir(parents=True, exist_ok=True)
    try:
        with open(logdir / Path(config.invalid_heuristic_counter), "x") as f:
            f.write(str(0))
    except FileExistsError:
        pass


def increment_invalid_heuristic_counter():
    # Increment the heuristic counter
    logdir = logger.get_current_logdir()
    with open(logdir / Path(config.invalid_heuristic_counter), "r+") as f:
        counter = f.read().split()[0]
        counter = int(counter) + 1  # type: ignore
        f.seek(0)
        f.write(str(counter))
    log.debug(f"Invalid heuristic counter incremented to: {counter}")


def log_invalid_heuristic(exp_id: int, check: HeuristicValidityCheck):
    # Append data to the log file
    logdir = logger.get_current_logdir()
    with open(logdir / Path(config.invalid_heuristic_log), "a") as f:
        f.write(f"Exp: {exp_id} ExitCode: {check.returncode} ErrorMsg: {check.msg}")


def check_heuristic_validity(
    heuristic: Heuristic, prover_path: Optional[str] = None
) -> HeuristicValidityCheck:
    if not heuristic.any_solver_is_selected():
        # The heuristic is invalid if no heuristic is selected
        log.debug("SMAC did not select a solver, heuristic is invalid")
        return HeuristicValidityCheck(
            is_valid=False, msg="No solver selected (inst/res/sup)", returncode=1
        )

    return check_if_heuristic_parses(heuristic, prover_path)


def check_if_heuristic_parses(
    heuristic: Heuristic, prover_path
) -> HeuristicValidityCheck:
    # Create cmd string
    heuristic_str = heuristic.get_heuristic_cmd(
        timeout=1, extra_options={"dbg_just_parse": "true"}
    )
    # Do not want to throw process errors for this - if log level is not debug
    # Might create a context for this later if it becomes recurring
    curr_loglevel = logging.root.level
    if logging.DEBUG < curr_loglevel:
        log.debug("Silencing all non-critical log messages")
        logging.getLogger().setLevel(logging.CRITICAL)
    pp = run_prover_process(heuristic_str, config.TEST_PROBLEM, 0.3, prover_path)
    logging.getLogger().setLevel(curr_loglevel)
    log.debug(f"Restored log level to {curr_loglevel}")

    assert (
        pp.errors is not None and pp.returncode is not None
    )  # and pp.out_err is not None and pp.returncode is not None # FIXME
    return HeuristicValidityCheck(
        is_valid=not pp.errors, msg=pp.out_err, cmd=pp.cmd, returncode=pp.returncode
    )


def handle_invalid_heuristic(
    exp_config_file: str,
    problem_set: str,
    machines: str,
    validity_check: HeuristicValidityCheck,
) -> int:
    """Store invalid heuristic data in db and increment the invalid counter."""

    exp_id = run_mock_experiment(
        exp_config_file, problem_set, machines, ResultStatus.ERROR
    )
    log.debug(f"{validity_check.returncode} {validity_check.msg}")

    # Increment the counter to keep track of the number of invalid cases
    increment_invalid_heuristic_counter()
    log_invalid_heuristic(exp_id, validity_check)

    return exp_id


def init_invalid_heuristic_counter() -> None:
    # Set the invalid heuristic counter to zero
    create_counter_file()


def check_reached_no_invalid_heuristics(max_count: Optional[int]) -> bool:
    # Ignore if none
    if max_count is None:
        return False

    # Get the counter
    logdir = logger.get_current_logdir()
    with open(logdir / Path(config.invalid_heuristic_counter), "r+") as f:
        counter = int(f.read().split()[0])

    # Return result
    return counter > max_count
