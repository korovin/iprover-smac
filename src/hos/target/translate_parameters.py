"""
Module containing a collection of functions for parsing SMAC parameters into heuristic options.
"""
from typing import Dict, List, Optional
from hos.target.heuristic import Heuristic

# List separator string constant
LSEP = ";"


def translate_param(
    heur: Heuristic, param_dict: Dict[str, str], option: str, value
) -> None:
    """Add a single standard parameter to the heuristic."""

    heur.add_option(option, value)
    del param_dict[option]


def translate_param_flag(
    heur: Heuristic,
    param_dict: Dict[str, str],
    option: str,
    value: str,
    subtags: Optional[List[str]] = None,
) -> None:
    """Add a parameter represented with a flag. Also removes subtags related to the flag if provided."""

    heur.add_option(option, value)
    del param_dict[f"{option}_flag"]

    if subtags is None:
        return

    for tag in subtags:
        if f"{option}_{tag}" in param_dict:
            del param_dict[f"{option}_{tag}"]


def extract_list_non_order_boolean_param(
    heur: Heuristic, param_dict, prefix, simp_rules: List[str]
) -> None:
    """Extract indicies parameters from superposition which are constrained to boolean parameters."""

    values = []

    # Check which indices that are set to true
    for rule in simp_rules:
        # Check if rule is specified and true
        rule_tag = f"{prefix}_{rule}"
        if rule_tag in param_dict and param_dict[rule_tag] == "true":
            values += [rule]

    value_str = f"[{LSEP.join(values)}]"
    heur.add_option(prefix, value_str)

    # Clean up
    del param_dict[f"{prefix}_flag"]
    for rule in simp_rules:
        rule_tag = f"{prefix}_{rule}"
        if rule_tag in param_dict:
            del param_dict[rule_tag]


def extract_nested_list_parameters(
    heur: Heuristic, param_dict: Dict[str, str], prefix: str
) -> None:
    """Extracting queue parameters of the form [[..];..;[..;..;..]] with an associated frequency parameter."""

    # Nested list of queue values
    queue_values = []
    # The queue frequency string
    freq_values = []

    # Get the number of queues
    queue_tag = f"{prefix}_len"
    no_queues = param_dict[queue_tag]
    del param_dict[queue_tag]

    # Construct each queue
    for queue_no in range(1, int(no_queues) + 1):
        # Get the picking frequency of this queue
        queue_freq_tag = f"{prefix}_{str(queue_no)}_freq"
        freq_values += [param_dict[queue_freq_tag]]
        del param_dict[queue_freq_tag]

        # Get number of parameters in the queue and start constructing
        queue_length_tag = f"{prefix}_{str(queue_no)}_len"
        queue_length = param_dict[queue_length_tag]
        del param_dict[queue_length_tag]

        values = []  # Values of the current queue
        for param_no in range(1, int(queue_length) + 1):
            # Obtain the parameter
            queue_value_tag = f"{prefix}_metric_{queue_no}_{param_no}"
            values += [param_dict[queue_value_tag]]
            del param_dict[queue_value_tag]

        queue_values += [values]

    # Compute intermediate string rep for better readability
    queue_values_str = ["[" + LSEP.join(q) + "]" for q in queue_values]
    # Add queues
    heur.add_option(prefix, f"[{LSEP.join(queue_values_str)}]")
    # Add frequency
    heur.add_option(f"{prefix}_freq", f"[{LSEP.join(freq_values)}]")


def extract_list_parameters(
    heur: Heuristic, param_dict: Dict[str, str], prefix: str
) -> None:
    """Extracting list parameters of the form [..;..;..]."""
    values = []
    list_length = param_dict[f"{prefix}_len"]
    del param_dict[f"{prefix}_len"]

    for param_no in range(1, int(list_length) + 1):
        tag = f"{prefix}_metric_{param_no}"
        values += [param_dict[tag]]
        del param_dict[tag]

    heur.add_option(prefix, f"[{LSEP.join(values)}]")
