"""
Module for translating SMAC generated parameters to an iProver heuristic.
"""
import logging
from typing import Dict

from config import (
    STATIC_EXPERIMENT_OPTIONS,
    PROBLEMS_IN_TFF,
    CLAUSIFIER_OPTIONS,
    CLAUSIFIER_PATH,
)
from hos.target.heuristic import Heuristic
from hos.target.translate_parameters import (
    translate_param,
    extract_nested_list_parameters,
    extract_list_parameters,
    extract_list_non_order_boolean_param,
    translate_param_flag,
)

log = logging.getLogger()


def get_heuristic_from_options(param_dict: Dict[str, int | str | float]) -> Heuristic:
    heur = Heuristic()

    for param in list(param_dict.keys()):
        heur.add_option(param, param_dict[param])
        del param_dict[param]

    add_clausifier_params(heur, {})
    return heur


def translate_argument_dict_to_heuristic(
    param_dict: Dict[str, str], ignore_empty_check=False
) -> Heuristic:
    # Translate parameters from the parameter dictionary to an iProver heuristic to use in cluster experiments
    heur = Heuristic()

    # Add options from the parameter dict
    add_general_parameters(heur, param_dict)
    add_prep_params(heur, param_dict)
    add_qbf_params(heur, param_dict)
    add_inst_params(heur, param_dict)
    add_res_params(heur, param_dict)
    add_sup_params(heur, param_dict)
    add_abstr_params(heur, param_dict)
    add_sat_params(heur, param_dict)
    add_clausifier_params(heur, param_dict)

    # Add static options
    add_static_experiment_options(heur)

    if not ignore_empty_check:
        check_if_param_dict_is_empty(param_dict)

    return heur


def add_static_experiment_options(heur: Heuristic) -> None:
    """Add any static parameters specified in config."""
    for opt in STATIC_EXPERIMENT_OPTIONS.items():
        heur.add_option(*opt)


def check_if_param_dict_is_empty(param_dict: Dict[str, str]) -> None:
    # It is expected that all the parameters in param_dict are translated
    if len(param_dict) > 0:
        # Report that not all parameters were converted
        log.warning(
            f"Number of parameters not translated/removed when constructing experiment heuristic: {len(param_dict)}"
        )
        log.warning(
            f"Parameters left: {'  '.join(f'{k}::{param_dict[k]}' for k in sorted(param_dict.keys()))}"
        )
        raise ValueError("Argument dict is not empty!")


def add_general_parameters(heur: Heuristic, param_dict) -> None:
    """Translate a bunch of ungrouped parameters"""

    if "-bc_imp_inh" in param_dict:
        if param_dict["-bc_imp_inh"] == "conj_cone":
            translate_param(heur, param_dict, "bc_imp_inh", "conj_cone")
        else:
            translate_param(heur, param_dict, "bc_imp_inh", "[]")

    for p in (
        "add_important_lit",
        "clause_weak_htbl",
        "large_theory_mode",
        "prolific_symb_bound",
        "lt_threshold",
        "comb_mode",
        "comb_res_mult",
        "comb_inst_mult",
        "comb_sup_mult",
        "comb_sup_deep_mult",
        "conj_cone_tolerance",
        "bmc1_symbol_reachability",
        "prop_solver_per_cl",
        "extra_neg_conj",
        "subs_bck_mult",
        "share_sel_clauses",
    ):
        if p in param_dict:
            translate_param(heur, param_dict, p, param_dict[p])


def add_clausifier_params(heur: Heuristic, param_dict) -> None:
    # Set the clausifier path
    heur.add_option("clausifier", CLAUSIFIER_PATH)

    # Start building the option string - Add the clausification mode
    claus_str = "--mode "
    if PROBLEMS_IN_TFF:
        claus_str += "tclausify "
    else:
        claus_str += "clausify "

    # Add timeout placeholder
    claus_str += f"-t {300:.2f} "

    # Add any extra clausifier options
    if len(CLAUSIFIER_OPTIONS) > 0:
        claus_str += CLAUSIFIER_OPTIONS + " "

    # Check if sine is in use
    p = "clausifier_sine_flag"
    if p in param_dict and param_dict[p] == "true":
        # Extract the parameters for SiNE and insert
        sine_sd = param_dict["clausifier_sine_sd"]
        sine_st = param_dict["clausifier_sine_st"]
        claus_str += f"-ss axioms -sd {sine_sd} -st {float(sine_st):.1f} "

        del param_dict["clausifier_sine_flag"]
        del param_dict["clausifier_sine_sd"]
        del param_dict["clausifier_sine_st"]

    # Add clausifier to the heuristic
    heur.add_option("clausifier_options", claus_str.strip())


def add_prep_params(heur: Heuristic, param_dict) -> None:
    """Add preprocessing Parameters to a Heuristic"""

    # Check if the preprocessing is in use
    if "preprocessing_flag" not in param_dict:
        return

    if param_dict["preprocessing_flag"] == "true":
        translate_param(heur, param_dict, "preprocessing_flag", "true")
    else:
        translate_param(heur, param_dict, "preprocessing_flag", "false")
        return

    # Prep params
    for p in (
        "time_out_prep_mult",
        "splitting_mode",
        "splitting_grd",
        "splitting_cvd",
        "splitting_cvd_svl",
        "splitting_nvd",
        "sub_typing",
        "prep_gs_sim",
        "prep_unflatten",
        "prep_res_sim",
        "prep_sup_sim_sup",
        "prep_sup_sim_all",
        "prep_upred",
        "prep_sem_filter",
        "prep_sem_filter_out",
        "pred_elim",
        "res_sim_input",
        "eq_ax_congr_red",
        "pure_diseq_elim",
        "prep_def_merge",
        "prep_def_merge_prop_impl",
        "prep_def_merge_mbd",
        "prep_def_merge_tr_red",
        "prep_def_merge_tr_cl",
        "smt_preprocessing",
        "smt_ac_axioms",
        "preprocessed_out",
        "preprocessed_stats",
    ):
        if p in param_dict:
            translate_param(heur, param_dict, p, param_dict[p])


def add_qbf_params(heur: Heuristic, param_dict) -> None:
    # Check if qbf is in use
    if "qbf_mode" not in param_dict:
        return

    if param_dict["qbf_mode"] == "true":
        translate_param(heur, param_dict, "qbf_mode", "true")
    else:
        translate_param(heur, param_dict, "qbf_mode", "false")
        return

    # QBF Parameters
    for p in (
        "qbf_elim_univ",
        "qbf_dom_inst",
        "qbf_dom_pre_inst",
        "qbf_sk_in",
        "qbf_pred_elim",
        "qbf_split",
    ):
        if p in param_dict:
            translate_param(heur, param_dict, p, param_dict[p])


def add_sat_params(heur: Heuristic, param_dict: Dict[str, str]) -> None:
    # Check if instantiation is in use
    # If not we return the empty string
    if "sat_mode" not in param_dict:
        return

    if param_dict["sat_mode"] == "true":
        translate_param(heur, param_dict, "sat_mode", "true")
    else:
        translate_param(heur, param_dict, "sat_mode", "false")
        return

    # Instantiation Standard Parameters
    for p in (
        "sat_epr_types",
        "sat_non_cyclic_types",
        "sat_finite_models",
        "sat_fm_lemmas",
        "sat_fm_prep",
        "sat_fm_uc_incr",
    ):
        if "" + p in param_dict:
            translate_param(heur, param_dict, p, param_dict[p])


def add_inst_params(heur: Heuristic, param_dict) -> None:
    # Check if instantiation is specified with the flag
    if "instantiation_flag" not in param_dict:
        return

    if param_dict["instantiation_flag"] == "true":
        translate_param(heur, param_dict, "instantiation_flag", "true")
    else:
        translate_param(heur, param_dict, "instantiation_flag", "false")
        return

    # Instantiation Standard Parameters
    for p in (
        "inst_lit_sel_side",
        "inst_solver_per_active",
        "inst_solver_calls_frac",
        "inst_passive_queue_type",
        "inst_dismatching",
        "inst_eager_unprocessed_to_passive",
        "inst_prop_sim_given",
        "inst_prop_sim_new",
        "inst_subs_new",
        "inst_eq_res_simp",
        "inst_subs_given",
        "inst_orphan_elimination",
        "inst_learning_loop_flag",
        "inst_learning_start",
        "inst_learning_factor",
        "inst_start_prop_sim_after_learn",
        "inst_sel_renew",
        "inst_lit_activity_threshold",
        "inst_restr_to_given",
        "inst_activity_threshold",
        "inst_lit_activity_flag",
        "inst_to_smt_solver",
    ):
        if p in param_dict:
            translate_param(heur, param_dict, p, param_dict[p])

    # Instantiation passive queue parameters
    if "inst_passive_queues_1_len" in param_dict:
        extract_nested_list_parameters(heur, param_dict, "inst_passive_queues")

    # Instantiation literal selection
    if "inst_lit_sel_len" in param_dict:
        extract_list_parameters(heur, param_dict, "inst_lit_sel")

    # Check if the inst_sos section is described
    if "inst_sos_flag" not in param_dict:
        return

    if param_dict["inst_sos_flag"] == "false":
        translate_param(heur, param_dict, "inst_sos_flag", "false")
    elif param_dict["inst_sos_flag"] == "true":
        translate_param(heur, param_dict, "inst_sos_flag", "true")
        # Set the phase parameter
        if "inst_sos_phase" in param_dict:
            translate_param(
                heur, param_dict, "inst_sos_phase", param_dict["inst_sos_phase"]
            )
        # Get the queue
        if "inst_sos_sth_lit_sel_len" in param_dict:
            extract_list_parameters(heur, param_dict, "inst_sos_sth_lit_sel")


def add_res_params(heur: Heuristic, param_dict) -> None:
    # Check if resolution is in use
    # If not we return the empty string
    if "resolution_flag" not in param_dict:
        return

    if param_dict["resolution_flag"] == "true":
        translate_param(heur, param_dict, "resolution_flag", "true")
    else:
        translate_param(heur, param_dict, "resolution_flag", "false")

    # Resolution Standard Parameters
    for p in (
        "res_lit_sel",
        "res_lit_sel_side",
        "res_ordering",
        "res_to_prop_solver",
        "res_prop_simpl_new",
        "res_prop_simpl_given",
        "res_passive_queue_type",
        "res_forward_subs_resolution",
        "res_backward_subs_resolution",
        "res_orphan_elimination",
        "res_to_smt_solver",
    ):
        if p in param_dict:
            translate_param(heur, param_dict, p, param_dict[p])

    # Res subs parameter (they have a special length[n] parameter)
    for p in ("res_forward_subs", "res_backward_subs"):
        if p in param_dict:
            if param_dict[p] == "length":
                translate_param(heur, param_dict, p, f"length[{param_dict[p]}]")
            else:
                translate_param(heur, param_dict, p, param_dict[p])

    # Resolution time parameter
    if "res_time_limit" in param_dict:
        translate_param(
            heur,
            param_dict,
            "res_time_limit",
            f"{float(param_dict['res_time_limit']):.2f}",
        )

    # Resolution passive queue parameters
    if "res_passive_queues_1_len" in param_dict:
        extract_nested_list_parameters(heur, param_dict, "res_passive_queues")


def add_sup_params(heur: Heuristic, param_dict) -> None:
    # Translates the superposition parameters from SMAC over to parameter options compatible with iProver.

    # Check if superposition is in use if not exit
    if "superposition_flag" not in param_dict:
        return

    if param_dict["superposition_flag"] == "true":
        translate_param(heur, param_dict, "superposition_flag", "true")
    else:
        translate_param(heur, param_dict, "superposition_flag", "false")

    # Superposition Standard Parameters
    for p in (
        "sup_passive_queue_type",
        "demod_completeness_check",
        "demod_use_ground",
        "sup_unprocessed_bound",
        "sup_to_prop_solver",
        "sup_prop_simpl_new",
        "sup_prop_simpl_given",
        "sup_fun_splitting",
        "sup_iter_deepening",
        "sup_restarts_mult",
        "sup_score",
        "sup_share_score_frac",
        "sup_share_max_num_cl",
        "sup_ordering",
        "sup_symb_ordering",
        "sup_term_weight",
        "sup_full_fixpoint",
        "sup_main_fixpoint",
        "sup_immed_fixpoint",
        "sup_input_fixpoint",
        "sup_cache_sim",
        "sup_smt_interval",
        "sup_bw_gjoin_interval",
    ):
        if p in param_dict:
            translate_param(heur, param_dict, p, param_dict[p])

    # Superposition passive queue parameters
    if "sup_passive_queues_1_len" in param_dict:
        extract_nested_list_parameters(heur, param_dict, "sup_passive_queues")

    # ######## Superposition Simplification Setup ###########

    # # Superposition Indices ##
    simp_indices = [
        "UnitSubsumption",
        "NonunitSubsumption",
        "Subsumption",
        "FwDemod",
        "BwDemod",
        "LightNorm",
        "FwACDemod",
        "BwACDemod",
        "SMTIncr",
        "SMTSet",
        "BwGjoin",
    ]
    p = "sup_indices_passive"
    if f"{p}_flag" in param_dict:
        if param_dict[f"{p}_flag"] == "true":
            extract_list_non_order_boolean_param(heur, param_dict, p, simp_indices)
        else:
            translate_param_flag(heur, param_dict, p, "[]", subtags=simp_indices)

    # # Superposition Triv ##
    simp_triv = ["PropSubs", "Unflattening", "SMTSimplify"]
    for p in ("sup_full_triv", "sup_immed_triv", "sup_input_triv"):
        if f"{p}_flag" in param_dict:
            if param_dict[f"{p}_flag"] == "true":
                extract_list_non_order_boolean_param(heur, param_dict, p, simp_triv)
            else:
                translate_param_flag(heur, param_dict, p, "[]", subtags=simp_triv)

    # # Superposition FW ##
    simp_fw = [
        "Subsumption",
        "SubsumptionRes",
        "UnitSubsAndRes",
        "FullSubsAndRes",
        "SMTSubs",
        "Demod",
        "LightNorm",
        "ACJoinability",
        "ACNormalisation",
        "ACDemod",
        "GroundJoinability",
        "Connectedness",
    ]
    for p in ("sup_full_fw", "sup_immed_fw_main", "sup_immed_fw_immed", "sup_input_fw"):
        if f"{p}_flag" in param_dict:
            if param_dict[f"{p}_flag"] == "true":
                extract_list_non_order_boolean_param(heur, param_dict, p, simp_fw)
            else:
                translate_param_flag(heur, param_dict, p, "[]", subtags=simp_fw)

    # # Superposition BW ##
    simp_bw = [
        "Subsumption",
        "SubsumptionRes",
        "UnitSubsAndRes",
        "FullSubsAndRes",
        "Demod",
        "ACDemod",
    ]
    for p in ("sup_full_bw", "sup_immed_bw_main", "sup_immed_bw_immed", "sup_input_bw"):
        if f"{p}_flag" in param_dict:
            if param_dict[f"{p}_flag"] == "true":
                extract_list_non_order_boolean_param(heur, param_dict, p, simp_bw)
            else:
                translate_param_flag(heur, param_dict, p, "[]", subtags=simp_bw)


def add_abstr_params(heur: Heuristic, param_dict) -> None:
    """Translates the abstraction parameters"""

    # Extract the abstr_ref_under parameters
    # We extract these first as it is somewhat separate from abstr_ref(over)
    # This is now modeled as two categorical variables, cone and []
    if "abstr_ref_under" in param_dict:
        # Needed to be handled differently as new pcs parser does not allow lists in the suntac
        if param_dict["abstr_ref_under"] == "cone":
            abstr_val = "[cone]"
        elif param_dict["abstr_ref_under"] == "none":
            abstr_val = "[]"
        else:
            raise ValueError(
                f'No abstr_ref_under value implemented for: "{param_dict["abstr_ref_under"]}"'
            )
        translate_param(heur, param_dict, "abstr_ref_under", abstr_val)

    # Check if abstr  is in use - otherwise exit
    if "abstr_ref_len" not in param_dict:
        return

    # If ref is empty we also exit
    if param_dict["abstr_ref_len"] == "0":
        # Special parameter so added by hand
        heur.add_option("abstr_ref", "[]")
        del param_dict["abstr_ref_len"]
        return

    # Extract the abstr_ref parameters
    if "abstr_ref_len" in param_dict:
        extract_list_parameters(heur, param_dict, "abstr_ref")

    # Abstraction Standard Parameters
    for p in (
        "abstr_ref_prep",
        "abstr_ref_until_sat",
        "abstr_ref_sig_restrict",
        "abstr_ref_af_restrict_to_split_sk",
    ):
        if p in param_dict:
            translate_param(heur, param_dict, p, param_dict[p])
