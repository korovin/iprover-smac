"""
Module containing high-level abstractions of the special cases of target experiments.
"""
from enum import Enum, auto

from hos import database as db
from hos.process import ExperimentProcess


class ResultStatus(Enum):
    ERROR = auto()
    UNKNOWN = auto()


def store_result(exp_id: int, result: ResultStatus) -> None:
    if result is ResultStatus.ERROR:
        db.experiment.store_error_results(exp_id)
    elif result is ResultStatus.UNKNOWN:
        db.experiment.store_unknown_results(exp_id)
    else:
        raise ValueError(f"Not implemented support for ResultStatus {result}")


def run_mock_experiment(
    exp_config_file: str, problem_set: str, machines: str, result: ResultStatus
) -> int:
    # Create an experiment but do not run it
    exp_id = initialise_experiment_in_db(exp_config_file, problem_set, machines)
    # Fill the experiment results with all errors for this experiment
    store_result(exp_id, result)
    # Remove problems
    db.experiment.remove_problems_from_queue(exp_id)

    return exp_id


def initialise_experiment_in_db(
    exp_config_file: str, problem_set: str, machines: str, interval: float = 0.1
) -> int:
    ep = ExperimentProcess(exp_config_file, problem_set, machines, mock_exp=True)
    ep.start()  # Only db and file creation
    ep.wait(interval=interval)
    exp_id = ep.get_experiment_id()
    ep.clean()
    return exp_id
