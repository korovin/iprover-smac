"""
Module containing key functionality for computing stats related to evaluation.
"""
from typing import List, Optional, Set, Dict, Tuple
import numpy as np
import pandas as pd
import logging

import hos.experiment_config as exp_conf
import hos.database as db
from hos.admissible import (
    get_heuristic_evaluation_data,
    compute_admissible_evaluation_matrix,
)
from hos.experiment_config import get_global_timeout

log = logging.getLogger()


def compute_and_report_evaluation_performance_stats(
    global_heuristics: List[int], initial_heuristics: List[int]
) -> None:
    # Get the runtime of the global evaluation to assert the upper bound
    timeout = get_global_timeout()

    # Compute the measurements
    solved_union = len(compute_solved_union(global_heuristics, timeout))
    solved_intersection_avg_time = compute_solved_intersection_avg_time(
        global_heuristics, initial_heuristics, timeout
    )
    solved_complement_to_default = compute_solved_complement_to_initial(
        global_heuristics, initial_heuristics, timeout
    )
    solved_problems_best_heuristic = compute_solved_problems_best_heuristic(
        global_heuristics, timeout
    )

    # Report measurements to db
    db.smac.insert_hlp_evaluation_statistics(
        global_heuristics,
        solved_union,
        solved_intersection_avg_time,
        solved_complement_to_default,
        solved_problems_best_heuristic,
    )


def compute_solved_complement_to_initial(
    heuristics: List[int], initial: List[int], upper_time_bound: float
) -> int:
    # Compute solved in the separate heuristic sets
    heuristic_solved = compute_solved_union(
        heuristics, upper_time_bound, exclude=initial
    )
    initial_solved = compute_solved_union(initial, upper_time_bound)

    # Compute and return the size of the difference
    return len(set(heuristic_solved) - set(initial_solved))


def compute_solved_problems_best_heuristic(
    heuristics: List[int], upper_time_bound: float
) -> int:
    # List to store the number of problems solved
    solved_list: List[int] = []

    # Get the number of problems solved by each heuristic
    for heuristic in heuristics:
        solved_list += [
            len(db.stats.query_id_solved_problems_in_exp(heuristic, upper_time_bound))
        ]

    # Return the max number of problems solved
    return max(solved_list)


def compute_solved_union(
    heuristics: List[int], upper_time_bound: float, exclude: Optional[List[int]] = None
) -> Set[str]:
    solved_list: List[str] = []
    for heuristic in heuristics:
        if exclude is None or heuristic not in exclude:
            # Get the ids of the solved problems
            res = db.stats.query_id_solved_problems_in_exp(heuristic, upper_time_bound)
            solved_list += res

    # Return number of problems solved in union
    return set(solved_list)


def compute_fastest_solutions(
    heuristics: List[int], upper_time_bound: float
) -> Dict[int, float]:
    runtime_dict: Dict[int, float] = {}
    for heuristic in heuristics:
        heur_runtimes = db.stats.query_id_runtime_solved_problems_in_exp(
            heuristic, upper_time_bound
        )
        for prob, runtime in heur_runtimes.items():
            runtime_dict[prob] = min(runtime_dict.get(prob, float("inf")), runtime)

    return runtime_dict


def compute_solved_intersection_avg_time(
    heuristics: List[int], initial: List[int], upper_time_bound: float
) -> float:
    fastest = compute_fastest_solutions(heuristics, upper_time_bound)

    solved_initial = compute_solved_union(initial, upper_time_bound)

    avg_inter_runtime = float(
        np.mean([fastest[int(prob)] for prob in solved_initial])
    )  # TODO FIXME are the ids correct, e.g. matching?
    return avg_inter_runtime


def compute_top_heuristic_for_collection(
    collection: str, heuristics: List[int], upper_time_bound: float
) -> Tuple[int | None, int]:
    """
    Computes the top heuristic amongst a set of heuristics for a given problem collection and upper time
    bound, according to the maximum solving time with the minimum runtime.
    """

    # Summaries for each heuristic on this cluster
    heuristic_summary = []

    # Compute summary of cluster for each heuristic
    for heuristic in heuristics:
        # Get the summary
        sql_query_summary = db.stats.query_heuristic_summary_solved_in_collection(
            heuristic, collection, upper_time_bound
        )

        # Get the summary
        heuristic_summary += [sql_query_summary]

    # Compute the best heuristic in cluster
    best_heuristic, solved, solved_time = sorted(
        heuristic_summary, key=lambda tup: (-tup[1], tup[2], tup[0])
    )[0]

    if int(solved) == 0:
        # If the heuristic does not solve anything, return none.
        return None, -1

    return best_heuristic, int(solved)


def get_experiment_admissible_set_cover(
    heuristics: List[int],
    problem_collection: str,
    admissible_constant: float,
    admissible_factor: float,
) -> List[int]:
    # Compute the admissible dictionary
    admissible_sets = get_problems_admissible_by_heuristics(
        heuristics, problem_collection, admissible_constant, admissible_factor
    )

    # Greedily compute the set cover
    cover, universe_size = compute_greedy_min_cover(admissible_sets)
    log.info(f"Cover size is {len(cover)} and solves {universe_size} problems\n")
    for exp in cover:
        log.info(
            f"New heuristic {exp} solves: {db.stats.query_no_solved_problems_in_exp(exp)}"
        )

    return cover


def compute_set_universe(sets: Dict[int, Set[int]]) -> Set[int]:
    universe: Set[int] = set()
    for v in sets.values():
        universe = universe.union(v)
    return universe


def get_top_heuristic_minimimal_solving_time_over_problems(
    heuristics: List[int], subsets: Dict[int, Set[int]], heuristic_filter: Set[int]
) -> int:
    time_pairs: List[Tuple[int, float]] = []
    for heur in heuristics:
        # Get the problems in the required cover for this heuristic
        cover_problems = subsets[heur] - heuristic_filter

        # Get all solving times for this heuristic
        solved_times = db.stats.query_id_runtime_solved_problems_in_exp(heur)

        # Get the total solving time of the problems in the required cover
        time = sum([solved_times[p] for p in cover_problems])

        # Add the time pair to the list
        time_pairs += [(heur, time)]

    # Sort pairs and get heuristic the heuristic with the lowest time
    best = sorted(time_pairs, key=lambda tup: tup[1])[0][0]
    return best


def compute_time_greedy_set_cover(
    universe: Set[int], subsets: Dict[int, Set[int]]
) -> List[int]:
    """
    Compute the greedy set cover where heuristics with an equal cover size are selected based on minimal runtime
    """

    covered: Set[int] = set()
    heuristic_cover: List[int] = []

    while covered != universe:
        # Find the current max covers
        max_cover_val = 0
        max_cover: List[int] = []
        for k, v in subsets.items():
            cover_val = len(v - covered)
            if cover_val == max_cover_val:
                max_cover.append(k)
            elif max_cover_val < cover_val:
                max_cover_val = cover_val
                max_cover = [k]

        if len(max_cover) == 1:
            # If only one heuristic in the cover - add it
            selected_heuristic = max_cover[0]
        else:
            # Multiple with the same cover - get the best according to solving time
            selected_heuristic = get_top_heuristic_minimimal_solving_time_over_problems(
                max_cover, subsets, covered
            )

        # Add the result
        heuristic_cover.append(selected_heuristic)
        covered = covered.union(subsets[selected_heuristic])
        del subsets[selected_heuristic]

    return heuristic_cover


def compute_greedy_min_cover(solved_dict: Dict[int, Set[int]]) -> Tuple[List[int], int]:
    universe = compute_set_universe(solved_dict)

    cover = compute_time_greedy_set_cover(universe, solved_dict)

    return cover, len(universe)


def compute_admissible_problem_dict(adm_df: pd.DataFrame) -> Dict[int, Set[int]]:
    res = {}
    for heur in adm_df.columns:
        res[heur] = set(adm_df.loc[adm_df[heur] == 1].index)
    return res


def get_problems_admissible_by_heuristics(
    heuristics: List[int],
    problem_collection: str,
    admissible_constant: float,
    admissible_factor: float,
) -> Dict[int, Set[int]]:
    eval_df = get_heuristic_evaluation_data(
        heuristics, problem_collection, exp_conf.get_global_timeout()
    )

    # Compute the admissible evaluation
    adm_df = compute_admissible_evaluation_matrix(
        eval_df, admissible_constant, admissible_factor
    )

    admissible_sets = compute_admissible_problem_dict(adm_df)
    return admissible_sets
