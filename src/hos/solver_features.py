"""
Module for computing, processing and loading problem properties, and solver statics
outputted from the prover into solver state features.
"""
import multiprocessing
import os
import re
from typing import Dict, List, Optional
import numpy as np
import pandas as pd
from pathlib import Path
from sklearn.preprocessing import StandardScaler
import logging

import hos.database as db
from hos.experiment_config import get_prover_exec, EXP_ENV
from hos.process import run_solver_stats_extraction
from hos.target.translate_to_iprover import (
    get_heuristic_from_options,
)
from hos.utils import check_prover_success, get_szs_status_from_output

log = logging.getLogger()

FEATURE_COMPUTATION_TIME_LIMIT: float = 1
FEATURE_COMPUTATION_BASE_HEURISTIC: Dict[str, float | str | int] = {
    "stats_out": "all",
    # "preprocessing_flag": "false",
    "schedule": "none",
    "time_out_real": FEATURE_COMPUTATION_TIME_LIMIT,
    "out_options": "none",
    "proof_out": "false",
}
PROVER_STATS_SECTIONS = [
    "General",
    "Preprocessing",
    "Problem properties",
    "Propositional Solver",
    "Instantiation",
    "Resolution",
    "Superposition",
    "Simplifications",
]

REMOVE_PROBLEMS_SOLVED_DURING_FEATURE_EXTRACTION: bool = True
MAX_PARSING_TIME: float = 1.1

FEATURE_BASE_PATH: str = "res/solver_features/"
Path(FEATURE_BASE_PATH).mkdir(exist_ok=True)


# TODO drop columns with errors?


class LogScaler:
    """
    Helper class for conforming log scaling to scikit learn format
    """

    def _init_(self):
        return self

    def fit(self, X):
        # No parameters
        pass

    def fit_transform(self, X):
        return self.transform(X)

    def transform(self, X):
        return np.log1p(X)


FEATURE_PROCESSING_PIPELINE = [LogScaler(), StandardScaler()]  # TODO slightly bad form


def fit_feature_processors(X):
    for proc in FEATURE_PROCESSING_PIPELINE:
        proc.fit(X)


def process_features(X):
    X = np.asarray(X, dtype=np.float64)
    for proc in FEATURE_PROCESSING_PIPELINE:
        X = proc.transform(X)
    return X


def get_solver_features_file_path(problem_collection: str, prover_paths: str) -> str:
    prover_name = Path(prover_paths).stem
    path = Path(FEATURE_BASE_PATH) / Path(
        f"{problem_collection}_{prover_name}_time_{FEATURE_COMPUTATION_TIME_LIMIT:.1f}.csv"
    )
    return str(path)


def save_solver_features_as_csv(file_path: str, df: pd.DataFrame) -> None:
    df.to_csv(file_path, index=True)


def get_problem_paths(
    problem_collection: str, library_path: Optional[str] = None
) -> List[str]:
    if library_path is None:
        library_path = str(EXP_ENV["library_path"])

    # Get problem paths from the database
    filenames = db.general.get_problem_filenames_in_collection(
        problem_collection, str(EXP_ENV["library"]), str(EXP_ENV["library_version"])
    )

    # Join the paths and check that all exists
    problem_paths: List[str] = []
    for prob in filenames:
        if "library" in EXP_ENV and EXP_ENV["library"] == "TPTP":
            # Add domain to filename for TPTP
            prob_path = Path(library_path) / Path(prob[:3]) / Path(prob)
        else:
            prob_path = Path(library_path) / Path(prob)
        if not prob_path.exists():
            log.warning(f"Cannot find problem path for problem {prob}: {prob_path}")
        problem_paths.append(str(prob_path))
    return problem_paths


def convert_output_dicts_to_df(
    res_dict: Dict[str, Dict[str, bool | float | int]]
) -> pd.DataFrame:
    df = pd.DataFrame(res_dict).T
    log.debug(
        f"Created dataframe from results: problems:{len(df.index)} heuristics:{len(df.columns)}"
    )
    return df


def run_prover_and_compute_statistics(
    problem_paths: List[str],
) -> Dict[str, Dict[str, int | float | bool]]:
    # Get the prover here to save db queries
    prover_exec = get_prover_exec()

    heur = get_heuristic_from_options(FEATURE_COMPUTATION_BASE_HEURISTIC)
    heur_str = heur.get_heuristic_cmd(FEATURE_COMPUTATION_TIME_LIMIT)

    # Add the prover options
    k_args = [
        (prover_exec, heur_str, prob, FEATURE_COMPUTATION_TIME_LIMIT)
        for prob in problem_paths
    ]
    pool = multiprocessing.Pool(processes=os.cpu_count() - 1)  # type: ignore

    res = pool.starmap(run_and_extract_features_from_problem, k_args)
    pool.close()
    pool.join()

    return {k: v for d in res for k, v in d.items()}  # Join into a single dictionary


def extract_stats_from_output(prover_out: str) -> Dict[str, float]:
    # Dict to hold the stats
    stats: Dict[str, float] = {}

    for stat_section in PROVER_STATS_SECTIONS:
        # Get the parameters in the section
        match = re.search(
            rf"^------ {stat_section}\n((\s|\S)*?)(^-----|\Z)",
            prover_out,
            flags=re.MULTILINE,
        )
        if match is None:
            return {}

        try:
            # Get the first group
            match = match.group(1)  # type: ignore
            res = match.split()  # type: ignore
        except AttributeError:
            # Prover did not terminate properly on the problem
            return {}

        # Extract the values
        res = dict(zip(res[::2], res[1::2]))

        # Remove the colon from the name key
        res = dict([k[:-1], v] for k, v in res.items())

        for k, v in res.items():
            try:
                # Try to make the string repr into a float
                res[k] = float(v)
            except ValueError:
                # Some values are "undef", but they are ultimately zero
                res[k] = 0.0

        # Add this section to the dictionary
        stats = {**stats, **res}

    return stats


def run_and_extract_features_from_problem(
    prover_path: str, heur_str: str, problem: str, time_limit: float
) -> Dict[str, Dict[str, int | bool | float]]:
    # TODO integration test on this with a new prover?
    prover_out, errs = run_solver_stats_extraction(
        heur_str, problem, prover_path, time_limit + 0.1
    )

    # Extract statistics from the output
    stats_out = extract_stats_from_output(prover_out)
    stats_out["solved"] = check_prover_success(
        get_szs_status_from_output(prover_out), ltb_mode=False
    )
    stats_out["errors"] = errs

    # Return the statistics
    return {Path(problem).name: stats_out}


def compute_and_load_features(problem_collection: str, file_path: str) -> pd.DataFrame:
    # Get the problem paths
    problem_paths = get_problem_paths(problem_collection)

    # Compute the statistics for each problem
    results_dict = run_prover_and_compute_statistics(problem_paths)

    # Convert into a dataframe
    df = convert_output_dicts_to_df(results_dict)

    # Remove problems where feature computation failed
    df = remove_problems_with_incomplete_features(df)

    # Filter static uninformative features
    df = filter_static_features(df)
    assert len(df) > 0, "Need to have at least a single feature"

    # Save features file
    save_solver_features_as_csv(file_path, df)

    # Return the result
    return df


def load_features_as_dataframe(file_path: str) -> pd.DataFrame:
    df = pd.read_csv(file_path, index_col=0)
    log.debug(
        f"Loading solver feature file: {file_path} with {len(df.columns)} features and {len(df.index)} problems."
    )
    return df


def remove_problems_solved_during_extraction(df: pd.DataFrame) -> pd.DataFrame:
    curr_len = len(df)
    df = df.loc[df["solved"] != 1]
    log.info(
        f"Number of problems solved during feature computation: {curr_len - len(df)}"
    )
    return df


def filter_problems_on_parsing_time(df: pd.DataFrame) -> pd.DataFrame:
    curr_len = len(df)
    df = df.loc[df["parsing_time"] <= MAX_PARSING_TIME]
    log.info(
        f"Number of problems above the max parsing time {MAX_PARSING_TIME}: {curr_len - len(df)}"
    )
    return df


def remove_problems_with_incomplete_features(df: pd.DataFrame) -> pd.DataFrame:
    curr_len = len(df)
    df = df.dropna(axis="columns", how="any")

    log.info(
        f"Number of problems removed due to incomplete features: {curr_len - len(df)}"
    )
    return df


def filter_static_features(df: pd.DataFrame) -> pd.DataFrame:
    """
    Function for removing features that are always the same value.
    """
    curr_len = len(df.columns)
    num_col = df.select_dtypes(include="number").columns

    drop_col = df[num_col].loc[:, np.isclose(0, df[num_col].var())]
    df = df.drop(drop_col, axis="columns")

    log.info(f"Number of static features: {curr_len - len(df.columns)}")
    return df


def filter_problem_entries(df: pd.DataFrame) -> pd.DataFrame:
    curr_len = len(df)

    # Remove problems solved during extraction
    if REMOVE_PROBLEMS_SOLVED_DURING_FEATURE_EXTRACTION:
        df = remove_problems_solved_during_extraction(df)

    # Remove problems which took too long to parse
    if MAX_PARSING_TIME is not None:
        df = filter_problems_on_parsing_time(df)

    # Return the resulting data frame
    log.info(
        f"Removed {curr_len - len(df)} problems, resulting in {len(df)} problems after filtering"
    )
    return df


def remove_meta_features(df: pd.DataFrame) -> pd.DataFrame:
    """
    Drop columns containing meta features like solved, and szs status
    """
    drop = ["solved", "szs_status", "errors", "out_proof_time", "total_time"]
    df = df.drop(labels=drop, axis=1, errors="ignore")
    return df


def get_solver_state_features(
    problem_collection: str, file_path: Optional[str] = None
) -> pd.DataFrame:
    if file_path is None:
        file_path = get_solver_features_file_path(problem_collection, get_prover_exec())

    if os.path.exists(file_path):
        # If the features exist - load it from disk
        df = load_features_as_dataframe(file_path)
    else:
        # The features are not computed - compute them and return the result
        df = compute_and_load_features(problem_collection, file_path)

    # Filter the dataframe entries based on factors like clausification and processing times
    df = filter_problem_entries(df)

    df = remove_meta_features(df)

    # FIXME - slight hack as this feature is counted slightly weird and can be negative
    if "pred_elim_cl" in df:
        df["pred_elim_cl"] = df["pred_elim_cl"].abs()

    # Return the df
    return df
