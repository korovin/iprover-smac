------KK

* ./confugure to compile smac

* mv wrappers/db_cred_dummy.py to wrappers/db_cred.py and change

* smac paths etc. are in config.py, change them accordingly

* run iprover-smac:

  cd wrappers/
  nohup python3 smac.py > nohup.smac &

* files to change before each smac run:

 1. smac/config/scenario.txt:
     pcs-file = /shareddata/home/korovin/iprover-smac/smac/config/parameter_space_epr_nhorn_eq.pcs

   and the parameter file  /shareddata/home/korovin/iprover-smac/smac/config/parameter_space_epr_nhorn_eq.pcs

 2. wrappers/config.py

    problem_collection='tptp_v6.3_hard_epr_nhorn_eq'

    optionally, to restore a previous model change:
      restore_scenario_param

* problem collections are defined in
  problem_collections/

------------------
cluster_scripts configure file:

  wrappers/experiment_config
  change there new prover version

-------------------
logs:   smac/logs/
models: smac/output/

--------------------

Wrappers:
    Note: all scripts were written for Python 3, but they should work
          (perhaps with slight modifications) with Python 2

    smac.py:
        * Description:
            This script can be used to start a SMAC experiment.
            Roughly put, it:
                1) Adds a new entry to the SMACExperiment table in the database
                2) Starts SMAC and waits for it to finish
                3) After SMAC has finished, it updates the SMACExperiment
                   entry it added at the beginning
        * Requirements:
            * MySQL Connector/Python
              (see https://dev.mysql.com/downloads/connector/python/ for
              download and installation instructions
              for Ubuntu add package python3-mysql.connector)

            * MySQL database with the appropriate SMAC tables
              (see SQL Scripts below)
        * Installation: not needed
        * Usage:
            * The script can be called like standard Python script, e.g.:
                    python3 smac.py
            * Optional parameters:
                * --warmstart <tracedir>
                    Warmstarts a run of SMAC from a previous run. Need to provide
                    the tracedirectory which usually is in the output folder of
                    the program and named "state-run{N}" where N is the experiment
                    number of the experiment to warmstart from
                * --collection_subset <sizeOfSubSet> <maxIteration>
                    Runs iProver over random subsets of the problems in the specified problem collection.
                    The first argument defines the size of the set (cannot be larger than the ProblemColelction).
                    The second parameter specifies how many iterations each subset is used, before a fresh subset is created.
                * --options_subset  <1|0 update file> <original_param_file> <subset_section> <defined_subsets_file>
                    Optimises a subset of iProver parameters. The first argument 1|0 tells whether to update the options
                    (from the bets incumbent in the previous experiment). original_param_file is a JSON file containing all the
                    prover parameters for the specified prover + the SMAC specific parameters (queues, lit selection).
                        # Run create_parameter_space_template.py to generate this file (see how in its main method)
                    defined_subsets_file is used to specify which subset you would like to optimise (the rest are used with
                    their default values). The subsections are named by their array id in the json file. The values to optimise
                    are then listed. Use the section name for optimising SMAC specific parameters
                * --options_subset_random <1|0 update file> <original_param_file> <subset_size>
                    Same as options_subset except optimises over a randomly generated subset. The SMAC specialised options (inst queue, res queue etc)
                    are kept are considered 1 option, to ensure no illegal configurations.
                * --quality <qualityFunction>
                    Changes the quality function which SMAC minimises over. The default function is to maximise number of solved
                    problems.
                    The available functions are
                    * avg_rating (maximise the average TPTP rating/ranking/hardness of the problems solved)
                    * solved_time (minimise the time used on the solved problems, rounded to nerest 10 second interval)
                    * unique_solved (maximise problems only solved by this strategy, which is hard and therefore mostly returns 0)
                    * total_solved [default] (maximise number of problems solved in the set)
                * --set_iprover_time <time_out_in_seconds>
                    Sets the time_out_teal option for the iProver experiments. If not supplied the value is taken from
                    --timeout_wallclock in experiment_config. The dea behind this parameter is to affect the SMAC quality measures.
            * There are a number of hard-coded paths and names in the script,
              which most likely will need to be changed, e.g. by editing the
              script with a text editor:
                * Lines 10-15: Database connection details
                * Lines 33 and 80: SMAC scenario file
                   smac/config/scenario.txt
                   runcount-limit = 100  number of runs of wrapper scripts/configurations (other limits are possible)

                * Line 37: SMAC PCS file
                      smac/config/parameter_space.pcs

                * Line 44: Log file
                   smac/logs/2017-06-20_13-49-29.log

                * Line 76: SMAC jars

    smac_parallel.py:
        * Description:
            This script is used to run SMAC instances i parallel. The benefit of doing this
            is mainly that we can utilise the cluster better when running iProver.
            It can take the same program parameters as smac.py
        * Usage:
            Run as $ python3 parallel_smac.py <smac.py parameters>
            Define clusters in .cluster-script and put the names in the list
            machines list within the program

    iprover.py:
        * Description:
            This script is used by SMAC as an algorithm wrapper for iProver, so
            typically there is no need for the user to call this script
            directly.
            Roughly put, it:
                1) Translates the iProver parameters supplied as arguments to
                   the script from the SMAC format to their iProver format
                2) Adds the parameters to the configuration file used by the
                   setup-experiment cluster script (modifies wrappers/experiment_config)
                3) Starts the setup-experiment cluster script and waits for it
                   to finish
                4) After setup-experiment has finished, the script computes
                   some information about the setup-experiment run, and it
                   inserts a new WrapperRun entry into the database
        * Requirements:
            * MySQL Connector/Python
            * MySQL database with the appropriate SMAC tables
        * Installation: not needed
        * Usage:
            * The script is not invoked directly by the user, rather it is
              called by SMAC
            * The script accepts as arguments the iProver parameters as
              described in the PCS file used by SMAC, plus a few more arguments
              (see the SMAC manual at
              http://www.cs.ubc.ca/labs/beta/Projects/SMAC/ for information
              about how algorithm wrappers are called)
            * There are a number of hard-coded paths and names in the script,
              which most likely will need to be changed, e.g. by editing the
              script with a text editor:
                * Lines 11-16: Database connection details
                * Lines 231 and 252: Experiment configuration file (as used by
                                     the setup-experiment cluster script)
                * Line 253: Name of problem collection
                * Line 254: Name of cluster
                * Line 268: Total number of problems in collection
                * Line 293: Path to the kill_my script

                contains alias to vip
                contains quality:
                 quality = 150.0

SQL Scripts:
    Note: all the SQL scripts require the iProver database tables (e.g.
          Experiment, Problem, ProblemVersion, Collection) and are used as any
          .sql file (e.g. by using "source <file>" in the MySQL prompt)

    recreate_smac_tables.sql:
        * Description:
            SQL code for the (re-)creation of all the database tables used by
            the smac.py and iprover.py scripts

    add_collection.sql:
        * Description:
            SQL code for adding a collection (i.e. subset) of TPTP problems

    delete_collection.sql:
        * Description:
            SQL code for deleting a collection

    check_unsolved_problems.sql:
        * Description:
            SQL code for selecting TPTP problems for a particular SMAC
            experiment which have been listed as unsolved in the iProver DB


smac DB connection reset:

smac/dev/src/smac-src/ca/ubc/cs/beta/smac/configurator/AbstractAlgorithmFramework.java

.cluster-scripts is also used there (file called with hyphen and not underscore)

