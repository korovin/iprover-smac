import mysql.connector as db
import traceback

# Our global config file which mostly contains parameters for running this HLP loop
import global_config
import database

# Config mostly related to SMAC
import warmstart as ws

from experiment_config import (
    get_global_experiment_config,
    get_global_timeout,
    get_local_timeout,
    get_prover_id,
)

db_connection_details = db_cred.db_connection_details

# Machines that are used to evaluate the new heuristics on the full problem set
# This variable name should not be changed, as it is better to change the node
# ranges in .cluster_scripts

# Time of when to stop repeating the loop. The loop itself is regarded as atomic
# hence there is no break mid-loop.

# The file path of the SMAC scenario file. This is particulary used to amend
# the number of iterations to run SMAC for.
SCENARIO_PATH = "../smac/config/scenario.txt"


###################################################################


def get_number_of_local_optimisation_iterations(logf):
    # If no freezing, only iterate once
    if not global_config.FREEZE_PARAMETERS:
        max_iterations = 1
    else:
        # One iteration per freezing section
        max_iterations = len(global_config.FREEZE_DOMAIN)

    logf.write("Number of local iterations: {0}\n".format(max_iterations))
    return max_iterations


def build_optimisation_cmd(collection_name, global_heuristics, incumbent, local_iteration, logf):
    # Initial setup
    cmd = [
        "python3",
        "smac_parallel_wait.py",
        str(incumbent),
        global_config.IPROVER_PARAMETERS_JSON,
        "--quality",
        global_config.optimisation_metric,
        "--collection",
        collection_name,
    ]

    # Handle parameter freezing
    if global_config.FREEZE_PARAMETERS:
        # Check if we are to freeze None
        logf.write("Freezing parameters: ")
        # Freeze a parameter section
        freeze_section = global_config.FREEZE_DOMAIN[local_iteration]
        logf.write("{0}\n".format(", ".join(str(f) for f in freeze_section)))
        # Not adding this parameter as None means no freezing for this round
        if freeze_section is None:
            cmd += ["--freeze_section"] + freeze_section

    # See if we are to run with warmstart
    elif global_config.USE_EXPERIMENT_HISTORY_WARMSTART:
        # Run handle warmstart
        current_timeout = get_local_timeout()
        prover_id = get_prover_id()

        # Create the appropriate files for the warmstart
        warm_path = ws.handle_warmstart(
            global_config.PROBLEM_COLLECTION,
            collection_name,
            global_config.IPROVER_PARAMETERS_JSON,
            prover_id,
            current_timeout,
            global_heuristics,
            incumbent,
        )

        # Check that some warmstart files were added
        if warm_path is not None:
            # Add the warmstart option
            cmd += ["--warmstart", warm_path]

    logf.flush()

    return cmd


# Currenlty run this as a single instance of SMAC
def discover_local_heuristics(collection_name, global_heuristics, logf):
    # List for storing all local heuristics for this cluster
    new_heuristics = []

    # Get how many times we are optimising over the problem collection
    max_iterations = get_number_of_local_optimisation_iterations(logf)

    for local_iteration in range(max_iterations):
        if max_iterations > 1:
            logf.write("\n** Local iteration: {0}\n".format(local_iteration))

        # Delete all previous SMAC output files
        delete_previous_smac_output()

        # Get the best local heuristic
        incumbent, incumbent_score = get_local_incumbent(
            collection_name, global_heuristics + new_heuristics, logf
        )

        # Build the cmd for the SMAC environment
        cmd = build_optimisation_cmd(collection_name, global_heuristics, incumbent, local_iteration, logf)

        # Run SMAC opimisation
        run_optimiser(cmd)

        # Get the top heuristics from the experiment
        new_heuristics += get_new_local_heuristics(global_heuristics + new_heuristics, logf)

    # Return all new heuristics
    return new_heuristics


def get_smac_best_heuristic_filter_current(heuristic_filter):
    # Get the heuristics of the top quality that does not have the same paramstring as in the filter
    # then select the heuristic with the lowest total time among them.

    # If we have a list of heuristis, make them into a comma string
    if isinstance(heuristic_filter, list):
        heuristic_filter = ", ".join(str(h) for h in heuristic_filter)

    try:
        conn = db.connect(**db_connection_details)
        curs = conn.cursor()

        # Get all the candidate heuristics, given the current experiment and the heuristic filter
        sql_query = """
                     SELECT WrapperRun.Experiment,
                            WrapperRun.QUALITY
                     FROM WrapperRun
                     INNER JOIN SMAC_ParamString
                     ON SMAC_ParamString.iProverExperimentID = WrapperRun.Experiment
                     WHERE WrapperRun.SMACExperiment = (SELECT MAX(SMACExperimentID) FROM SMACExperiment)
                     AND SMAC_ParamString.ParamString NOT IN
                     (
                         SELECT ParamString
                         FROM SMAC_ParamString
                         WHERE iProverExperimentID IN ({0})
                     )
                    ;""".format(
            heuristic_filter
        )
        curs.execute(sql_query)
        res = curs.fetchall()

        # Check if there are any candidates
        if len(res) == 0:
            return None, 0

        # Compute the highest score, and check that it is not zero
        best_score = min(r[1] for r in res)
        if best_score == 0:
            return None, 0

        # Get the experiments with the highest quality (min value)
        top_heuristics = []
        for h, s in res:
            if s == best_score:
                top_heuristics += [h]

        # Only one heuristic with this score, return it as the best
        if len(top_heuristics) == 1:
            return top_heuristics[0], best_score

        # There are multiple heuristics with this score
        # Hence, we order them by solving time and select the best
        # Get total solving times for the heuristics for a second ordering
        solving_times = []
        for heur in top_heuristics:
            solving_times += [(heur, database.query_experiment_solved_tot_runtime(heur))]

        # Order by the least solving time and return
        times_sorted = sorted(solving_times, key=lambda tup: tup[1])
        optimal_heuristic = times_sorted[0][0]

        # Return the name of the heuristic, and it's quality score
        return optimal_heuristic, best_score

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


if __name__ == "__main__":
    main()
