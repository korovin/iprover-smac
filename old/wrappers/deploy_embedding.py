import time
import pickle
import pandas as pd

import database
from configX.adm_prediction import PREDICTION_MODEL_PARAMETERS
from admissible_mapping.feature import get_data_sets, get_features
from admissible_mapping.model import HNN4
from run_embedding import Embedding


def compute_solved_problems(data):
    # List to hold which heuristics are solved
    solved_names = []

    # Store runtime data for all the problems
    all_runtimes = data.values

    #  For every problem
    for name, runtimes in zip(data.index, all_runtimes):
        # Compute number of solved instances for the problem
        solved_inst = sum(runtimes > 0)
        if solved_inst > 0:
            solved_names += [name]

    return solved_names



def main(prover, feature_file, heuristics, heuristic_data, processing_heuristic=None):

    # Check if we have heuristic IDs or collected data
    if heuristics is None:
        solved_problems_names = compute_solved_problems(heuristic_data)
        unsolved_problems_names = sorted(set(heuristic_data.index) - set(solved_problems_names)) # Not used so set it to empty
        heuristics = heuristic_data.columns
    else:
        # We have raw heuristic IDs, compute solved/unsolved and get the names
        solved_problems_id = database.get_solved_problems_in_experiments(heuristics)
        unsolved_problems_id = database.get_unsolved_problems_in_experiments(heuristics)
        solved_problems_names = database.get_problem_names_from_ids(solved_problems_id)
        unsolved_problems_names = database.get_problem_names_from_ids(unsolved_problems_id)
        heuristics = [str(h) for h in heuristics]

    df, id_train, id_test, targets = get_data_sets(
        feature_file, heuristics, solved_problems_names, unsolved_problems_names, df_eval=heuristic_data)

    # Train model
    features = get_features(df, id_train)
    clf = HNN4(heuristics, features, multi_model_params=PREDICTION_MODEL_PARAMETERS['HNN4'])
    clf.fit(df, id_train, id_test)

    # Make embedding
    emb = Embedding(clf.features, feature_file, heuristics, prover, clf)
    res = emb.compute_features('/shareddata/TPTP-v7.4.0/Problems/PUZ/PUZ007-1.p')
    #emb.model = clf

    # Set the heuristic for feature computation
    if processing_heuristic is not None:
        emb.processing_heuristic = processing_heuristic

    res = emb.embed('/shareddata/TPTP-v7.4.0/Problems/PUZ/PUZ007-1.p', -1)
    print('PUZ007-1.p', res)

    print("Save embedding")
    time_stamp = time.strftime('%Y-%m-%d_%H-%M-%S.log', time.localtime())
    model_file = "admissible_mapping/models/embedding_{0}.pkl".format(time_stamp)
    with open(model_file, 'wb') as out_file:
        pickle.dump(emb, out_file, pickle.DEFAULT_PROTOCOL)
    print("Embedding saved to: ", model_file)

    # Delete to be certain
    del emb

    print("Load embedding")
    with open(model_file, 'rb') as in_file:
        emb = pickle.load(in_file, encoding='latin1')

    print("Testing")
    res = emb.embed('/shareddata/TPTP-v7.4.0/Problems/PUZ/PUZ007-1.p', -1)
    print('PUZ007-1.p', res)
    res = emb.embed('/shareddata/TPTP-v7.4.0/Problems/ALG/ALG242-1.p', -1)
    print('ALG242-1.p', res)


if __name__ == "__main__":

    # Parameters
    prover = 'iproveropt_2021_02_14'

    # Training features
    #feature_file = "admissible_mapping/data/tptp_1_287583.csv"
    #feature_file = "/shareddata/homes/holden/iprover-smac/wrappers/admissible_mapping/data/iproveropt_2021_02_14_tptp_v7.4_fof_4000_rand_1_287583.csv" # TODO need features for the whole TPTP?
    feature_file = "/shareddata/homes/holden/prover_stats/data/tptp_timing_1_287583.csv"

    # Targets
    #heuristics = [389408, 381333, 381322, 379314, 383338, 381331, 379312, 383340, 379310, 379313]
    #heuristics = [389408,379304,379305,379306,379307,379308,379309,379310,379311,379312,379313,379314,379315,379316,379317,379318,379319,379320,381321,381322,381323,381324,381325,381326,381327,381328,381329,381330,381331,381332,381333,381334,383333,383334,383335,383336,383337,383338,383339,383340,383341,386342,386343,386344,386345,386346,386347,386348,386349,386350,386351,389350,389351,389352]
    heuristics = None

    #heuristic_data = "/shareddata/homes/holden/scpeduler/data/tptp_4000_all_options_combined.csv"
    heuristic_data = "/shareddata/homes/holden/scpeduler/data/tptp_4000_super.csv"

    heuristic_data = pd.read_csv(heuristic_data, index_col=0)
    # This heuristic is incorrect
    del heuristic_data['433533']


    heur_str = '--stats_out all --sat_out_model none --out_options none --schedule none --abstr_ref "[]" --inst_lit_sel "[+split;-sign;-depth]" --inst_passive_queues_freq "[25;2]" --inst_passive_queues "[[-conj_dist;+conj_symb;-num_var];[+age;-num_symb]]" --inst_passive_queue_type priority_queues --inst_solver_calls_frac 1.0 --inst_lit_sel_side num_symb --extra_neg_conj none --res_out_proof false --inst_out_proof false --superposition_flag true --resolution_flag false --inst_sos_flag false --inst_restr_to_given false --inst_lit_activity_flag true --inst_learning_loop_flag true --inst_subs_given false --inst_subs_new false --inst_prop_sim_new false --inst_prop_sim_given true --inst_eager_unprocessed_to_passive true --inst_dismatching true --instantiation_flag true --inst_activity_threshold 500 --inst_start_prop_sim_after_learn 3 --inst_learning_factor 2 --inst_learning_start 3000 --inst_solver_per_active 1400 --prop_solver_per_cl 1024 --comb_sup_mult 2 --comb_inst_mult 10'


    # Create embedding model
    main(prover, feature_file, heuristics, heuristic_data, processing_heuristic=heur_str)


