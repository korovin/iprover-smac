"""
Script for generating a exp_config prover_options type string
from the current default values in the given param_opt file.
"""
import json
import argparse
from typing import Dict

import global_config
from target_create_heuristic import create_experiment_heuristic


def load_param_options_from_json() -> Dict[str, str]:
    # Load json_template_pcs file
    with open(global_config.IPROVER_PARAMETERS_JSON, "r") as f:
        data = json.load(f)

    # Load all options and default values
    params = {}
    for section in data.values():
        for opt in section:
            params[opt["opt_name"].strip("--")] = opt["default_val"]

    return params


def main():

    # TODO this should come from some config file!
    parser = argparse.ArgumentParser()
    parser.add_argument("--timeout", default=None, type=int, help="Timeout to use in the heuristic")
    args = parser.parse_args()

    params = load_param_options_from_json()

    heur = create_experiment_heuristic(params, timeout=args.timeout, ignore_empty_check=True)

    print("# Experiment config:")
    print(heur.get_prover_options())
    print()
    print("# Cmd string")
    print(heur.get_heuristic_cmd())


if __name__ == "__main__":
    main()
