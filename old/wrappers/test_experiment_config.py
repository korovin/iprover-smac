import unittest
import os


from experiment_config import (
    EXP_ENV,
    RUN_ENV,
    _get_env,
    get_global_experiment_config,
    get_local_experiment_config,
    get_prover_id,
    get_global_timeout,
    get_local_timeout,
)


def test_get_env_local():
    env = _get_env("local")
    assert env["timeout_wallclock"] == RUN_ENV["local"]["timeout_wallclock"]


def test_get_env_global():
    env = _get_env("global")
    assert env["timeout_wallclock"] == RUN_ENV["global"]["timeout_wallclock"]


def test_global_conf_timeout():
    conf = get_global_experiment_config("test")
    assert conf.get_timeout() == RUN_ENV["global"]["timeout_wallclock"]


def test_local_conf_timeout():
    conf = get_local_experiment_config("test")
    assert conf.get_timeout() == RUN_ENV["local"]["timeout_wallclock"]


def test_config_str_contents():
    conf = get_global_experiment_config("test")
    conf_str = conf._to_str()
    assert "prover=" in conf_str
    assert "prover_options=" in conf_str
    assert "timeout_wallclock=" in conf_str


def test_config_str_grace():
    conf = get_global_experiment_config("test")
    conf.timeout_wallclock = 100
    conf.wallclock_grace = 20
    conf_str = conf._to_str()
    assert "timeout_wallclock=120" in conf_str


def test_config_to_file_exists():
    conf_file = get_global_experiment_config("test").to_file()
    assert os.path.exists(conf_file)


def test_get_prover_id():
    assert get_prover_id() == EXP_ENV["prover"]


def test_get_global_timeout():
    assert get_global_timeout() == _get_env("global")["timeout_wallclock"]


def test_get_local_timeout():
    assert get_local_timeout() == _get_env("local")["timeout_wallclock"]
