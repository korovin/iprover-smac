import multiprocessing
import argparse
import pickle
import sys
import os
import numpy
from collections import namedtuple
from admissible_mapping.compute_problem_features import run_feature_collection_on_problem


# Setup root logger
import logging
logging.basicConfig(stream=sys.stderr, level=logging.INFO,
                    format="%(message)s")
log = logging.getLogger()


class Embedding():

    def __init__(self, features, feature_file, heuristics, prover,
                 base_model, processing_heuristic="", processing_timelimit=1):

        self.features = features
        self.feature_file = feature_file
        self.heuristics = heuristics
        self.prover = prover
        self.processing_heuristic = processing_heuristic
        self.processing_timelimit = processing_timelimit
        self.base_model = base_model

    def compute_features(self, problem_path):
        return run_feature_collection_on_problem(self.prover, self.processing_heuristic,
                                                 problem_path, self.processing_timelimit,
                                                 '/tmp', False, delete_tmp_file=False)

    def update_prover(self, new_prover):
        self.prover = new_prover

    def embed(self, problem_path, no_cores):
        # Create tuple for storing the result
        EmbeddingResult = namedtuple('EmbeddingResult', ['embedding_vector', 'solved', 'proof'])

        # Try to compute the problem features
        feat = self.compute_features(problem_path)

        # The features will always include solved, name and szs_status
        # However, if the statistic computation fails these are the
        # only resulting features
        if len(feat) == 3:
            raise ValueError("Feature computation failed")

        # Check that the features are computed
        if not set(self.features).issubset(set(feat.keys())):
            raise ValueError("Mismatch on computed features. Missing: {0}".format(set(self.features) - set(feat.keys())))

        # Compute whether the problem was solved
        if "solved" in feat and feat["solved"]:
            solved = True
        else:
            solved = False

        # If the problem was solved, get the proof
        if solved:
            # Extract the proof from the processing output
            with open(feat['out_file'], 'r') as f:
                proof = f.read()
        else:
            proof = None


        # Compute the problem embedding, if it was solved during
        # extraction we set the eembedding to be all ones.
        if solved:
            # Get an embedding of all ones
            embedding_vector = numpy.asarray([1] * len(self.heuristics))
        else:
            # Predict the embedding
            # Extract the features
            X = [[feat[f] for f in self.features]]

            # Predict
            embedding_vector = self.base_model.predict(X, no_cores)[0]


        # Delete the tmp feature processing file
        try:
            os.remove(feat["out_file"])
        except OSError:
            pass

        return EmbeddingResult(embedding_vector, solved, proof)


def print_embedding_info(emb):

    print("Embedding was deployed with prover: {0}".format(emb.prover))
    print("It was trained on {0} features from {1}".format(
        len(emb.features), emb.feature_file))
    print("Feature set used: {0}".format(emb.features))
    print("Mapped to {0} heuristics".format(len(emb.heuristics)))
    print("Heuristics used: {0}".format(list(emb.heuristics)))
    print("Processing heuristic set to: {0}".format(emb.processing_heuristic))


def main(prover_path, model_path, problem_path, clausifier_path=None, print_info=False, no_cores=-1, print_proof=True):

    # Check prover path
    if not os.path.isfile(prover_path):
        log.error("Invalid prover path")
        sys.exit(1)

    # Check problem path
    if not os.path.isfile(problem_path):
        log.error("Invalid problem path")
        sys.exit(1)

    # Check that the model file exists
    if not os.path.isfile(model_path):
        log.error("Invalid model path")
        sys.exit(1)

    log.debug("Loading embedding: ", model_path)
    with open(model_path, 'rb') as in_file:
        emb = pickle.load(in_file, encoding='latin1')

    if print_info:
        print_embedding_info(emb)
        sys.exit(0)

    # Set the prover
    emb.update_prover(prover_path)

    # Set clausifier if provided
    if clausifier_path is not None:
        emb.processing_heuristic += " --clausifier {0}".format(clausifier_path)

    # Embed the problem
    # try and catch?
    try:
        res = emb.embed(problem_path, no_cores)
    except ValueError as err:
        log.error(err)
        sys.exit(1)
    except Exception as err:
        log.error(err)
        log.error("Unexpected exception")
        sys.exit(1)

    # If the problem was solved and we are printing proofs
    if res.solved and print_proof:
        # Print the proof and exit with code 10
        print(res.proof)
        sys.exit(10)
    else:
        # Print the embedding
        with numpy.printoptions(threshold=numpy.inf, linewidth=numpy.inf):
            print(res.embedding_vector)

    # Exit normally
    sys.exit(0)


def get_input_args(argv=None):

    parser = argparse.ArgumentParser()
    parser.add_argument("prover_path", help="Path to prover used for feature computation")
    parser.add_argument("model_path", help="Path to model used to embed problem")
    parser.add_argument("problem_path", help="Path to the problem used to embed")

    parser.add_argument("--print_proof", type=bool, default=True,
                        help="Print the proof and exit with code 10 if solved during feature computation")
    parser.add_argument("--print_info", action="store_true",
                        help="Print embedding model info and exit")
    parser.add_argument("--clausifier_path", type=str,
                        help="Path to the clausifier")
    parser.add_argument("--no_cores", type=int, default=-1,
                        help="Cores used for embedding.")

    # Parse the arguments
    args = parser.parse_args(argv)

    return args


if __name__ == "__main__":
    multiprocessing.freeze_support()


    args = get_input_args()
    main(**(vars(args)))
