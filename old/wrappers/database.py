import sys
import mysql.connector as db
import traceback
import os

from typing import List

# Our global config file which mostly contains parameters for running this HLP loop
import global_config

# Config mostly related to SMAC
import config

dpath = config.dpath

import db_cred  # not in git

db_connection_details = db_cred.db_connection_details


def get_problem_ids_in_collection(problem_collection):
    # Function returns all the problem ids in a a collection
    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        curs.execute(
            """
                     SELECT Problem
                     FROM CollectionProblems
                     WHERE Collection = (SELECT CollectionID
                                         FROM Collection
                                         WHERE CollectionName = %s)
                    ORDER BY Problem
                    ;""",
            (problem_collection,),
        )

        res = curs.fetchall()
        res = [r[0] for r in res]

        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def get_experiment_quality(experiment):
    # Function returns all the problem ids in a a collection
    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        curs.execute(
            """
                     SELECT Quality
                     FROM  WrapperRun
                     WHERE Experiment = %s
                     ;""",
            (experiment,),
        )

        res = curs.fetchall()
        res = float(res[0][0])

        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def get_problem_ids_in_experiment(experiment):
    # Function returns all the problem ids in a a collection
    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        curs.execute(
            """
                     SELECT Problem
                     FROM ProblemRun
                     WHERE Experiment = %s
                     ORDER BY Problem
                     ;""",
            (experiment,),
        )

        res = curs.fetchall()
        res = [r[0] for r in res]

        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def get_solved_problems_in_experiments(experiments):
    # Take a list of heuristics and return all the problems which has at least one solution
    solved_list = []
    for exp_id in experiments:
        solved_list += query_id_solved_problems_in_exp(exp_id)

    # Compute the set of solved problems
    solved_set = sorted(set(solved_list))
    return solved_set


def get_unsolved_problems_in_experiments(experiments):
    # Take a list of heuristics and return the problems which have no known solutions
    # We do this by getting the unsolved problems from the first experiment
    # Then we iteratively remove the problems which are solved by the later experiments
    init_unsolved = set(query_id_unsolved_problems_in_exp(experiments[0]))
    solved_set = set(get_solved_problems_in_experiments(experiments))

    # Get the problems with no known solutions
    unsolved_problems = sorted(init_unsolved - solved_set)

    return unsolved_problems


def query_experiment_solved_tot_runtime(exp_id, upper_time_bound=None):
    select = """
            SELECT IFNULL(SUM(PR.RunTime), 0)
            """
    return float(_base_query_solved_problemrun(select, exp_id, upper_time_bound)[0][0])


def query_experiment_solved_avg_runtime(exp_id, upper_time_bound=None):
    select = """
            SELECT IFNULL(AVG(PR.RunTime), 0)
            """
    return float(_base_query_solved_problemrun(select, exp_id, upper_time_bound)[0][0])


def query_experiment_runtimes_filter_set(exp_id, problem_set):
    select = """
            SELECT PR.RunTime
            """
    res = _base_query_solved_problemrun_filter_set(select, exp_id, problem_set, multi=True)
    return res


def query_id_problems_in_exp(exp_id):
    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        curs.execute(
            """
                     SELECT Problem
                     FROM ProblemRun
                     WHERE Experiment=%s
                     ORDER BY Problem
                     ;""",
            (exp_id,),
        )

        res = curs.fetchall()
        res = [r[0] for r in res]

        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def query_id_unsolved_problems_in_exp(exp_id):
    # get the number of solved problems
    select = "SELECT PR.Problem"
    res = _base_query_unsolved_problemrun(select, exp_id)
    # Make it into a list
    res = [r[0] for r in res]
    return res


def query_no_solved_problems_filter_problems_solved_runtime(exp_id, max_runtime, collection):
    select = "SELECT COUNT(*)"
    res = _base_query_filter_problems_solved_runtime(select, exp_id, max_runtime, collection)
    return res[0][0]


def query_solved_problems_filter_problems_solved_runtime(exp_id, max_runtime, collection):
    select = "SELECT PR.Problem"
    res = _base_query_filter_problems_solved_runtime(select, exp_id, max_runtime, collection)
    res = [r[0] for r in res]
    return res


def query_tot_time_filter_problems_runtime(exp_id, max_runtime, collection):
    select = "SELECT IF(PR.Runtime > {0}, {0}, PR.Runtime)".format(max_runtime)

    # We set the max_runtime to the max value as we want to include every single problem in the collection
    # so we replace the max_runtime with a very high runtime
    res = _base_query_filter_problems_solved_runtime(select, exp_id, 1000000, collection, filter_solved=False)
    res = [r[0] for r in res]

    # Sum the values
    res = sum(res)

    return res


def _base_query_filter_problems_solved_runtime(select, exp_id, max_runtime, collection, filter_solved=True):
    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        sql_query = (
                select
                + """
            FROM ProblemRun PR
            """
        )

        # Join on appopriate tables to get status problem information
        sql_query += """
            JOIN ProblemVersion PV ON PV.ProblemVersionID = PR.Problem
            JOIN SZSStatus SL ON PV.Status = SL.SZSStatusID
            JOIN SZSStatus SR ON PR.Status = SR.SZSStatusID
            """

        sql_query += """
                    WHERE PR.Experiment={0}
                    AND PR.Runtime <= {1}
                    AND PR.Problem IN
                    (
                        SELECT CP.Problem
                        FROM CollectionProblems CP
                        WHERE CP.Collection = (SELECT C.CollectionID
                                            FROM Collection C
                                            WHERE C.CollectionName='{2}')
                    )
                    """.format(
            exp_id, max_runtime, collection
        )

        if global_config.PROBLEM_COLLECTION_IS_LTB:
            sql_query += """
                 AND (PR.SZS_Status = 'Theorem'
                 OR PR.SZS_Status = 'Unsatisfiable')
                         """
        elif filter_solved:
            # Include all solved problems
            sql_query += """
                 AND SR.Solved = 1
                         """

        # Remove the incorrects by only including the correct
        if not global_config.INCLUDE_INCORRECT:
            sql_query += """
                AND ((SL.Unsat AND SR.Sat)
                    OR (SL.Sat AND SR.Unsat)) = 0"""

        # Execute
        sql_query += ";"
        curs.execute(sql_query)

        res = curs.fetchall()
        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def _base_query_unsolved_problemrun(select, exp_id):
    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        sql_query = """
            {0}
            FROM ProblemRun PR
            """.format(
            select
        )

        # Join on appopriate tables to be able get status information
        sql_query += """
            JOIN ProblemVersion PV ON PV.ProblemVersionID = PR.Problem
            JOIN SZSStatus SL ON PV.Status = SL.SZSStatusID
            JOIN SZSStatus SR ON PR.Status = SR.SZSStatusID
            """

        sql_query += """
                WHERE PR.Experiment={0}
                """.format(
            exp_id
        )

        if global_config.PROBLEM_COLLECTION_IS_LTB:
            sql_query += """
                 AND (PR.SZS_Status != 'Theorem'
                 AND PR.SZS_Status != 'Unsatisfiable')
                         """
            # Check for incorrect on unsat
            if not global_config.INCLUDE_INCORRECT:
                sql_query += """
                    AND (SL.Sat AND SR.Unsat) = 0
                """

        else:
            # Filter unsolved
            sql_query += """
                 AND SR.Solved = 0
                         """
            # Check for incorrect
            if not global_config.INCLUDE_INCORRECT:
                sql_query += """
                    AND ((SL.Unsat AND SR.Sat)
                        OR (SL.Sat AND SR.Unsat)) = 0
                        """

        # Execute
        sql_query += ";"
        curs.execute(sql_query)

        res = curs.fetchall()

        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def query_beuristic_summary_solved_in_set(exp_id, problem_set, upper_time_bound=None):
    select = """SELECT IFNULL(PR.Experiment, {0}),
                Count(*),
                IFNULL(SUM(PR.RunTime), 0)
             """.format(
        exp_id
    )
    return _base_query_solved_problemrun_filter_set(
        select, exp_id, problem_set, upper_time_bound=upper_time_bound
    )


def _base_query_solved_problemrun_filter_set(select, exp_id, problem_set, multi=False, upper_time_bound=None):
    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        sql_query = """
            {0}
            FROM ProblemRun PR
                INNER JOIN CollectionProblems CP
                ON CP.Problem=PR.Problem
            """.format(
            select
        )

        # Get Status information
        sql_query += """
            JOIN ProblemVersion PV ON PV.ProblemVersionID = PR.Problem
            JOIN SZSStatus SL ON PV.Status = SL.SZSStatusID
            JOIN SZSStatus SR ON PR.Status = SR.SZSStatusID
            """

        # Filter on expeirment and collection
        sql_query += " WHERE Experiment={0} ".format(exp_id)
        sql_query += " AND CP.Problem IN ({0}) ".format(", ".join(str(p) for p in problem_set))

        # Set upper time bound
        if upper_time_bound is not None:
            sql_query += " AND PR.Runtime <= {0} ".format(upper_time_bound)

        if global_config.PROBLEM_COLLECTION_IS_LTB:
            sql_query += """
                 AND (PR.SZS_Status = 'Theorem'
                 OR PR.SZS_Status = 'Unsatisfiable')
                         """
        else:
            sql_query += """
                AND SR.Solved=1
                         """

        # Remove the incorrects by only including the correct
        if not global_config.INCLUDE_INCORRECT:
            sql_query += """
                AND ((SL.Unsat AND SR.Sat)
                    OR (SL.Sat AND SR.Unsat)) = 0"""

        # print(sql_query)
        # Execute
        sql_query += ";"
        curs.execute(sql_query)

        if multi:
            res = curs.fetchall()
        else:
            res = curs.fetchall()[0]

        return res

    except Exception as err:
        print(err)
        print(traceback.format_exc())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def get_problem_names_from_ids(problem_ids):
    if len(problem_ids) == 0:
        return []

    try:
        conn = db.connect(**db_connection_details)
        curs = conn.cursor()

        # Make sure list is str
        problem_ids = [str(p) for p in problem_ids]

        # Get the quality measure from this experiment
        # print(problem_ids)
        curs.execute(
            """
                     SELECT Filename
                     FROM ProblemVersion
                     WHERE ProblemVersionID IN ({0})
                     ;""".format(
                ",".join(problem_ids)
            )
        )

        problem_names = curs.fetchall()
        problem_names = [p[0] for p in problem_names]
        return problem_names

    except Exception as exc:
        print(exc)
        print(traceback.format_exception())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def get_problem_paths_in_collection(collection_name):
    try:
        conn = db.connect(**db_connection_details)
        curs = conn.cursor()

        # Get the quality measure from this experiment
        curs.execute(
            """
                    SELECT LD.Path AS Path, PV.Filename AS Filename
                    FROM ((ProblemVersion PV INNER JOIN Problem P
                           ON PV.Problem = P.ProblemID) INNER JOIN LibraryDomain LD
                           ON P.LibraryDomain = LD.LibraryDomainID) INNER JOIN LibraryVersion LV
                           ON PV.Version = LV.LibraryVersionID
                    WHERE ProblemVersionID
                    IN (
                          SELECT Problem
                           FROM CollectionProblems
                           WHERE Collection = (SELECT CollectionID FROM Collection WHERE CollectionName=%s)
                       )
                    ;""",
            (collection_name,),
        )

        path_tuple = curs.fetchall()

        problem_paths = []
        for path, filename in path_tuple:
            # Append subdirectory if exists
            if path == "":
                problem_paths += [filename]
            else:
                problem_paths += [path + "/" + filename]

        return problem_paths

    except Exception as exc:
        print(exc)
        print(traceback.format_exception())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def check_if_heuristic_paramstring_matches_other_experiments(experiment, other):
    # Convert list to comma string
    if isinstance(other, list):
        other = ", ".join(str(o) for o in other)

    try:
        conn = db.connect(**db_connection_details)
        curs = conn.cursor()

        # Get the quality measure from this experiment
        sql_query = """
                    SELECT iProverExperimentID
                    FROM SMAC_ParamString
                    WHERE ParamString = (SELECT ParamString
                                         FROM SMAC_ParamString
                                         WHERE iProverExperimentID = {0})
                    AND iProverExperimentID IN ({1})
                    ;""".format(
            experiment, other
        )
        curs.execute(sql_query)

        res = curs.fetchall()
        res = [r[0] for r in res]

        return res

    except Exception as exc:
        print(exc)
        print(traceback.format_exception())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


def report_options_freezing_to_db(this_smacexp_id, freeze_params):
    try:
        conn = db.connect(**db_connection_details)
        curs = conn.cursor()

        # We ignore as we might have duplicate entries if the program is ran
        # as a parallel smac experiment. However, this will not cause any negative problems
        curs.execute(
            """
                     INSERT IGNORE INTO SMAC_ParameterFreeze(SMACExperimentID, Parameters)
                     VALUES(%s, %s)
                     ;""",
            (this_smacexp_id, ", ".join(sorted(freeze_params))),
        )

        conn.commit()

    except Exception as exc:
        print(exc)
        print(traceback.format_exception())
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


# TARGET FUNCTION METHODS START ################################################################3


# Function for releasing a set of machines after computation

# TARGET FUNCTION METHODS END   ################################################################3


def get_all_options_in_prover_version(prover_id: int) -> List[str]:
    conn = None
    curs = None

    try:
        db_conn_details = db_cred.db_connection_details
        conn = db.connect(**db_conn_details)
        curs = conn.cursor()

        curs.execute(
            """
                     SELECT ParameterName
                     FROM ProverParameters
                     WHERE Prover=%s
                     ;""",
            (prover_id,),
        )

        prover_options = curs.fetchall()
        prover_options = [p[0] for p in prover_options]
        return prover_options

    except db.Error as err:
        print(err)
        raise Exception(err)
    finally:
        if curs:
            curs.close()
        if conn:
            conn.close()


if __name__ == "__main__":
    # SELECT * FROM ProblemVersion WHERE Filename="CSR041+4.p";
    # n = get_problem_names_from_ids([493900, 496254, 496277, 496278, 496281, 496282, 496283])
    # n = get_problem_names_from_ids([496278, 496281, 496282, 496283])
    # n = get_problem_ids_from_names(['CSR041+4.p', 'AGT021+1.p', 'AGT009+2.p', 'AGT009+1.p', 'AGT007+2.p', 'AGT007+1.p', 'AGT006+2.p'], 'tptp_v7.4_fof_4000_rand')
    # print(n)

    # res = check_if_heuristic_paramstring_matches_other_experiments(319265, [19262, 319263, 319264, 319265, 319266, 319267, 319268])
    # print(res)

    """
    exp_id =181331 # with incorrects
    #exp_id = 183046 # None solved
    print("query_experiment_solved_tot_runtime")
    print(query_experiment_solved_tot_runtime(exp_id))
    print("query_no_solved_problems_in_exp")
    print(query_no_solved_problems_in_exp(exp_id))
    print("query_name_runtime_solved_problems_in_exp")
    print(query_id_runtime_solved_problems_in_exp(exp_id))
    print("query_names_solved_problems_in_exp")
    print(query_id_solved_problems_in_exp(exp_id))
    print(query_no_solved_problems_filter_problems_runtime(exp_id, 300, 'qbfeval_2020_test'))
    #"""
    # exp_id =183845 # with incorrects
    # exp_id = 183046 # None solved

    # res = query_heuristic_evaluation(exp_id)
    # print(res)
    # r = query_no_solved_problems_in_exp(330959, upper_time_bound=300)
    # p = query_id_solved_problems_in_exp(330959, upper_time_bound=20)
    # print(p)
    # r = query_beuristic_summary_solved_in_set(330959, p, upper_time_bound=20)

    # r = get_problem_collection()
    # print(r)
