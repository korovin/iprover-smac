import logging
import os

import config

# TODO maybe place all loggers inside here??

# Main log file for the target function (iprover) output
IPROVER_OUTPUT_ERROR_FILE = os.path.join(config.dpath, "wrappers", "logs/iprover_output_error.log")


FORMATTER = logging.Formatter("%(levelname)s:%(message)s")


def setup_logger(name: str, log_file: str, level: int = logging.INFO):
    """To setup as many loggers as you want"""

    handler = logging.FileHandler(log_file)
    handler.setFormatter(FORMATTER)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger


""" TODO REMOVE
def get_target_logger():

    logging.basicConfig(filename=IPROVER_OUTPUT_ERROR_FILE, level=logging.INFO, format=FORMATTER)
    logger = logging.getLogger("target_logger")
    return logger
"""


def get_heuristic_viability_logger():
    return setup_logger(
        "viability_logger",
        os.path.join(config.dpath, "wrappers", "logs/heuristic_viability.log"),
        level=logging.DEBUG,
    )


def get_target_logger():
    return setup_logger("target_logger", IPROVER_OUTPUT_ERROR_FILE)


log = get_target_logger()  # TODO maybe change how this is done? - get in in the file instead?
