import pytest

# TODO need to run as python3 -m pytest

from target_create_heuristic import (
    add_prep_params,
    add_qbf_params,
    add_inst_params,
    add_sat_params,
    add_res_params,
    add_sup_params,
    add_abstr_params,
)

# TODO make a data getter?
from target_heuristic import Heuristic
import target_create_heuristic


@pytest.mark.parametrize(
    "add_params_func,flag_tag",
    [
        pytest.param(add_prep_params, "preprocessing_flag", id="prep"),
        pytest.param(add_inst_params, "instantiation_flag", id="inst"),
        pytest.param(add_res_params, "resolution_flag", id="res"),
        pytest.param(add_sup_params, "superposition_flag", id="sup"),
        pytest.param(add_qbf_params, "abstr_ref", id="abstr"),
        pytest.param(add_qbf_params, "qbf_mode", id="qbf"),
        pytest.param(add_sat_params, "sat_mode", id="sat"),
    ],
)
def test_param_flags_undefined(add_params_func, flag_tag):
    # Check that nothing is added when the flag is not false

    # Make a default heuristic - instantly adds the timeout
    heur = Heuristic(1)
    # Add some stuff to the dict to make sure it remains unchanged
    params = {"foo": "bar"}
    add_params_func(heur, params)
    assert heur.get_no_options() == 1, "Something was added on emtpy dict"
    assert heur.get_option_value(flag_tag) is None, "Flag incorrectly specified"
    assert len(params) == 1, "Option was removed form params"


@pytest.mark.parametrize(
    "add_params_func,params",
    [
        pytest.param(add_prep_params, {"preprocessing_flag": "false"}, id="prep"),
        pytest.param(add_inst_params, {"instantiation_flag": "false"}, id="inst"),
        pytest.param(add_res_params, {"resolution_flag": "false"}, id="res"),
        pytest.param(add_sup_params, {"superposition_flag": "false"}, id="sup"),
        pytest.param(add_qbf_params, {"qbf_mode": "false"}, id="qbf"),
        pytest.param(add_sat_params, {"sat_mode": "false"}, id="sat"),
    ],
)
def test_param_flags_false(add_params_func, params):
    # Check that false flags are false!

    # Make a default heuristic - instantly adds the timeout
    heur = Heuristic(1)
    old_heur_size = heur.get_no_options()
    option_tag = next(iter(params))

    add_params_func(heur, params)

    assert heur.get_no_options() == old_heur_size + 1, "Flag option was not added to the heuristic"
    assert heur.get_option_value(option_tag) == "false", "Flag was not set correctly"
    assert len(params) == 0, "Option was not removed from the param dict"


def test_params_flags_false_abstr():
    # Make a default heuristic - instantly adds the timeout
    heur = Heuristic(1)
    old_heur_size = heur.get_no_options()
    params = {"abstr_ref_len": "0"}
    option_tag = "abstr_ref"

    add_abstr_params(heur, params)
    assert heur.get_no_options() == old_heur_size + 1, "Flag option was not added to the heuristic"
    assert heur.get_option_value(option_tag) == "[]", "Flag was not set correctly"
    assert len(params) == 0, "Option was not removed from the param dict"


# Same as above but abstraction ref is weird..
