#########################################################################
##########################################################################
# This file is used to have a global configuration of
# various parameters that affects the smac_hlp loop
# it might also use some variables that configures SMAC directly.
##########################################################################

import os

import config

# The initial heuristic(s)
# This works as both the baseline to compare against and as the initial starting
# point for the heuristics. To obtain this heuristic you run a normal iProver experiment
# over the problem collection. Be carefull that the experiment id's are tied
# to the database on the cluster you are running.
# INITIAL_HEURISTICS = [281963] # FOF 4000
# INITIAL_HEURISTICS = [281963, 293584, 294601, 293582, 290888] # FOF 4000
# INITIAL_HEURISTICS = [287583, 296939, 300000, 292049, 281963, 298378, 295482, 285683, 289978, 301535, 295478, 296941, 284957, 292343, 294166, 300739, 282445, 288550, 290889, 296646, 282446, 284954, 303649, 304010, 304377] # FOF 4000
# INITIAL_HEURISTICS = [310261]
# INITIAL_HEURISTICS = [310261, 310593, 310626, 310627]
# INITIAL_HEURISTICS = [317851]

# Use this now!
# INITIAL_HEURISTICS = [330959] #[330956] # Newest default heuristic
# INITIAL_HEURISTICS = [389408, 381333, 381322, 379314, 383338, 381331, 379312, 383340, 379310, 379313]
# INITIAL_HEURISTICS = [471003]
# INITIAL_HEURISTICS = [471003, 473898, 473899, 473900, 473901, 473902, 473903, 473904, 473905, 473906, 473907, 473908, 473909] # LTB restart!
INITIAL_HEURISTICS = [18]  # FOR TPTP problems

# Overall timeout of the hlp loop, given in minutes
LOOP_DURATION_MINUTES = 518400  # 7200 #5760 #1440 #2880 #4320

# The max number of hlp iterations we want to run
# MAX_HLP_ITERATION = 20
MAX_HLP_ITERATION = 1

# The problem collection to optimise over
# This corresponds to the name of the problem collection which you have uploaded in the db.
# PROBLEM_COLLECTION = "tptp_v7.4_fof_4000_rand"
# PROBLEM_COLLECTION = "tptp_v7.4_fof_4000_rand_complement"
# PROBLEM_COLLECTION = "tptp_v7.4_ueq_all"
# PROBLEM_COLLECTION = "tptp_ueq_problems"
PROBLEM_COLLECTION = "smt_comp_2023_train_problems"

# Tells whether the problem collection is LTB or not.
# This is important as it tells whether the outputted SZS status is correct or not,
# and especially because LTB problems are unlikely to have a correct/incorrect status
# in the db due to how they are uploaded.
# PROBLEM_COLLECTION_IS_LTB = True
PROBLEM_COLLECTION_IS_LTB = False

# Flag to state whether the problem set is in TFF or in FOF
# This is important for how the vclausify_rel options of
# the clausifier is parsed
PROBLEMS_IN_TFF = True  # False # TODO this should be extended to modes??

# Include incorrects in result
INCLUDE_INCORRECT = False

# The path to the clausifier
CLAUSIFIER_PATH = "/shareddata/homes/holden/bin/vclausify_rel"

# The maximum number of invalid heuristics SMAC can create
# in the experiment before we report a problem and terminate
# the optimisation.
# If None is supplied, there is no limit
MAX_NO_INVALID_HEURISTICS = 4000  # None

# JSON file containing the iProver parameters you want to optimise over.
# IPROVER_PARAMETERS_JSON = "param_opt/test_02_02_21_sup.json"
# IPROVER_PARAMETERS_JSON = "param_opt/casc_all_sine_11_05_21.json"
# IPROVER_PARAMETERS_JSON = "param_opt/casc_28/casc_ueq_test.json"
# IPROVER_PARAMETERS_JSON = "param_opt/sup_ueq_2022.json"
IPROVER_PARAMETERS_JSON = "param_opt/smt_comp_18_5_23.json"

# Selection method for picking the local heuristics after a SMAC run
# top_heuristic | heuristic_cover | admissible_heuristic_cover
LOCAL_HEURISTIC_SELECTION = "heuristic_cover"

# Variables for incrementing the SMAC run count
# The number of SMAC iterations in the frst iteration
INITIAL_SMAC_RUN_COUNT = 3  # 20  # 22  #12 #17 #20  #12 # TODO COMP

# Usually we want to run SMAC for longer of period of times
# when we have more data to cluster on. This is the factor we increment by
# for each time we run SMAC.
SMAC_RUN_COUNT_INCREMENTER = 4  # 4 #2 # TODO

# Wallclock time limits for global and local evaluation
WC_LOCAL = 3  # TODO
WC_GLOBAL = 5

# The optimisation metric used for running SMAC
# optimisation_metric = 'total_solved' # total_solved | total_time | weighted_solved # TODO use Enum?
optimisation_metric = "weighted_solved"

# # # # # # # # # # # # # # # # # # # # #
# # Warmstart and freezing are some rather special features
# # and when using them it is cruical to see if any errors
# # are generated due to incosistencies.
# # NOTE: Warmstart and freezing cannot be used at the same time
# # as this will crash SMAC!
# # # # # # # # # # # # # # # # # # # # #

# Variable for determining whether we will check the iProver/SMAC
# history to build a warmstart file for SMAC.
USE_EXPERIMENT_HISTORY_WARMSTART = True
# USE_EXPERIMENT_HISTORY_WARMSTART = False

# Limits the heuristic data to the heuristics used within the current HLP experiment
WARMSTART_LIMIT_CURRENT_HLP_EXPERIMENT = True

# Check whether we want to freeze the domain on some specific parameters
# to reduce the learning space for smac
FREEZE_PARAMETERS = False

# List which defines which sets of parameters we want to freeze
# this is currently based on string matching, where None means
# that we are not freezing any values.
# FREEZE_DOMAIN = [None, ["inst"]]
# FREEZE_DOMAIN = [None]
# FREEZE_DOMAIN = []

# FREEZE_DOMAIN = [["standard", "prep"],
#                 ["inst", "inst_sos_sth_lit_sel", "inst_lit_sel", "inst_passive_queues"],
#                 ["res", "res_passive_queues"],
#                 ["sup", "sup_passive_queues", "sup_indices_passive", "sup_full_triv", "sup_immed_triv", "sup_input_triv", "sup_full_fw", "sup_immed_fw_main", "sup_immed_fw_immed", "sup_input_fw", "sup_full_bw", "sup_immed_bw_main", "sup_immed_bw_main", "sup_input_bw"]]

# LTB
# FREEZE_DOMAIN = [["standard", "prep", "sine"],
#                 ["inst", "inst_sos_sth_lit_sel", "inst_lit_sel", "inst_passive_queues", "sine"],
#                 ["res", "res_passive_queues", "sine"],
#                 ["sup", "sup_passive_queues", "sup_indices_passive", "sup_full_triv", "sup_immed_triv", "sup_input_triv", "sup_full_fw", "sup_immed_fw_main", "sup_immed_fw_immed", "sup_input_fw", "sup_full_bw", "sup_immed_bw_main", "sup_immed_bw_main", "sup_input_bw", "sine"]]

# TODO: should allow for this to be just None
FREEZE_DOMAIN = [[None]]
"""
FREEZE_DOMAIN = [
    [None],
    ["inst", "inst_sos_sth_lit_sel", "inst_lit_sel", "inst_passive_queues", "standard", "prep"],
    [
        "sup",
        "sup_passive_queues",
        "sup_indices_passive",
        "sup_full_triv",
        "sup_immed_triv",
        "sup_input_triv",
        "sup_full_fw",
        "sup_immed_fw_main",
        "sup_immed_fw_immed",
        "sup_input_fw",
        "sup_full_bw",
        "sup_immed_bw_main",
        "sup_immed_bw_main",
        "sup_input_bw",
    ],
]
"""
