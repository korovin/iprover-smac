import numpy as np
import pandas as pd

from hos.embedding import compute_unsolved_problem_embeddings, TargetType


def test_compute_unsolved_problem_embeddings():
    df_eval = pd.read_csv("tests/res/tptp_all.csv", index_col=0).replace(
        {-1: np.nan, -2: np.nan}
    )

    res = compute_unsolved_problem_embeddings(
        "test",
        df_eval,
        TargetType.ADMISSIBLE,
        2,
        1.2,
        file_path="tests/res/solver_features_new.csv",
    )
    assert len(res) > 10
    assert len(list(res.values())[0]) == len(df_eval.columns)
