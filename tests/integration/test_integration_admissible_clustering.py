import warnings

import pandas as pd
from mock import mock
from sklearn.metrics import pairwise_distances

from hos.admissible import (
    get_admissible_clusters,
    ClusterQualityMetric,
    compute_and_evaluate_kmedoids_quality,
    compute_admissible_evaluation_matrix,
    common_solved,
    UnsolvedMapping,
)
from hos.embedding import TargetType


def get_eval_df():
    df_path = "tests/res/tptp_all.csv"
    df = pd.read_csv(df_path, index_col=0)
    df = df.replace({-1: None, -2: None})
    df = df.loc[list(df.index)[:200]]
    assert (
        len(df.dropna(axis="index", how="all")) > 10
    )  # Ensure we have at least 10 problems
    return df


@mock.patch("hos.admissible.MIN_ADMISSIBLE_CLUSTER_POPULATION", 0)
@mock.patch("hos.admissible.MIN_ADMISSIBLE_HEURISTIC", 2)
@mock.patch("hos.admissible.exp_conf.get_global_timeout")
@mock.patch("hos.admissible.get_heuristic_evaluation_data")
def test_compute_admissible_clusters_solved_problems(mock_eval, mock_timeout):
    df = get_eval_df()

    # Mocking this function as it required the db
    mock_eval.return_value = df
    mock_timeout.return_value = 20
    res = get_admissible_clusters(
        list(df.columns),
        "collection",
        2,
        1.2,
        0.95,
        ClusterQualityMetric.DI,
        UnsolvedMapping.NONE,
        TargetType.SOLVED,
        1.0,
        2,
    )

    assert len(res) == 4  # Not much else to check?
    assert all(len(r) > 0 for r in res)


def test_compute_and_evaluate_kmedoids_quality():
    # Need to run this separately as coverage does not pick up the call via multiprocessing
    eval_df = get_eval_df()
    adm_df = compute_admissible_evaluation_matrix(eval_df, 2, 1.2)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        X_dist = pairwise_distances(adm_df.values, metric=common_solved, n_jobs=-1)
    k, scores = compute_and_evaluate_kmedoids_quality(X_dist, adm_df, 4)
    assert k == 4
    assert len(scores) == len(ClusterQualityMetric)
