from mock import mock
import pandas as pd

from hos.solver_features import (
    run_and_extract_features_from_problem,
    filter_problem_entries,
    FEATURE_COMPUTATION_BASE_HEURISTIC,
    run_prover_and_compute_statistics,
)
from hos.target.translate_to_iprover import get_heuristic_from_options


@mock.patch("hos.solver_features.MAX_PARSING_TIME", 2)
@mock.patch(
    "hos.solver_features.REMOVE_PROBLEMS_SOLVED_DURING_FEATURE_EXTRACTION", True
)
def test_filter_problem_entries():
    inp = pd.DataFrame(
        [[True, 0], [False, 10], [False, 1]],
        columns=["solved", "parsing_time"],
        index=(list(range(3))),
    )
    exp = pd.DataFrame([[False, 1]], columns=["solved", "parsing_time"], index=[2])

    res = filter_problem_entries(inp)
    assert res.equals(exp)


def test_run_and_extract_features_from_problem():
    heur = get_heuristic_from_options(FEATURE_COMPUTATION_BASE_HEURISTIC)

    res = run_and_extract_features_from_problem(
        "tests/res/iproveropt",
        heur.get_heuristic_cmd(0.2),
        "tests/res/problem_easy.p",
        0.2,
    )
    assert res["problem_easy.p"]["solved"]
    assert not res["problem_easy.p"]["errors"]
    assert len(res["problem_easy.p"]) == 188


@mock.patch("hos.solver_features.FEATURE_COMPUTATION_TIME_LIMIT", 1)
@mock.patch("hos.solver_features.get_prover_exec")
def test_run_prover_and_compute_statistics(mock_prover):
    # Set the prover path to test prover
    mock_prover.return_value = "tests/res/iproveropt"
    res = run_prover_and_compute_statistics(["tests/res/problem_easy.p"])
    # Ensure that the result is retrieved and looks correct
    assert "problem_easy.p" in res
    assert len(res) == 1
