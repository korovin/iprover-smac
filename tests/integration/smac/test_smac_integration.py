import pytest
from ConfigSpace import Configuration

from hos.smac.optimiser import run_smac
from hos.target.translate_to_iprover import translate_argument_dict_to_heuristic
from run_iprover_target import get_parameters_from_input


def dummy(config, seed: int = 0):
    return 12  # Arbitrary feedback


def dummy_fail(config, seed: int = 0):
    raise Exception("Failing target function")


def test_integration_run_smac_dummy_target_function():
    # This function performs an integration test on the combination of optimisatin
    # facade, scenario adn config space. It does not test with the actual target
    # function but instead utilises a dummy function,
    # The number of runs must be 2 as we are evaluating the default as well
    # Slightly faster without default config when testing
    res = run_smac(
        "tests/res/test_pcs.json",
        None,
        1,
        1,
        None,
        target_function=dummy,
        use_default_config=False,
    )

    assert res


def test_integration_run_smac_dummy_target_function_fails():
    with pytest.raises(ValueError):
        run_smac(
            "tests/res/test_pcs.json",
            None,
            1,
            1,
            None,
            target_function=dummy_fail,
            use_default_config=False,
        )


def target_create_heuristic(config: Configuration, seed: int = 0):
    """
    Target function replicating loading SMAC options as heuristics
    """
    # Convert config to the input type used for external scripts
    inp = []
    for k, v in dict(config).items():
        inp.append(f"--{k}={v}")

    # Convert the config into
    param_dict = get_parameters_from_input(inp)

    # Create the heuristic for this experiment
    translate_argument_dict_to_heuristic(param_dict)
    return 14  # Arbitrary feedback


def test_integration_run_smac_get_heuristic():
    # Check if the SMAC output can be converted into a heuristic
    # Slightly faster without default config when testing
    res = run_smac(
        "tests/res/test_pcs.json",
        None,
        1,
        1,
        None,
        target_function=target_create_heuristic,
        use_default_config=False,
    )
    assert res
