from typing import Dict

from mock import mock

from hos.target.translate_to_iprover import translate_argument_dict_to_heuristic


@mock.patch.dict(
    "hos.target.translate_to_iprover.STATIC_EXPERIMENT_OPTIONS",
    {
        "schedule": "none",
        "out_options": "none",
        "proof_out": "false",
        "stats_out": "none",
        "preprocessed_out": "false",
    },
    clear=True,
)
@mock.patch("hos.target.translate_to_iprover.PROBLEMS_IN_TFF", True)
@mock.patch("hos.target.translate_to_iprover.CLAUSIFIER_OPTIONS", "")
@mock.patch("hos.target.translate_to_iprover.CLAUSIFIER_PATH", "vclausify_rel")
def test_translate_argument_dict_to_heuristic_from_exp():
    # From experiment 70592
    test_input_str = """comb_inst_mult 8 comb_mode clause_based comb_res_mult 2 comb_sup_deep_mult 64 comb_sup_mult 0 
    conj_cone_tolerance 2.3181860295283765 demod_completeness_check off demod_use_ground true extra_neg_conj none 
    inst_activity_threshold 128 inst_dismatching true inst_eager_unprocessed_to_passive false inst_eq_res_simp false 
    inst_learning_factor 8 inst_learning_loop_flag true inst_learning_start 64 inst_lit_activity_flag false 
    inst_lit_sel_len 3 inst_lit_sel_metric_1 +eq inst_lit_sel_metric_2 +eq inst_lit_sel_metric_3 +num_symb 
    inst_lit_sel_side num_lit inst_orphan_elimination true inst_passive_queue_type stack inst_passive_queues_1_freq 2 
    inst_passive_queues_1_len 1 inst_passive_queues_len 1 inst_passive_queues_metric_1_1 -num_var inst_prop_sim_given 
    true inst_prop_sim_new false inst_restr_to_given false inst_sel_renew model inst_solver_calls_frac 
    0.8915184698465359 inst_solver_per_active 128 inst_sos_flag true inst_sos_phase true inst_sos_sth_lit_sel_len 5 
    inst_sos_sth_lit_sel_metric_1 -num_var inst_sos_sth_lit_sel_metric_2 +depth inst_sos_sth_lit_sel_metric_3 +eq 
    inst_sos_sth_lit_sel_metric_4 +eq inst_sos_sth_lit_sel_metric_5 +ground inst_start_prop_sim_after_learn 9 
    inst_subs_given true inst_subs_new true inst_to_smt_solver true instantiation_flag true prep_sup_sim_all true 
    prep_sup_sim_sup false preprocessing_flag true prolific_symb_bound 512 prop_solver_per_cl 16384 res_to_smt_solver 
    true resolution_flag true share_sel_clauses false subs_bck_mult 64 sup_bw_gjoin_interval 100 sup_cache_sim all 
    sup_full_bw_flag false sup_full_fixpoint false sup_full_fw_flag false sup_fun_splitting true 
    sup_immed_bw_immed_ACDemod false sup_immed_bw_immed_Demod true sup_immed_bw_immed_FullSubsAndRes false 
    sup_immed_bw_immed_Subsumption false sup_immed_bw_immed_SubsumptionRes true sup_immed_bw_immed_UnitSubsAndRes 
    true sup_immed_bw_immed_flag true sup_immed_bw_main_ACDemod false sup_immed_bw_main_Demod false 
    sup_immed_bw_main_FullSubsAndRes true sup_immed_bw_main_Subsumption true sup_immed_bw_main_SubsumptionRes true 
    sup_immed_bw_main_UnitSubsAndRes true sup_immed_bw_main_flag true sup_immed_fixpoint false 
    sup_immed_fw_immed_ACDemod true sup_immed_fw_immed_ACJoinability false sup_immed_fw_immed_ACNormalisation false 
    sup_immed_fw_immed_Connectedness false sup_immed_fw_immed_Demod false sup_immed_fw_immed_FullSubsAndRes true 
    sup_immed_fw_immed_GroundJoinability true sup_immed_fw_immed_LightNorm true sup_immed_fw_immed_SMTSubs false 
    sup_immed_fw_immed_Subsumption true sup_immed_fw_immed_SubsumptionRes false sup_immed_fw_immed_UnitSubsAndRes true 
    sup_immed_fw_immed_flag true sup_immed_fw_main_ACDemod false sup_immed_fw_main_ACJoinability true 
    sup_immed_fw_main_ACNormalisation false sup_immed_fw_main_Connectedness true sup_immed_fw_main_Demod false 
    sup_immed_fw_main_FullSubsAndRes true sup_immed_fw_main_GroundJoinability true sup_immed_fw_main_LightNorm true 
    sup_immed_fw_main_SMTSubs false sup_immed_fw_main_Subsumption true sup_immed_fw_main_SubsumptionRes true 
    sup_immed_fw_main_UnitSubsAndRes false sup_immed_fw_main_flag true sup_immed_triv_PropSubs false 
    sup_immed_triv_SMTSimplify false sup_immed_triv_Unflattening true sup_immed_triv_flag true sup_indices_passive_flag 
    false sup_input_bw_flag false sup_input_fixpoint false sup_input_fw_flag false sup_input_triv_PropSubs true 
    sup_input_triv_SMTSimplify true sup_input_triv_Unflattening true sup_input_triv_flag true sup_iter_deepening 0 
    sup_main_fixpoint false sup_ordering kbo sup_passive_queue_type priority_queues sup_passive_queues_1_freq 15 
    sup_passive_queues_1_len 2 sup_passive_queues_2_freq 25 sup_passive_queues_2_len 3 sup_passive_queues_len 2 
    sup_passive_queues_metric_1_1 -conj_dist sup_passive_queues_metric_1_2 +age sup_passive_queues_metric_2_1 -age 
    sup_passive_queues_metric_2_2 +max_atom_input_occur sup_passive_queues_metric_2_3 -num_lits sup_prop_simpl_given 
    true sup_prop_simpl_new false sup_restarts_mult 12 sup_smt_interval 16384 sup_symb_ordering invfreq_arity 
    sup_term_weight default sup_to_prop_solver passive sup_unprocessed_bound 10 superposition_flag true"""

    def get_set(s):
        return set([x.strip() for x in s.split(",")])

    input_params = test_input_str.split()
    param_dict = dict(zip(input_params[::2], input_params[1::2]))
    heur = translate_argument_dict_to_heuristic(param_dict, ignore_empty_check=False)

    exp_res = """--stats_out none, --out_options none, --schedule none, 
    --clausifier_options --mode tclausify -t 100.00, --clausifier vclausify_rel, --sup_input_bw [], 
    --sup_immed_bw_immed [SubsumptionRes;UnitSubsAndRes;Demod], 
    --sup_immed_bw_main [Subsumption;SubsumptionRes;UnitSubsAndRes;FullSubsAndRes], --sup_full_bw [], --sup_input_fw [], 
    --sup_immed_fw_immed [Subsumption;UnitSubsAndRes;FullSubsAndRes;LightNorm;ACDemod;GroundJoinability], 
--sup_immed_fw_main [Subsumption;SubsumptionRes;FullSubsAndRes;LightNorm;ACJoinability;GroundJoinability;Connectedness], 
    --sup_full_fw [], --sup_input_triv [PropSubs;Unflattening;SMTSimplify], --sup_immed_triv [Unflattening], 
    --sup_indices_passive [], --sup_passive_queues_freq [15;25], 
    --sup_passive_queues [[-conj_dist;+age];[-age;+max_atom_input_occur;-num_lits]], --sup_cache_sim all, 
    --sup_term_weight default, 
    --sup_symb_ordering invfreq_arity, --sup_ordering kbo, --sup_to_prop_solver passive, --demod_completeness_check off,
     --sup_passive_queue_type priority_queues, --inst_sos_sth_lit_sel [-num_var;+depth;+eq;+eq;+ground], 
     --inst_lit_sel [+eq;+eq;+num_symb], --inst_passive_queues_freq [2], --inst_passive_queues [[-num_var]], 
     --inst_sel_renew model, 
     --inst_passive_queue_type stack, --inst_solver_calls_frac 0.8915184698465359, --inst_lit_sel_side num_lit, 
     --extra_neg_conj none, --conj_cone_tolerance 2.3181860295283765, --comb_mode clause_based, 
     --preprocessed_out false, --proof_out false, --sup_input_fixpoint false, --sup_immed_fixpoint false, 
     --sup_main_fixpoint false, --sup_full_fixpoint false, --sup_fun_splitting true, --sup_prop_simpl_given true, 
     --sup_prop_simpl_new false, --demod_use_ground true, --superposition_flag true, --res_to_smt_solver true, 
     --resolution_flag true, --inst_sos_phase true, --inst_sos_flag true, --inst_to_smt_solver true, 
     --inst_lit_activity_flag false, --inst_restr_to_given false, --inst_learning_loop_flag true, 
     --inst_orphan_elimination true, --inst_subs_given true, --inst_eq_res_simp false, --inst_subs_new true, 
     --inst_prop_sim_new false, --inst_prop_sim_given true, --inst_eager_unprocessed_to_passive false, 
     --inst_dismatching true, --instantiation_flag true, --prep_sup_sim_all true, --prep_sup_sim_sup false, 
     --preprocessing_flag true, --share_sel_clauses false, --sup_bw_gjoin_interval 100, --sup_smt_interval 16384, 
     --sup_restarts_mult 12, --sup_iter_deepening 0, --sup_unprocessed_bound 10, --inst_activity_threshold 128, 
     --inst_start_prop_sim_after_learn 9, --inst_learning_factor 8, --inst_learning_start 64, 
     --inst_solver_per_active 128, --subs_bck_mult 64, --prop_solver_per_cl 16384, --comb_sup_deep_mult 64, 
     --comb_sup_mult 0, --comb_inst_mult 8, --comb_res_mult 2, --prolific_symb_bound 512, --time_out_real 100"""
    set_heur = get_set(heur.get_prover_options(100))
    set_exp = {f'"{h}"' for h in get_set(exp_res)}
    assert set_heur == set_exp
    assert len(param_dict) == 0


@mock.patch.dict(
    "hos.target.translate_to_iprover.STATIC_EXPERIMENT_OPTIONS",
    clear=True,
)
@mock.patch("hos.target.translate_to_iprover.PROBLEMS_IN_TFF", False)
@mock.patch("hos.target.translate_to_iprover.CLAUSIFIER_OPTIONS", "")
@mock.patch("hos.target.translate_to_iprover.CLAUSIFIER_PATH", "vclausify_rel")
def test_translate_argument_dict_to_heuristic_empty():
    param_dict: Dict[str, str] = {}
    heur = translate_argument_dict_to_heuristic(param_dict, ignore_empty_check=False)
    assert len(param_dict) == 0
    # clausifier is always added
    assert heur.get_no_options() == 2
    assert heur.get_option_value("clausifier") == "vclausify_rel"
    assert heur.get_option_value("clausifier_options") == "--mode clausify -t 300.00"


def test_translate_argument_dict_to_heuristic_all_flags_false():
    param_dict = {
        "preprocessing_flag": "false",
        "qbf_mode": "false",
        "sat_mode": "false",
        "instantiation_flag": "false",
        "superposition_flag": "false",
        "resolution_flag": "false",
        "abstr_ref_len": "0",
    }
    heur = translate_argument_dict_to_heuristic(param_dict, ignore_empty_check=False)
    assert set(param_dict).issubset(set(heur.options))
