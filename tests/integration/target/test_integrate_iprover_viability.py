from mock import mock

from hos.target.heuristic import Heuristic
from hos.target.translate_to_iprover import add_clausifier_params
from hos.target.viability import check_heuristic_viability

PROVER_PATH = "tests/res/iproveropt_2022_02_23"


@mock.patch("hos.target.viability.db")
@mock.patch("hos.target.viability.get_easy_problems")
@mock.patch("hos.target.viability.NUMBER_OF_EASY_PROBLEMS", 1)
def test_check_heuristic_viability_viable(mock_probs, mock_db):
    mock_db.smac.get_current_smac_experiment_incumbent.return_value = 30458
    mock_probs.return_value = ["tests/res/problem_easy.p"]
    # Easy problem should always pass
    heur = Heuristic()
    add_clausifier_params(heur, {})
    res = check_heuristic_viability(heur, prover_path=PROVER_PATH)
    assert res is True


@mock.patch("hos.target.viability.db")
@mock.patch("hos.target.viability.get_easy_problems")
@mock.patch("hos.target.viability.NUMBER_OF_EASY_PROBLEMS", 1)
@mock.patch("hos.target.viability.RUNTIME_EASY_PROBLEMS", 0.1)
def test_check_heuristic_viability_nonviable(mock_probs, mock_db):
    mock_db.smac.get_current_smac_experiment_incumbent.return_value = 30458
    mock_probs.return_value = ["tests/res/problem_hard.p"]
    # Easy problem should always pass
    heur = Heuristic()
    add_clausifier_params(heur, {})
    res = check_heuristic_viability(heur, prover_path=PROVER_PATH)
    assert res is False
