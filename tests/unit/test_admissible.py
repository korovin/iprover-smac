import mock
import pytest
import pandas as pd
from sklearn_extra.cluster import KMedoids

from hos.admissible import (
    remove_small_admissible_clusters,
    common_solved,
    compute_admissible_evaluation_matrix,
    filter_problems_by_admissible_ratio,
    mask_runtime_upper_bound_df,
    get_heuristic_evaluation_data,
    get_problem_ids_not_in_clusters,
    supplement_random_unsolved_problems,
    supplement_kmedoid_classification,
    supplement_knn_classification,
    supplement_embedded_distance,
)


@pytest.mark.parametrize(
    "inp, exp",
    [
        [{}, {}],
        [{0: range(100), 1: range(200)}, {0: range(100), 1: range(200)}],
        [{0: range(1), 1: range(5)}, {}],
        [{4: range(9), 8: range(10)}, {8: range(10)}],
    ],
)
@mock.patch("hos.admissible.MIN_ADMISSIBLE_CLUSTER_POPULATION", 10)
def test_remove_small_clusters(inp, exp):
    res = remove_small_admissible_clusters(inp)  # type: ignore
    assert res == exp


@pytest.mark.parametrize(
    "u, v, exp",
    [
        [[0], [0], 1],
        [[0, 0, 0], [0, 0, 0], 1],
        [[1], [1], 0],
        [[1, 1, 1], [1, 1, 1], 0],
        [[1, 1, 1, 1], [0, 0, 0, 1], 0.6],
        [[1, 1, 1, 0], [1, 1, 1, 0], 0],
        [[1, 1, 0], [0, 1, 1], 0.5],
    ],
)
def test_common_solved(u, v, exp):
    res = common_solved(u, v)
    assert res == exp


def test_compute_admissible_evaluation_matrix():
    df = pd.DataFrame(
        [
            [-1, -1, -1],
            [-1, -1, 0],
            [-1, 1, 1],
            [10, 2, 3],
            [-1, 100, 105],
            [1.5, 2, 23],
        ],
        columns=["h1", "h2", "h3"],
        index=["p1", "p2", "p3", "p4", "p5", "p6"],
    )
    df = df.replace({-1: None})
    exp = pd.DataFrame(
        [[0, 0, 1], [0, 1, 1], [0, 1, 1], [0, 1, 1], [1, 1, 0]],
        columns=["h1", "h2", "h3"],
        index=["p2", "p3", "p4", "p5", "p6"],
    )
    res = compute_admissible_evaluation_matrix(df, adm_sc=2, adm_sf=1.2)
    print(res)
    print(exp == 1)
    assert len(res.columns) == len(exp.columns)
    assert len(res.index) == len(exp.index)
    assert res.equals(exp == 1)

    # compute_admissible_evaluation_matrix


def get_admissible_matrix():
    df = pd.DataFrame(
        [[0, 0, 1, 1], [1, 0, 1, 1], [1, 1, 1, 1], [1, 1, 0, 0], [1, 0, 0, 0]],
        columns=["h1", "h2", "h3", "h4"],
        index=["p2", "p3", "p4", "p5", "p6"],
    )
    return df


def test_filter_problems_by_admissible_ratio_remove_all():
    df = get_admissible_matrix()
    res = filter_problems_by_admissible_ratio(df, adm_ratio=0)
    assert len(res) == 0


def test_filter_problems_by_admissible_ratio_remove_none():
    df = get_admissible_matrix()
    res = filter_problems_by_admissible_ratio(df, adm_ratio=1.0)
    assert res.equals(df)


def test_filter_problems_by_admissible_ratio_remove_some():
    df = get_admissible_matrix()
    res = filter_problems_by_admissible_ratio(df, adm_ratio=0.74)
    assert len(res) == 3


def test_mask_runtime_upper_bound_df():
    df = pd.DataFrame(
        [
            [-1, -1, -1],
            [-1, -1, 0],
            [-1, 1, 11],
            [10, 2, 3],
            [-1, 100, 105],
            [1.5, 2, 23],
        ]
    )
    df = df.replace({-1: None})

    exp = pd.DataFrame(
        [
            [-1, -1, -1],
            [-1, -1, 0],
            [-1, 1, -1],
            [10, 2, 3],
            [-1, -1, -1],
            [1.5, 2, -1],
        ]
    )
    exp = exp.replace({-1: None})

    res = mask_runtime_upper_bound_df(df, 10)
    assert res.equals(exp)


@mock.patch("hos.admissible.db")
def test_get_heuristic_evaluation_data_size_mismatch(mock_db):
    mock_db.general.get_problem_ids_in_collection.return_value = list(range(10))
    mock_db.stats.query_heuristic_evaluation.side_effects = list(range(5))
    with pytest.raises(ValueError):
        _ = get_heuristic_evaluation_data([3045, 3046], "collection", 10)


@mock.patch("hos.admissible.db")
def test_get_heuristic_evaluation_data(mock_db):
    mock_db.general.get_problem_ids_in_collection.return_value = list(range(5))
    mock_db.stats.query_heuristic_evaluation.side_effect = (
        list(range(5)),
        list(range(5)),
    )
    df = get_heuristic_evaluation_data([3045, 3046], "collection", 10)
    assert len(df.columns) == 2
    assert len(df.index) == 5
    mock_db.stats.query_heuristic_evaluation.assert_has_calls(
        [mock.call(3045), mock.call(3046)]
    )


@mock.patch("hos.admissible.db")
def test_get_problems_not_in_clusters(mock_db):
    inp = {1: [1, 2, 3, 4], 2: [5, 6, 7]}
    mock_db.general.get_problem_ids_in_collection.return_value = list(range(11))
    exp = [0, 8, 9, 10]
    res = get_problem_ids_not_in_clusters(inp, "collection")
    assert res == exp


def test_supplement_random_unsolved_problems_no_problems():
    c = {1: [1, 2], 2: [1, 2, 3, 4], 3: [1, 2, 3, 4, 5, 0]}
    supp = [5, 6, 7, 8]
    res = supplement_random_unsolved_problems(c, supp, 1.0)
    assert len(res) == len(c)
    assert len(c[1]) == 4
    assert len(c[2]) == 8
    assert len(c[3]) == 10


@mock.patch("hos.admissible.map_predict_problems_to_clusters")
def test_supplement_kmedoid_classification(mock_map):
    c = {1: [1, 2], 2: [1, 2, 3, 4], 3: [1, 2, 3]}
    pred = {1: [4, 5, 6], 2: [5, 6], 4: [1, 2]}
    mock_map.return_value = pred
    exp = {1: [1, 2, 4, 5, 6], 2: [1, 2, 3, 4, 5, 6], 3: [1, 2, 3], 4: [1, 2]}
    res = supplement_kmedoid_classification(c, mock.Mock(), mock.Mock(pd.DataFrame))
    assert res == exp


def test_supplement_knn_classification():
    c = {1: [1, 2], 2: [1, 2, 3, 4], 3: [1, 2, 3]}
    mock_med = mock.Mock(KMedoids)
    adm_df = pd.DataFrame([[1, 0, 0, 0], [0, 1, 1, 0], [0, 1, 1, 0], [0, 0, 0, 1]])
    mock_med.predict.return_value = [1, 2, 2, 3]

    emb_df = pd.DataFrame([[1, 0, 0, 0], [0, 1, 1, 0], [0, 1, 1, 1]], index=[5, 6, 7])

    res = supplement_knn_classification(c, mock_med, 1, emb_df, adm_df)
    assert res == {1: [1, 2, 5], 2: [1, 2, 3, 4, 6, 7], 3: [1, 2, 3]}


def test_supplement_embedded_distance():
    c = {0: [1, 2], 1: [1, 2, 3, 4], 2: [1, 2, 3]}
    cluster_centers = [[1, 0, 0, 0], [1, 1, 1, 0], [0, 0, 0, 1]]

    emb_df = pd.DataFrame(
        [[1, 0, 0, 0], [0, 1, 1, 0], [1, 1, 1, 0], [1, 1, 1, 1]], index=[5, 6, 7, 8]
    )

    res = supplement_embedded_distance(c, cluster_centers, emb_df, 0.5)
    assert len(res) == len(c)
    assert res[0] == [1, 2, 5]
    assert res[1] == [1, 2, 3, 4, 7, 8]
    assert res[2] == [1, 2, 3, 8]
