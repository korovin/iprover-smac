from argparse import Namespace

import mock

from run_optimiser import (
    extract_clustering_args,
    report_optimisation_results,
    optimise_over_cluster,
)


def test_extract_clustering_args():
    inp = {
        "a": "a",
        "b": "b",
        "admissible": 100,
        "_admissible_": 200,
        "max_cluster_pop": 20,
    }
    args = Namespace(**inp)
    res = extract_clustering_args(args)
    assert len(res) == 3
    assert "admissible" in res
    assert "_admissible_" in res
    assert "max_cluster_pop" in res


@mock.patch("run_optimiser.stats")
@mock.patch("run_optimiser.exp_conf")
def test_report_optimisation_results(mock_conf, mock_stats):
    heur_init = [1, 1, 2, 2]
    heur_global = [4, 4, 7, 7]
    mock_conf.get_global_timeout.return_value = 12345
    report_optimisation_results(heur_init, heur_global)
    assert mock_conf.get_global_timeout.call_count == 2
    assert mock_stats.compute_solved_union.call_count == 2
    mock_stats.compute_solved_union.assert_has_calls(
        [mock.call(heur_init, 12345), mock.call(heur_global, 12345)], any_order=True
    )


def test_optimise_over_cluster_empty():
    res = optimise_over_cluster(
        "pcs", [3058, 3060], [], "collection", 100, 20, "quality_metric", 1, 1.2
    )
    assert res == []
