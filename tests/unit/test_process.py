import signal
import subprocess

import pytest
from mock import mock
import time

from hos.process import Process, ExperimentProcess, ProverProcess


def get_process():
    with mock.patch("hos.process.utils") as mock_utils:
        mock_utils.get_tmp_out_file.return_value = "tmp_file"
        p = Process()
    return p


def test_process_init():
    p = get_process()
    assert p.proc is None
    assert p.cmd is None
    assert p.start_time is None
    assert "tmp_file" in p.out_file
    assert "tmp_file_error" in p.out_file_errs
    assert p.out_err is None
    assert p.returncode is None

    with pytest.raises(NotImplementedError):
        p._get_cmd()


@mock.patch("hos.process.subprocess.Popen")
@mock.patch("hos.process.os.setsid", 2)
def test_prover_process_start(mock_pop):
    p = Process()
    mock_cmd = mock.Mock(str)
    mock_cmd.return_value = "cmd string"
    p._get_cmd = mock_cmd  # type: ignore
    p.start()

    mock_cmd.assert_called_once()
    assert p.start_time == pytest.approx(time.time(), 2)
    assert p.proc is mock_pop.return_value
    mock_pop.assert_called_once()


@mock.patch("hos.process.utils.remove_file")
def test_prover_process_remove_out(mock_rm):
    p = get_process()
    p.remove_out_files()
    assert mock_rm.call_count == 2
    assert mock.call("tmp_file") in mock_rm.call_args_list
    assert mock.call("tmp_file_error") in mock_rm.call_args_list


@pytest.mark.parametrize("poll, res", [(None, True), (not None, False)])
def test_prover_process_is_running(poll, res):
    p = get_process()
    p.proc = mock.Mock(poll=mock.Mock(return_value=poll))
    assert p.is_running() == res


@pytest.mark.parametrize("running, res", [(False, True), (True, False)])
def test_prover_process_is_finished(running, res):
    p = get_process()
    p.is_running = mock.Mock(return_value=running)
    assert p.is_finished() == res


@mock.patch("hos.process.os.killpg")
def test_prover_process_kill_not_running(mock_os):
    p = get_process()
    p.is_running = mock.Mock(return_value=False)
    p.proc = mock.Mock(subprocess.Popen)
    p.kill()
    mock_os.assert_not_called()
    p.proc.communicate.assert_not_called()


@mock.patch("hos.process.os.getpgid")
@mock.patch("hos.process.os.killpg")
def test_prover_process_kill_is_running(mock_kill, mock_pid):
    p = get_process()
    p.is_running = mock.Mock(return_value=True)
    p.proc = mock.Mock(subprocess.Popen)
    p.proc.pid = 2
    mock_pid.return_value = 5

    p.kill()
    mock_pid.assert_called_with(2)
    mock_kill.assert_called_with(5, signal.SIGKILL)
    p.proc.communicate.assert_called()


@mock.patch("hos.process.os.getpgid")
def test_prover_process_kill_process_termianted(mock_pid):
    p = get_process()
    p.is_running = mock.Mock(return_value=True)
    p.proc = mock.Mock(subprocess.Popen)
    p.proc.pid = 2
    mock_pid.side_effect = ProcessLookupError

    p.kill()
    mock_pid.assert_called_with(2)
    p.proc.communicate.assert_called()


@pytest.mark.parametrize("returncode, err_out", [(0, ""), (-9, "\n\n")])
def test_prover_process__check_errors_none(caplog, returncode, err_out):
    p = get_process()
    p.proc = mock.Mock(subprocess.Popen)
    p.proc.returncode = returncode
    p._empty_buffers = mock.Mock()

    with mock.patch("builtins.open", mock.mock_open(read_data=err_out)):
        p.check_errors()

    p._empty_buffers.assert_called_once()
    assert len(caplog.messages) == 0
    assert caplog.text == ""


@pytest.mark.parametrize(
    "returncode, err_out",
    [(0, "error no problem"), (-9, "\nno problem\n"), (1, ""), (1, ("error"))],
)
def test_prover_process_check_errors_errors(caplog, returncode, err_out):
    p = get_process()
    p.proc = mock.Mock(subprocess.Popen)
    p.proc.returncode = returncode
    p._empty_buffers = mock.Mock()

    with mock.patch("builtins.open", mock.mock_open(read_data=err_out)):
        res = p.check_errors()

    p._empty_buffers.assert_called_once()
    assert res is True
    assert f"ran with exit code {returncode} and error: " in caplog.text


@mock.patch("hos.process.utils")
def test_experiment_process_init(mock_utils):
    mock_utils.get_tmp_out_file.return_value = "tmp_file"

    p = ExperimentProcess("exp_config_file", "problem_set", "machines")
    assert p.exp_config_file == "exp_config_file"
    assert p.problem_set == "problem_set"
    assert p.machines == "machines"
    assert p.mock_exp is False

    res = p._get_cmd()
    assert res == (
        "setup-experiment -f exp_config_file -pc problem_set -s machines -q -i 1>> tmp_file 2>> "
        "tmp_file_error"
    )


@mock.patch("hos.process.get_prover_exec")
@mock.patch("hos.process.utils")
def test_prover_process_init(mock_utils, mock_exec):
    mock_utils.get_tmp_out_file.return_value = "tmp_file"
    mock_exec.return_value = "bin/iproveropt_7"

    p = ProverProcess("--time_out_real 10", "problem.p")
    assert p.heuristic == "--time_out_real 10"
    assert p.problem_path == "problem.p"
    assert p.prover_path == "bin/iproveropt_7"

    res = p._get_cmd()
    assert "bin/iproveropt_7 --time_out_real 10 problem.p 1>>" in res
