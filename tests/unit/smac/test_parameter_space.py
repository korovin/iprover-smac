from hos.smac.parameter_space import (
    get_pcs_from_json,
    update_matching_options,
    update_sine_options,
    update_list_options,
    update_nested_list_options,
    update_index_options,
    convert_json_to_pcs_string,
)

TEST_PCS_FILE_PATH = "tests/res/test_pcs.json"


def test_get_pcs_from_json():
    conf = get_pcs_from_json(TEST_PCS_FILE_PATH)
    assert type(conf) is dict
    assert "--extra_neg_conj" in conf and len(conf["--extra_neg_conj"]) == 5
    assert len(conf) == 226


def test_update_matching_options():
    inp = {
        "--sub_typing": {
            "opt_name": "--sub_typing",
            "opt_type": "categorical",
            "opt_domain": "true, false",
            "default_val": "true",
            "possible_condition": "sub_typing | preprocessing_flag == true",
        }
    }
    inc = {"--sub_typing": "false"}

    res = update_matching_options(inp, inc)
    assert res["--sub_typing"]["default_val"] == "false"
    assert len(inc) == 0


def test_update_sine_options_flag_false():
    inp = {
        "--clausifier_sine_flag": {
            "opt_name": "--clausifier_sine_flag",
            "opt_type": "categorical",
            "opt_domain": "true, false",
            "default_val": "true",
            "possible_condition": "",
        }
    }
    inc = {"--clausifier_options": "--mode clausify -ss off"}
    res = update_sine_options(inp, inc)
    assert res["--clausifier_sine_flag"]["default_val"] == "false"
    assert len(inc) == 0


def test_update_sine_options_flag_set():
    inp = {
        "--clausifier_sine_flag": {
            "opt_name": "--clausifier_sine_flag",
            "opt_type": "categorical",
            "opt_domain": "true, false",
            "default_val": "true",
            "possible_condition": "",
        },
        "--clausifier_sine_sd": {
            "opt_name": "--clausifier_sine_sd",
            "opt_type": "ordinal",
            "opt_domain": "0, 1, 2, 4, 8, 16, 32, 64",
            "default_val": "1",
            "possible_condition": "clausifier_sine_sd | clausifier_sine_flag == true",
        },
        "--clausifier_sine_st": {
            "opt_name": "--clausifier_sine_st",
            "opt_type": "ordinal",
            "opt_domain": "1, 1.2, 1.4, 1.8, 2, 3, 4",
            "default_val": "1",
            "possible_condition": "clausifier_sine_st | clausifier_sine_flag == true",
        },
    }
    inc = {"--clausifier_options": "--mode clausify -ss -st 1.5 -sd 3"}
    res = update_sine_options(inp, inc)
    assert res["--clausifier_sine_flag"]["default_val"] == "true"
    assert res["--clausifier_sine_sd"]["default_val"] == "3"
    assert res["--clausifier_sine_st"]["default_val"] == "1.5"
    assert len(inc) == 0


def test_update_list_options():
    inp = {
        "--inst_lit_sel_len": {"default_val": "3"},
        "--inst_lit_sel_metric_1": {"default_val": "+split"},
        "--inst_lit_sel_metric_2": {"default_val": "-sign"},
    }
    inc = {"--inst_lit_sel": "[+ground;-num_var]"}

    res = update_list_options(inp, inc, "inst_lit_sel")
    assert res["--inst_lit_sel_len"]["default_val"] == "2"
    assert res["--inst_lit_sel_metric_1"]["default_val"] == "+ground"
    assert res["--inst_lit_sel_metric_2"]["default_val"] == "-num_var"
    assert len(inc) == 0


def test_update_nested_list_options():
    inp = {
        "--inst_passive_queues_len": {"default_val": "3"},
        "--inst_passive_queues_1_len": {"default_val": "3"},
        "--inst_passive_queues_1_freq": {"default_val": "2"},
        "--inst_passive_queues_2_len": {"default_val": "3"},
        "--inst_passive_queues_2_freq": {"default_val": "2"},
        "--inst_passive_queues_metric_1_1": {"default_val": "+age"},
        "--inst_passive_queues_metric_1_2": {"default_val": "+num_lits"},
        "--inst_passive_queues_metric_2_1": {"default_val": "-age"},
    }

    inc = {
        "--inst_passive_queues": "[[-age;+ground];[+conj_dist]]",
        "--inst_passive_queues_freq": "[5;15]",
    }
    res = update_nested_list_options(inp, inc, "inst_passive_queues")

    assert res["--inst_passive_queues_len"]["default_val"] == "2"
    assert res["--inst_passive_queues_1_len"]["default_val"] == "2"
    assert res["--inst_passive_queues_2_len"]["default_val"] == "1"
    assert res["--inst_passive_queues_1_freq"]["default_val"] == "5"
    assert res["--inst_passive_queues_2_freq"]["default_val"] == "15"
    assert res["--inst_passive_queues_metric_1_1"]["default_val"] == "-age"
    assert res["--inst_passive_queues_metric_1_2"]["default_val"] == "+ground"
    assert res["--inst_passive_queues_metric_2_1"]["default_val"] == "+conj_dist"
    assert len(inc) == 0


def test_update_index_options():
    inp = {
        "--sup_immed_fw_immed_flag": {
            "opt_name": "--sup_immed_fw_immed_flag",
            "default_val": "false",
        },
        "--sup_immed_fw_immed_Subsumption": {
            "opt_name": "--sup_immed_fw_immed_Subsumption",
            "default_val": "false",
        },
        "--sup_immed_fw_immed_SubsumptionRes": {
            "opt_name": "--sup_immed_fw_immed_SubsumptionRes",
            "default_val": "false",
        },
        "--sup_immed_fw_immed_UnitSubsAndRes": {
            "opt_name": "--sup_immed_fw_immed_UnitSubsAndRes",
            "default_val": "true",
        },
        "--sup_immed_fw_immed_Demod": {
            "opt_name": "--sup_immed_fw_immed_Demod",
            "default_val": "false",
        },
    }
    inc = {"--sup_immed_fw_immed": "[Subsumption;SubsumptionRes;Demod]"}
    res = update_index_options(inp, inc, "sup_immed_fw_immed")
    assert res["--sup_immed_fw_immed_flag"]["default_val"] == "true"
    assert res["--sup_immed_fw_immed_Subsumption"]["default_val"] == "true"
    assert res["--sup_immed_fw_immed_SubsumptionRes"]["default_val"] == "true"
    assert res["--sup_immed_fw_immed_UnitSubsAndRes"]["default_val"] == "false"
    assert res["--sup_immed_fw_immed_Demod"]["default_val"] == "true"
    assert len(inc) == 0


def test_convert_json_to_pcs_string():
    inp = {
        "--inst_passive_queues_len": {
            "opt_name": "--inst_passive_queues_len",
            "opt_type": "integer",
            "opt_domain": "1, 3",
            "default_val": "3",
            "possible_condition": "inst_passive_queues_len | instantiation_flag == true",
        }
    }
    res = convert_json_to_pcs_string(inp)
    assert (
        res
        == "inst_passive_queues_len integer [1, 3] [3]\ninst_passive_queues_len | instantiation_flag == true\n"
    )
