from mock import mock

from hos.experiment_config import (
    ExperimentConfig,
    _get_env,
    get_global_experiment_config,
    get_local_experiment_config,
    get_prover_id,
    get_global_timeout,
    get_local_timeout,
    get_prover_exec,
)


def test_experiment_config():
    # Just test everything in one function as it is slightly annoying
    conf = ExperimentConfig(
        1,
        300,
        256,
        20000,
        "environment",
        "library_path",
        "library",
        "library_version",
        "problem_forms",
        305,
        "prover_options",
    )

    assert conf.prover == 1
    assert conf.timeout_wallclock == 300
    assert conf.memory_limit == 256
    assert conf.stack_size == 20000
    assert conf.environment == "environment"
    assert conf.library_path == "library_path"
    assert conf.library == "library"
    assert conf.library_version == "library_version"
    assert conf.problem_forms == "problem_forms"
    assert conf.wallclock_grace == 305
    assert conf.prover_options == "prover_options"

    # Just check that the function executes well
    assert conf._to_str()

    assert conf.get_timeout() == 300

    with mock.patch("builtins.open"):
        with mock.patch(
            "hos.experiment_config.get_tmp_out_file", return_value="temporary_file"
        ) as tmp:
            res = conf.to_file()
    assert res == "temporary_file"
    tmp.assert_called_once()


@mock.patch(
    "hos.experiment_config.RUN_ENV", {"local_mode": {"env_test_key": "env_test_value"}}
)
@mock.patch.dict("hos.experiment_config.EXP_ENV", {"exp_test_key": "exp_test_value"})
def test_get_env():
    res = _get_env("local_mode")
    assert "env_test_key" in res and res["env_test_key"] == "env_test_value"
    assert "exp_test_key" in res and res["exp_test_key"] == "exp_test_value"


@mock.patch(
    "hos.experiment_config.RUN_ENV",
    {"local": {"timeout_wallclock": 12345, "wallclock_grace": 7}},
)
def test_get_local_timeout():
    res = get_local_experiment_config("--inst 100")
    assert res.prover_options == "--inst 100"
    assert res.timeout_wallclock == 12345
    assert res.wallclock_grace == 7


@mock.patch(
    "hos.experiment_config.RUN_ENV",
    {"global": {"timeout_wallclock": 54321, "wallclock_grace": 5}},
)
def test_get_global_timeout():
    res = get_global_experiment_config("--inst 100")
    assert res.prover_options == "--inst 100"
    assert res.timeout_wallclock == 54321
    assert res.wallclock_grace == 5


@mock.patch(
    "hos.experiment_config.RUN_ENV",
    {"global": {"timeout_wallclock": 200}, "local": {"timeout_wallclock": 300}},
)
def test_get_timeout():
    assert get_global_timeout() == 200
    assert get_local_timeout() == 300


@mock.patch.dict("hos.experiment_config.EXP_ENV", {"prover": 27})
def test_get_prover_id():
    res = get_prover_id()
    assert res == 27


@mock.patch("hos.experiment_config.db")
@mock.patch("hos.experiment_config.get_prover_id")
def test_get_prover_exec(mock_id, mock_db):
    # def test_get_prover_exec(mock_id):
    mock_id.return_value = 40
    mock_db.general.get_prover_executable_from_id.return_value = "iprover_exec"

    res = get_prover_exec()

    assert res == "iprover_exec"
    mock_id.assert_called_once()
    mock_db.general.get_prover_executable_from_id.assert_called_once_with(40)


@mock.patch("hos.experiment_config.db")
@mock.patch("hos.experiment_config.get_prover_id")
def test_get_prover_exec_exception(mock_id, mock_db):
    mock_id.return_value = 40
    mock_db.general.get_prover_executable_from_id.return_value = "iprover_exec"
    mock_db.general.get_prover_executable_from_id.side_effect = Exception

    res = get_prover_exec()
    assert "iproveropt" in res

    mock_id.assert_called_once()
    mock_db.general.get_prover_executable_from_id.assert_called_once_with(40)
