# TODO should do an itnegration test with a smalle feature set?
# TODO do everything else with integration testing?
import mock
import pandas as pd
import numpy as np

from hos.embedding import (
    compute_feature_target,
    TargetType,
    get_solved_unsolved_indexes,
    filter_evaluation_features,
    align_df_indexes,
)


@mock.patch("hos.embedding.compute_admissible_evaluation_matrix")
def test_compute_feature_target_adm(mock_comp):
    mock_df = mock.Mock(pd.DataFrame)
    compute_feature_target(mock_df, TargetType.ADMISSIBLE, 2, 1.5)
    mock_comp.assert_called_once_with(mock_df, 2, 1.5)


def test_compute_feature_target_solved():
    df = pd.DataFrame(
        [[np.nan, np.nan, 20, 40], [np.nan, np.nan, np.nan], [1, 5, 7, 11]]
    )
    res = compute_feature_target(df, TargetType.SOLVED, 2, 1.5)
    exp = pd.DataFrame([[0, 0, 1, 1], [0, 0, 0, 0], [1, 1, 1, 1]])
    assert res.equals(exp)


@mock.patch("hos.embedding.MINIMUM_RUNTIME_TARGET", 2)
def test_filter_evaluation_features():
    df = pd.DataFrame(
        [[np.nan, np.nan], [1, 5], [8, 9], [np.nan, 1], [np.nan, 9], [1, 1]],
        index=[1, 2, 3, 4, 5, 6],
    )
    res = filter_evaluation_features(df)
    exp = pd.DataFrame([[np.nan, np.nan], [8.0, 9.0], [np.nan, 9.0]], index=[1, 3, 5])
    assert res.equals(exp)


def test_align_df_indexes():
    a = pd.DataFrame([[], [], []], index=[1, 2, 3, 0])
    b = pd.DataFrame([[], [], []], index=[5, 2, 3, 9])
    a, b = align_df_indexes(a, b)
    assert list(a.index) == list(b.index) == [2, 3]


def test_get_solved_unsolved_indexes():
    df = pd.DataFrame([[np.nan, np.nan], [3, 5], [np.nan, 10]], index=["1", "2", "3"])
    s, u = get_solved_unsolved_indexes(df)
    assert s == ["2", "3"]
    assert u == ["1"]
