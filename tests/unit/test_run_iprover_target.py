from run_iprover_target import TargetStatus


def test_target_status():
    assert str(TargetStatus.SUCCESS) == "SUCCESS"
    assert str(TargetStatus.CRASHED) == "CRASHED"
