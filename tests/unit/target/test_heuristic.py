import pytest

from hos.target.heuristic import Heuristic


def test_heuristic_init():
    heur = Heuristic()
    assert len(heur.options) == 0
    assert heur.get_no_options() == 0


def test_add_option():
    heur = Heuristic()
    op, val = "--time", 10
    heur.add_option(op, val)
    assert heur.get_no_options() == 1
    assert heur.get_option_value(op) == val


def test_add_duplicate_option():
    heur = Heuristic()
    op, val = "--time", 10
    heur.add_option(op, val)
    with pytest.raises(ValueError):
        heur.add_option(op, val)


@pytest.mark.parametrize(
    "opts, exp",
    [
        (
            {
                "instantiation_flag": "true",
                "resolution_flag": "true",
                "superposition_flag": "true",
            },
            True,
        ),
        (
            {
                "instantiation_flag": "true",
                "resolution_flag": "false",
                "superposition_flag": "false",
            },
            True,
        ),
        (
            {
                "instantiation_flag": "false",
                "resolution_flag": "false",
                "superposition_flag": "false",
            },
            False,
        ),
    ],
)
def test_any_solver(opts, exp):
    heur = Heuristic()
    for op, val in opts.items():
        heur.add_option(op, val)

    assert heur.any_solver_is_selected() == exp


def test_set_timeout_no_options():
    heur = Heuristic()
    heur._set_timeout(10)
    assert heur.get_option_value("time_out_real") == 10
    assert heur.get_option_value("clausifier_options") is None


def test_set_timeout_with_clausifier():
    heur = Heuristic()
    heur.add_option("clausifier_options", "-st 1.0 -sd 3 -t 4.0")
    heur._set_timeout(10)
    assert heur.get_option_value("time_out_real") == 10
    assert heur.get_option_value("clausifier_options") == "-st 1.0 -sd 3 -t 10.00"


def get_base_heuristic():
    heur = Heuristic()
    heur.add_option("inst_flag", "true")
    heur.add_option("inst_simp", "ko")
    return heur


def test_get_prover_options():
    heur = get_base_heuristic()
    res = heur.get_prover_options(10)
    assert res == '"--inst_flag true", "--inst_simp ko", "--time_out_real 10"'


def test_get_prover_options_extra():
    heur = get_base_heuristic()
    res = heur.get_prover_options(10, extra_options={"sup_simp": "bw"})
    assert (
        res
        == '"--inst_flag true", "--inst_simp ko", "--time_out_real 10", "--sup_simp bw"'
    )


def test_get_heuristic_cmd():
    heur = get_base_heuristic()
    res = heur.get_heuristic_cmd(10)
    assert res == "--inst_flag 'true' --inst_simp 'ko' --time_out_real '10'"


def test_get_heuristic_cmd_options_extra():
    heur = get_base_heuristic()
    res = heur.get_heuristic_cmd(10, extra_options={"sup_simp": "bw"})
    assert (
        res
        == "--inst_flag 'true' --inst_simp 'ko' --time_out_real '10' --sup_simp 'bw'"
    )
