from mock import mock

from hos.target.experiment import ResultStatus
from hos.target.viability import handle_nonviable_heuristic


@mock.patch("hos.target.viability.run_mock_experiment")
def test_handle_nonviable_heuristic(mock_run):
    exp_conf, problem_set, machines = "exp_conf", "collection", "machines"
    handle_nonviable_heuristic(exp_conf, problem_set, machines)
    mock_run.assert_called_once_with(
        exp_conf, problem_set, machines, ResultStatus.UNKNOWN
    )
