from mock import mock
from hos.target.eval_cost import (
    compute_scores,
    compute_weighted_solved,
    compute_quality_scores,
    score_to_cost,
    weighted_solved_metric,
)


def test_score_to_cost_guard():
    res = score_to_cost("total_time", 12)
    assert res == 12


def test_score_to_cost():
    res = score_to_cost("solved", 200)
    assert res == -200


@mock.patch("hos.target.eval_cost.compute_weighted_solved")
@mock.patch("hos.target.eval_cost.db")
def test_compute_scores(mock_db, mock_weighted):
    mock_db.stats.query_no_solved_problems_in_exp.return_value = 1002
    mock_db.stats.query_tot_time_in_exp.return_value = 300
    mock_weighted.return_value = 1204
    res = compute_scores(123)

    mock_db.stats.query_no_solved_problems_in_exp.assert_called_once_with(123)
    mock_db.stats.query_tot_time_in_exp.assert_called_once_with(123)
    mock_weighted.assert_called_once_with(123)
    assert res == {"total_solved": 1002, "total_time": 300, "weighted_solved": 1204}


@mock.patch("hos.target.eval_cost.compute_scores")
@mock.patch("hos.target.eval_cost.db")
def test_compute_quality_scores(mock_db, mock_scores):
    mock_scores.return_value = {"total_solved": 25}
    mock_db.smac.get_current_quality_measure.return_value = "total_solved"
    res = compute_quality_scores(123)

    assert res == ("total_solved", -25)
    mock_scores.assert_called_once_with(123)
    mock_db.smac.insert_quality_scores.assert_called_once_with(
        123, {"total_solved": 25}
    )
    mock_db.smac.get_current_quality_measure.assert_called_once()


@mock.patch("hos.target.eval_cost.UNIQUE_SOLVED_WEIGHT", 30)
def test_weighted_solved_metric():
    solved_inc = list(map(str, range(0, 10)))
    solved_exp = list(map(str, range(8, 13)))
    res = weighted_solved_metric(solved_inc, solved_exp)
    assert res == 95


@mock.patch("hos.target.eval_cost.weighted_solved_metric")
@mock.patch("hos.target.eval_cost.db")
def test_compute_weighted_solved(mock_db, mock_metric):
    mock_db.smac.get_current_smac_experiment_incumbent.return_value = 1000

    res = compute_weighted_solved(123)
    assert res is not None
    mock_db.smac.get_current_smac_experiment_incumbent.assert_called_once()
    assert mock_db.stats.query_id_solved_problems_in_exp.call_count == 2
    mock_db.stats.query_id_solved_problems_in_exp.assert_has_calls(
        [mock.call(1000), mock.call(123)]
    )
