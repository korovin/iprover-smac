from mock import mock

from hos.admissible import ClusterQualityMetric, UnsolvedMapping
from hos.cluster import (
    ClusterMethod,
    get_problem_id_sample_random,
    get_problem_id_sample_split,
    get_problem_clusters,
)
from hos.embedding import TargetType


def test_cluster_method():
    assert str(ClusterMethod.SAMPLE_RANDOM) == "sample_random"
    assert str(ClusterMethod.SAMPLE_SPLIT) == "sample_split"
    assert str(ClusterMethod.ADMISSIBLE) == "admissible"


@mock.patch("hos.cluster.db")
def test_get_problem_id_sample_split(mock_db):
    mock_db.general.get_problem_ids_in_collection.return_value = list(range(100))

    res = get_problem_id_sample_split("collection", no_cluster_samples=4)

    assert len(res) == 4
    assert [len(r) for r in res] == [100 // 4] * 4
    assert sum(len(r) for r in res) == 100
    mock_db.general.get_problem_ids_in_collection.assert_called_once_with("collection")


@mock.patch("hos.cluster.db")
def test_get_problem_id_sample_random(mock_db):
    mock_db.general.get_problem_ids_in_collection.return_value = list(range(100))

    res = get_problem_id_sample_random(
        "collection", no_cluster_samples=4, max_cluster_size=20
    )

    assert len(res) == 4
    assert [len(r) for r in res] == [20] * 4
    assert sum(len(r) for r in res) == 80
    mock_db.general.get_problem_ids_in_collection.assert_called_once_with("collection")


@mock.patch("hos.cluster.get_admissible_clusters")
@mock.patch("hos.cluster.get_problem_id_sample_random")
@mock.patch("hos.cluster.get_problem_id_sample_split")
def test_get_problem_clusters_split(mock_split, mock_random, mock_admissible):
    cluster_res = [[1234567]]
    mock_split.return_value = cluster_res
    res = get_problem_clusters(
        ClusterMethod.SAMPLE_SPLIT,
        "collection",
        [3045, 3050],
        4,
        40,
        **{
            "admissible_constant": 1.0,
            "admissible_factor": 1.1,
            "admissible_filter": 0.9,
            "admissible_cluster_quality": ClusterQualityMetric.CH,
        },
    )
    assert res == cluster_res
    mock_split.assert_called_once_with("collection", 4)
    mock_random.assert_not_called()
    mock_admissible.assert_not_called()


@mock.patch("hos.cluster.get_admissible_clusters")
@mock.patch("hos.cluster.get_problem_id_sample_random")
@mock.patch("hos.cluster.get_problem_id_sample_split")
def test_get_problem_clusters_random(mock_split, mock_random, mock_admissible):
    cluster_res = [[1234567]]
    mock_random.return_value = cluster_res
    res = get_problem_clusters(
        ClusterMethod.SAMPLE_RANDOM,
        "collection",
        [3045, 3050],
        4,
        40,
        **{
            "admissible_constant": 1.0,
            "admissible_factor": 1.1,
            "admissible_filter": 0.9,
            "admissible_cluster_quality": ClusterQualityMetric.CH,
        },
    )
    assert res == cluster_res
    mock_split.assert_not_called()
    mock_random.assert_called_once_with("collection", 4, 40)
    mock_admissible.assert_not_called()


@mock.patch("hos.cluster.get_admissible_clusters")
@mock.patch("hos.cluster.get_problem_id_sample_random")
@mock.patch("hos.cluster.get_problem_id_sample_split")
def test_get_problem_clusters_admissible_solved_problems(
    mock_split, mock_random, mock_admissible
):
    cluster_res = [[1234567]]
    mock_admissible.return_value = cluster_res
    res = get_problem_clusters(
        ClusterMethod.ADMISSIBLE,
        "collection",
        [3045, 3050],
        4,
        40,
        **{
            "admissible_constant": 1.0,
            "admissible_factor": 1.1,
            "admissible_filter": 0.9,
            "admissible_cluster_quality": ClusterQualityMetric.CH,
            "admissible_unsolved_mapping": UnsolvedMapping.NONE,
            "admissible_unsolved_target": TargetType.SOLVED,
            "admissible_unsolved_sampling_ratio": 1.0,
            "admissible_unsolved_n_neighbours": 2,
        },
    )
    assert res == cluster_res
    mock_split.assert_not_called()
    mock_random.assert_not_called()
    mock_admissible.assert_called_once_with(
        [3045, 3050],
        "collection",
        **{
            "admissible_constant": 1.0,
            "admissible_factor": 1.1,
            "admissible_filter": 0.9,
            "admissible_cluster_quality": ClusterQualityMetric.CH,
            "admissible_unsolved_mapping": UnsolvedMapping.NONE,
            "admissible_unsolved_target": TargetType.SOLVED,
            "admissible_unsolved_sampling_ratio": 1.0,
            "admissible_unsolved_n_neighbours": 2,
        },
    )


@mock.patch("hos.cluster.get_admissible_clusters")
@mock.patch("hos.cluster.get_problem_id_sample_random")
@mock.patch("hos.cluster.get_problem_id_sample_split")
def test_get_problem_clusters_admissible_failed(
    mock_split, mock_random, mock_admissible
):
    # Test fallback when not enough heuristics
    mock_admissible.return_value = []
    cluster_res = [[1234567]]
    mock_random.return_value = cluster_res
    res = get_problem_clusters(
        ClusterMethod.ADMISSIBLE,
        "collection",
        [3045, 3050],
        4,
        40,
        **{
            "admissible_constant": 1.0,
            "admissible_factor": 1.1,
            "admissible_filter": 0.9,
            "admissible_cluster_quality": ClusterQualityMetric.CH,
        },
    )
    assert res == cluster_res
    mock_random.assert_called_once_with("collection", 4, 40)
    mock_split.assert_not_called()
    mock_admissible.assert_called_once_with(
        [3045, 3050],
        "collection",
        **{
            "admissible_constant": 1.0,
            "admissible_factor": 1.1,
            "admissible_filter": 0.9,
            "admissible_cluster_quality": ClusterQualityMetric.CH,
        },
    )
