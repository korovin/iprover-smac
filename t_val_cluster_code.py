import run_iprover_target
from hos.cluster import get_problem_clusters, ClusterMethod
from hos.admissible import ClusterQualityMetric


def test_sample_split():
    # All good
    clusters = get_problem_clusters(
        cluster_method=ClusterMethod.SAMPLE_SPLIT,
        problem_collection="tptp_ueq_problems",
        heuristics=None,
        no_cluster_samples=10,
        max_cluster_population=20,
        admissible_constant=2.0,
        admissible_factor=1.2,
        admissible_filter_ratio=0.95,
        admissible_cluster_quality=None,
    )
    print(sum(len(c) for c in clusters))
    print(len(clusters))
    print(len(clusters[0]))


def test_sample_random():
    clusters = get_problem_clusters(
        cluster_method=ClusterMethod.SAMPLE_RANDOM,
        problem_collection="tptp_ueq_problems",
        heuristics=None,
        no_cluster_samples=10,
        max_cluster_population=20,
        admissible_constant=2.0,
        admissible_factor=1.2,
        admissible_filter_ratio=0.95,
        admissible_cluster_quality=None,
    )
    print(sum(len(c) for c in clusters))
    print(len(clusters))
    print(len(clusters[0]))


def test_cluster_admissible():
    clusters = get_problem_clusters(
        cluster_method=ClusterMethod.ADMISSIBLE,
        # problem_collection="tptp_ueq_problems",
        problem_collection="smt_comp_2023_train_problems",
        # heuristics=[494048, 494049, 494050, 494051, 494052, 494053, 494054],
        heuristics=[
            36703,
            37330,
            37331,
            37332,
            37333,
        ],
        no_cluster_samples=10,
        max_cluster_population=20,
        admissible_constant=2.0,
        admissible_factor=1.2,
        admissible_filter_ratio=0.95,
        admissible_cluster_quality=ClusterQualityMetric.DI,
    )
    print(sum(len(c) for c in clusters))
    print(len(clusters))
    print(len(clusters[0]))


if __name__ == "__main__":
    # test_sample_split()
    # test_sample_random()
    test_cluster_admissible()
