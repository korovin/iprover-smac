#!/usr/bin/env python3
"""
Script for performing hyper-parameter optimisation over a given problem collection and configuration space.
"""
from typing import List, Optional, Dict, Any
import argparse
import sys
import time

from hos.logger import get_optimiser_logger, get_summary_logger
import hos.database as db
from hos.experiment import (
    evaluate_heuristics_on_problem_collection,
    evaluate_default_heuristic_configuration,
)
from hos.input_parser import get_optimisation_input_args
from hos.smac.optimiser import discover_local_heuristics
from hos.target.eval_cost import QualityMetric

from hos.target.validity import (
    init_invalid_heuristic_counter,
    check_reached_no_invalid_heuristics,
)
import hos.stats as stats
from hos.cluster import get_problem_clusters
import hos.experiment_config as exp_conf
import config
from hos.utils import store_experiment_parameters

# Get the logger
log = get_optimiser_logger()
summary = get_summary_logger()


def hlp_initialise_experiment(
    initial_heuristics: Optional[List[int]], problem_collection: str, pcs_file: str
) -> List[int]:
    # Evaluate the heuristics (we do not evaluate the first heuristics as they should exist in the db)
    if initial_heuristics is None:
        default_heuristic = evaluate_default_heuristic_configuration(
            problem_collection, pcs_file, config.MACHINES
        )
        global_heuristics = [default_heuristic]
    else:
        global_heuristics = initial_heuristics.copy()
    log.info(
        "Initial global  heuristics: " + ", ".join(str(s) for s in global_heuristics)
    )
    summary.info(
        "Initial global  heuristics: " + ", ".join(str(s) for s in global_heuristics)
    )

    # Initialise HLP experiment in the db
    db.smac.setup_smac_experiment_in_db(problem_collection, global_heuristics, pcs_file)

    init_invalid_heuristic_counter()

    return global_heuristics


def optimise_over_cluster(
    pcs_path: str,
    global_heuristics: List[int],
    cluster: List[int],
    problem_collection: str,
    max_cluster_size: int,
    n_smac_trials: int,
    quality_metric: QualityMetric,
    admissible_constant: float,
    admissible_factor: float,
) -> List[int]:
    # Check that there exists problems in this cluster
    if len(cluster) == 0:
        return []

    # Create a problem collection of the problem cluster
    collection_sample = db.experiment.create_problem_collection(
        cluster, problem_collection, max_cluster_size
    )
    log.info(f"## SubSample: {collection_sample}")

    # Run smac over the best heuristic in the current cluster
    new_heuristics = discover_local_heuristics(
        pcs_path,
        problem_collection,
        global_heuristics,
        n_smac_trials,
        quality_metric,
        admissible_constant,
        admissible_factor,
    )
    return new_heuristics


def hlp_post_experiment_cleanup() -> None:
    db.experiment.insert_end_time_hlp()


def report_optimisation_results(
    initial_heuristics: List[int], global_heuristics: List[int]
) -> None:
    # Report all heuristics collected
    summary.info("\nAll heuristics: " + str(global_heuristics))

    # Report the stats of the improvement
    summary.info(
        "Solved by initial heuristics: "
        f"{len(stats.compute_solved_union(initial_heuristics, exp_conf.get_global_timeout()))}"
    )
    summary.info(
        "Solved by global heuristics : "
        f"{len(stats.compute_solved_union(global_heuristics, exp_conf.get_global_timeout()))}"
    )


def run_optimisation_loop(
    pcs_file_path: str,
    smac_run_count: int,
    smac_run_count_increment: int,
    initial_heuristics: List[int],
    problem_collection: str,
    quality_metric: QualityMetric,
    max_runtime: int,
    max_hlp_iterations: int,
    max_no_invalid_heuristics: int,
    max_cluster_size: int,
    **cluster_args,
) -> List[int]:
    """
    Run the main optimisation loop over heterogeneous problem sets.
    """
    global_heuristics = initial_heuristics.copy()
    hlp_iteration: int = 0
    max_time_end: float = time.time() + 60 * 60 * max_runtime
    while time.time() <= max_time_end and hlp_iteration < max_hlp_iterations:
        summary.info(f"\n\n@@@ NEW LOOP ITERATION: {hlp_iteration}\n")
        summary.info(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))

        # Compute problem clusters
        problem_clusters = get_problem_clusters(
            heuristics=global_heuristics,
            problem_collection=problem_collection,
            **cluster_args,
        )

        log.info("Number of clusters: " + str(len(problem_clusters)))
        summary.info("Number of clusters: " + str(len(problem_clusters)))
        log.info("Cluster sizes: " + ", ".join(str(len(s)) for s in problem_clusters))

        # Loop for training SMAC on each problem cluster
        local_heuristics: List[int] = []
        for cluster in problem_clusters:
            new_heuristics = optimise_over_cluster(
                pcs_file_path,
                global_heuristics,
                cluster,
                problem_collection,
                max_cluster_size,
                smac_run_count,
                quality_metric,
                cluster_args["admissible_constant"],
                cluster_args["admissible_factor"],
            )
            log.debug(f"Optimisation returned {new_heuristics}")
            local_heuristics += new_heuristics

            # Check if the number of invalid heuristics SMAC has produced is too high
            if check_reached_no_invalid_heuristics(max_no_invalid_heuristics):
                log.error("\n### NUMBER OF INVALID HEURISTICS REACHED ###\n")
                summary.error("\n### NUMBER OF INVALID HEURISTICS REACHED ###\n")
                log.warning("Terminating experiment...")
                summary.warning("Terminating experiment...")
                return global_heuristics

        # Evaluate the local heuristics
        summary.info("Training round finished. Time for evaluation..")
        log.info("Training round finished. Time for evaluation..")
        new_evaluated_heuristics = evaluate_heuristics_on_problem_collection(
            local_heuristics, problem_collection, config.MACHINES
        )
        summary.info(
            f"Evaluation Returned: {', '.join(str(s) for s in new_evaluated_heuristics)}"
        )

        # Update global heuristics
        global_heuristics = sorted(global_heuristics + new_evaluated_heuristics)
        summary.info(
            "Global  heuristics: " + ", ".join(str(s) for s in global_heuristics)
        )

        clean_post_local_cluster_iteration(
            local_heuristics, global_heuristics, initial_heuristics
        )

        # Increment the loop counter
        hlp_iteration += 1
        smac_run_count += smac_run_count_increment

    return global_heuristics


def clean_post_local_cluster_iteration(
    local_heuristics: List[int],
    global_heuristics: List[int],
    initial_heuristics: List[int],
) -> None:
    # Delete temporary problem collections
    db.experiment.delete_problem_collection_subsets()

    # # Insert db details about this loop iteration
    db.experiment.store_heuristic_run_data([], local_heuristics, global_heuristics)

    # Compute and report performance metrics
    stats.compute_and_report_evaluation_performance_stats(
        global_heuristics, initial_heuristics
    )


def extract_clustering_args(args: argparse.Namespace) -> Dict[str, Any]:
    """
    Extracts the clustering related parameters as a dict from the input arguments for ease of use.
    """
    args_tags = ["admissible", "cluster"]
    arg_dict = vars(args)  # convert namespace to dictionary
    # Filter on substring match
    cluster_args = {
        k: v for k, v in arg_dict.items() if any(tag in k for tag in args_tags)
    }
    return cluster_args


def main() -> None:
    print("New SMAC Experiment")
    log.info(sys.argv)
    args = get_optimisation_input_args(sys.argv[1:])

    # Obtain and set the log level
    loglevel = args.loglevel or config.LOG_LEVEL
    log.setLevel(loglevel)

    # Store the experiment parameters for accessebeility
    store_experiment_parameters(
        args.problem_collection, args.wc_global, args.wc_local, loglevel
    )

    log.info(f"Input arguments: {args}")
    summary.info(f"Input arguments: {args}")
    initial_heuristics = hlp_initialise_experiment(
        args.initial_heuristics, args.problem_collection, args.parameter_space
    )

    cluster_args = extract_clustering_args(args)

    global_heuristics = run_optimisation_loop(
        args.parameter_space,
        args.smac_run_count,
        args.smac_run_count_increment,
        initial_heuristics,
        args.problem_collection,
        args.quality_metric,
        args.max_runtime,
        args.max_hlp_iterations,
        args.max_no_invalid_heuristics,
        args.max_cluster_population,
        **cluster_args,
    )

    # Insert the end time of experiment
    hlp_post_experiment_cleanup()
    report_optimisation_results(initial_heuristics, global_heuristics)

    # Report that experiment is finished
    log.info("FINISHED EXPERIMENT")
    summary.info("FINISHED EXPERIMENT")
    print("FINISHED EXPERIMENT")


if __name__ == "__main__":
    main()
