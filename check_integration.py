import logging

from hos.experiment import evaluate_default_heuristic_configuration
from hos.solver_features import get_solver_state_features
from hos.embedding import compute_unsolved_problem_embeddings
import hos.database as db
import hos.experiment_config as exp_conf
from hos.admissible import (
    get_admissible_clusters,
    UnsolvedMapping,
    ClusterQualityMetric,
    TargetType,
    get_heuristic_evaluation_data,
)


logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger()


# 'epr_mix_150_v7.5.0'
# tptp_ueq_problems


def compute_default():
    heur = evaluate_default_heuristic_configuration(
        "casc26_epr_v8.1.2", "tests/res/test_pcs.json", "vip-hlp-evaluate"
    )
    print("Returned exp id", heur)


def compute_solver_features():
    df = get_solver_state_features("casc26_epr_v8.1.2")
    print(df)


def compute_admissible_clusters_no_mapping():
    clusters = get_admissible_clusters(
        heuristics=[117507, 117507, 117507, 117507],
        problem_collection="casc26_epr_v8.1.2",
        admissible_constant=0.1,
        admissible_factor=1.02,
        admissible_filter_ratio=1.0,
        admissible_quality_metric=ClusterQualityMetric.DI,
        admissible_unsolved_mapping=UnsolvedMapping.NONE,
        admissible_unsolved_target=TargetType.SOLVED,
        admissible_unsolved_sampling_ratio=0.4,
        admissible_unsolved_n_neighbours=3,
    )
    print(clusters)


def compute_admissible_clusters_mapping():
    clusters = get_admissible_clusters(
        heuristics=[117507, 117507, 117507, 117507],
        problem_collection="casc26_epr_v8.1.2",
        admissible_constant=0.1,
        admissible_factor=1.02,
        admissible_filter_ratio=1.0,
        admissible_quality_metric=ClusterQualityMetric.DI,
        admissible_unsolved_mapping=UnsolvedMapping.DISTANCE,
        admissible_unsolved_target=TargetType.SOLVED,
        admissible_unsolved_sampling_ratio=0.4,
        admissible_unsolved_n_neighbours=3,
    )
    print(clusters)


def embed_problems():
    problem_collection = "casc26_epr_v8.1.2"
    heuristics = [
        117507,
        117717,
        117718,
        117719,
        117720,
        117721,
        117722,
        117723,
        117724,
        117725,
        117726,
        117727,
        117728,
        117729,
        117730,
        117731,
    ]

    df_eval = get_heuristic_evaluation_data(heuristics, problem_collection, exp_conf.get_global_timeout())
    id_map = db.general.get_filename_from_problem_ids_map(
        list(df_eval.index), problem_collection, reverse=True
    )
    df_eval = df_eval.rename(index=id_map)
    print(df_eval)

    compute_unsolved_problem_embeddings(
        problem_collection, df_eval, TargetType.ADMISSIBLE, adm_sc=1.0, adm_sf=1.1, file_path=None
    )


def test_get_local():
    problem_collection = "casc26_epr_v8.1.2"
    import config

    res = evaluate_heuristics_on_problem_collection(local_heuristics, problem_collection, config.MACHINES)
    print(res)
    print(type(res))


def main():
    # compute_default()
    # compute_solver_features()
    # compute_admissible_clusters_no_mapping()
    # compute_admissible_clusters_mapping()
    # embed_problems()
    test_get_local()


if __name__ == "__main__":
    main()
